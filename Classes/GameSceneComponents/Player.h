#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "GameItem.h"

class Player : public GameItem
{
public:
    Player(GameItem* gameItem);

    /*
    Returns a fully constructed player with the same dimensions and placement
    as the GameItem passed in
    */
    static Player* createPlayerFromGameItem(GameItem* gameItem);

    void runTappedAnimation();

private:
    /*
    Scale the player starts at. These values are used for animation, to make
    sure that the player always scales back to its original size.
    */
    float _playerStartScaleX;
    float _playerStartScaleY;

    /*
    player start position, making sure that we have a value that indicates
    where the player was spawned.
    */
    cocos2d::Vec2 _playerStartPosition;

    /*
    Saves the player information into the _playerStartScale and startPosition values
    */
    void savePlayerData(GameItem* gameItem);
};

#endif