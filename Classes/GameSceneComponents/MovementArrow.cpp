#include "MovementArrow.h"

MovementArrow::MovementArrow()
{
    onTexture = cocos2d::CCTextureCache::sharedTextureCache()->addImage("UserInterface/PauseMenu/ArrowOn.png");
    offTexture = cocos2d::CCTextureCache::sharedTextureCache()->addImage("UserInterface/PauseMenu/ArrowOff.png");
}

MovementArrow* MovementArrow::createArrow()
{
    MovementArrow* movementArrow = new MovementArrow();

    if (movementArrow && movementArrow->initWithFile("UserInterface/PauseMenu/ArrowOff.png"))
    {
        movementArrow->autorelease();
        return movementArrow;
    }
    CC_SAFE_DELETE(movementArrow);
    return NULL;
}

void MovementArrow::Enable()
{
    this->setTexture(onTexture);
}

void MovementArrow::Disable()
{
    this->setTexture(offTexture);
}