#include "math.h"
#include "AcidDripper.h"

#include "GameScenes\MainGameScene.h"
#include "GameSceneComponents\TagDefinitions.h"

#include "Helpers\PositioningHelper.h"

USING_NS_CC;
#define M_PI 3.14159265358979323846
#define PTM_RATIO 32.0

AcidDripper::AcidDripper(std::string kind, std::string type, float secondsBetweenDrops) : LevelItem(kind)
{
    startDripping(secondsBetweenDrops);
}

AcidDripper* AcidDripper::createDripper(std::string kind, std::string type, float secondsBetweenDrops)
{
    auto dripper = new AcidDripper(kind, type, secondsBetweenDrops);

    if (dripper && dripper->initWithFile("LevelItems/AcidDripper.png"))
    {
        dripper->autorelease();

        return dripper;
    }
    CC_SAFE_DELETE(dripper);
    return NULL;
}

void AcidDripper::startDripping(float secondsBetweenDrops)
{
    CCFiniteTimeAction* spawnDropAction = CCCallFuncN::create(this, callfuncN_selector(AcidDripper::spawnDrop));

    auto dropSequence = Sequence::create(
        DelayTime::create(secondsBetweenDrops),
        spawnDropAction,
        NULL);

    this->runAction(RepeatForever::create(dropSequence));
}

void AcidDripper::spawnDrop(Node* sender)
{
    auto mainGameScene = (MainGameScene*)(this->getParent());

    float shooterXPosition = this->getPosition().x - 50;
    float shooterYPosition = this->getPosition().y;

    // ===================== Initialize projectile =======================
    auto projectile = LevelItem::createLevelItem("AcidDrop");
    projectile->tag = "LinearProjectile";
    projectile->setPosition(shooterXPosition, shooterYPosition);

    auto screenBounds = PositioningHelper::screenBounds;
    auto relativeSideDimension = 0.2;

    float originalWidth = this->getContentSize().width;
    float originalHeight = this->getContentSize().height;

    float finalXScale = (screenBounds.width * 0.3) / originalWidth;
    float finalYScale = (screenBounds.height * 0.1) / originalHeight;

    projectile->setScale(finalXScale, finalYScale);

    mainGameScene->addChild(projectile, 0);

    auto finalPosition = Vec2(
        shooterXPosition,
        -10);

    auto movementCompleteCallback = CCCallFuncN::create(this, callfuncN_selector(AcidDripper::movementComplete));

    projectile->runAction(CCMoveTo::create(2, finalPosition));
    // ====================================================================

    // ===================== Initialize physics body =======================
    b2Body* gameItemBody;
    b2BodyDef projectileBodyDef;

    projectileBodyDef.type = b2_staticBody; // Sets wheter the body will move or stat still in the world

    projectileBodyDef.userData = projectile;
    gameItemBody = mainGameScene->World->CreateBody(&projectileBodyDef);
    gameItemBody->SetTransform(
        b2Vec2(shooterXPosition / PTM_RATIO, shooterYPosition / PTM_RATIO),
        -1 * CC_DEGREES_TO_RADIANS(projectile->getRotation()));

    b2PolygonShape projectileShape;
    projectileShape.SetAsBox(
        projectile->getWidth() / PTM_RATIO / 2,
        projectile->getHeight() / PTM_RATIO / 2);

    b2FixtureDef projectileShapeDef;
    projectileShapeDef.shape = &projectileShape;
    projectileShapeDef.density = 0.0f;
    projectileShapeDef.friction = 0.0f;
    projectileShapeDef.restitution = 0.0f;
    gameItemBody->CreateFixture(&projectileShapeDef);
}

void AcidDripper::movementComplete(Node* sender)
{

}