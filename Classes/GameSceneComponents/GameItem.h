#ifndef __GAMEITEM_H__
#define __GAMEITEM_H__

#include "cocos2d.h"

class GameItem : public cocos2d::Sprite
{
private:
    bool _flaggedForDelete;

public:
    GameItem();

    std::string tag;

    void markForDeletion();
    bool isMarkForDelete();

    virtual float getWidth();
    virtual float getHeight();
};

#endif