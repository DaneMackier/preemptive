#include "TagDefinitions.h"

int TagDefinitions::PLAYERTAG = 99;
int TagDefinitions::PAUSEMENUHUD = 98;
int TagDefinitions::GAMESCENELAYER = 97;
int TagDefinitions::MENUSCENELAYER = 96;
int TagDefinitions::MAINGAMEBACKGROUNDLAYER = 95;
int TagDefinitions::MIDDLEGAMESCENELAYER = 94;
int TagDefinitions::MIDDLEMENUSCENELAYER = 93;

int TagDefinitions::LEVELSELECTIONITEM = 92;

int TagDefinitions::GAMESCENETAG = 91;

int TagDefinitions::MENU_SCENE_LAYER = 90;
int TagDefinitions::MAIN_MENU_SCENE = 89;
int TagDefinitions::LEVEL_SELECTION_SCENE = 88;
int TagDefinitions::STATISTICS_SCENE = 87;

int TagDefinitions::PAUSE_MENU_HUD = 85; 

int TagDefinitions::MAIN_MENU_LAYER = 84; 
int TagDefinitions::LEVEL_SELECTION_LAYER = 83;
int TagDefinitions::WORLD_SELECTION_SCENE = 82;

int TagDefinitions::SHOOTER_TAG = 81;

int TagDefinitions::LEVEL_END_SCENE = 80; 

int TagDefinitions::GAMESCENE = 79; 
int TagDefinitions::TOPSCENE = 78;