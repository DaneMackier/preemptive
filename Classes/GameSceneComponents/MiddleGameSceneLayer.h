#ifndef __MIDDLEGAMESCENELAYER_H__
#define __MIDDLEGAMESCENELAYER_H__

#include "cocos2d.h"

class MiddleGameSceneLayer : public cocos2d::Layer
{
private:
    // initialize functions
    void initializeCloudProcess();
    void addMiddlePart();

    // Cocos callbacks
    void onCloudMoveComplete(cocos2d::Node* sender);

public:
    void addPlayerToMiddleLayerFollow(cocos2d::Node* player);

    void resetMiddleScene();

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MiddleGameSceneLayer);
};




#endif