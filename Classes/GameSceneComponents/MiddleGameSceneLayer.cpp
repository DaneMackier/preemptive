#include "MiddleGameSceneLayer.h"

#include "GameEngine\MiddleLayerFollow.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

#include "GameEngine\LevelManager.h"

USING_NS_CC;

Scene* MiddleGameSceneLayer::createScene()
{
    auto scene = Scene::create();
    auto layer = MiddleGameSceneLayer::create();
    scene->addChild(layer);
    return scene;
}

bool MiddleGameSceneLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }

    addMiddlePart();

  /*  auto& children = this->getChildren();
    for (auto &child : children) {
        child->setOpacity(0);
    }
*/
    return true;
}
void MiddleGameSceneLayer::addMiddlePart()
{
    auto middlePart = Sprite::create("LevelItems/World_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "/Middle_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentLevelAssetNumber()) + ".png");
    middlePart = PositioningHelper::placeGameItem(
        middlePart, 0.5, 0.3, 1, 1.8);
    middlePart->setTag(2);
    this->addChild(middlePart);

    initializeCloudProcess();
}

void MiddleGameSceneLayer::addPlayerToMiddleLayerFollow(Node* player)
{
    this->runAction(MiddleLayerFollow::create(player));
}


void MiddleGameSceneLayer::initializeCloudProcess()
{
    for (int i = 0; i < 4; i++)
    {
        bool left = false;
        float direction = rand() % 3;

        if (direction < 1.5) left = true;
        else left = false;

        int randomNumber = rand() % 3 + 1;
        auto cloud = Sprite::create(StringHelper::AddNumberToString("LevelItems/cloud", randomNumber) + ".png");
        CCFiniteTimeAction* cloudMoveComplete = CCCallFuncN::create(this, callfuncN_selector(MiddleGameSceneLayer::onCloudMoveComplete));

        cloud = PositioningHelper::placeGameItem(
            cloud, left?1.1f:-0.1, 0.6 + ((float)(i+1)/(float)randomNumber), 0.65, 0.2);
        cloud->setTag(i);
        this->addChild(cloud, -1);

        float cloudMovementTime = rand() % 16 + 7;

        cloud->runAction(CCSequence::create(
            CCMoveTo::create(cloudMovementTime, Vec2((left?-1:1) * 700, cloud->getPositionY())),
            CCFadeOut::create(0.5),
            cloudMoveComplete,
            NULL));
    }
   
}

void MiddleGameSceneLayer::onCloudMoveComplete(Node* sender)
{
    auto sentCloud = sender;
    this->removeChild(sender, true);

    bool left = false;
    float direction = rand() % 3;

    if (direction < 1.5) left = true;
    else left = false;

    int randomNumber = rand() % 3 + 1;
    auto cloud = Sprite::create(StringHelper::AddNumberToString("LevelItems/cloud", randomNumber) + ".png");
    CCFiniteTimeAction* cloudMoveComplete = CCCallFuncN::create(this, callfuncN_selector(MiddleGameSceneLayer::onCloudMoveComplete));

    cloud = PositioningHelper::placeGameItem(
        cloud, left ? 1.1f : 0, 0.6 + ((float)(sentCloud->getTag() + 1) / (float)randomNumber), 0.65, 0.2);
    cloud->setTag(sentCloud->getTag());
    this->addChild(cloud, -1);

    float cloudMovementTime = rand() % 16 + 7;

    cloud->runAction(CCSequence::create(
        CCMoveTo::create(cloudMovementTime, Vec2((left ? -1 : 1) * 700, cloud->getPositionY())),
        CCFadeOut::create(0.5),
        cloudMoveComplete,
        NULL));
}

void MiddleGameSceneLayer::resetMiddleScene()
{
    this->removeAllChildrenWithCleanup(true);

    addMiddlePart();
}