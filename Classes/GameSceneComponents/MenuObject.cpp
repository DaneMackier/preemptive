#include "MenuObject.h"

MenuObject::MenuObject(){}

MenuObject::MenuObject(int level)
{
    levelNumber = level;
}

MenuObject* MenuObject::createMenuObject(int levelNumber, const std::string& normalImage, const std::string& selectedImage, cocos2d::Ref* target,cocos2d::SEL_MenuHandler selector)
{
    MenuObject* menuObject = new MenuObject(levelNumber);

    if (menuObject && menuObject->initWithNormalImage(normalImage, selectedImage, 
        "", target, selector))
    {
        menuObject->autorelease();
        return menuObject;
    }
    CC_SAFE_DELETE(menuObject);
    return NULL;
}

float MenuObject::getWidth()
{
    return (this->getBoundingBox().getMaxX() - this->getBoundingBox().getMinX());
}

float MenuObject::getHeight()
{
    return (this->getBoundingBox().getMaxY() - this->getBoundingBox().getMinY());
}