#include "Player.h"

#include "Helpers\StringHelper.h"

#include "cocos2d.h"

USING_NS_CC;

// PUBLIC
Player::Player(GameItem* gameItem)
{
    this->tag = "Player";
    this->setPosition(gameItem->getPosition());
    this->setScale(gameItem->getScaleX(), gameItem->getScaleY());

    savePlayerData(gameItem);
}

Player* Player::createPlayerFromGameItem(GameItem* gameItem)
{
    Player* player = new Player(gameItem);
 
  
    if (player && player->initWithFile("LevelItems/Player.png"))
    {
        player->autorelease();
       
        return player;
    }
    CC_SAFE_DELETE(player);
    return NULL;
}

void Player::runTappedAnimation()
{
    float scaleBy = 0.005;

    this->runAction(CCSequence::create(
        CCScaleTo::create(0.1, _playerStartScaleX + scaleBy, _playerStartScaleY + scaleBy),
        CCScaleTo::create(0.1, _playerStartScaleX - scaleBy, _playerStartScaleY - scaleBy),
        CCScaleTo::create(0.1, _playerStartScaleX + scaleBy, _playerStartScaleY + scaleBy),
        CCScaleTo::create(0.1, _playerStartScaleX - scaleBy, _playerStartScaleY - scaleBy),
        CCScaleTo::create(0.1, _playerStartScaleX, _playerStartScaleY),
        NULL));
}

//PRIVATE
void Player::savePlayerData(GameItem* gameItem)
{
    // storing player related values for later use
    _playerStartPosition = Vec2(gameItem->getPositionX(), gameItem->getPositionY());
    _playerStartScaleX = this->getScaleX();
    _playerStartScaleY = this->getScaleY();
}

