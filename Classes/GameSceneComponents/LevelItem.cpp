#include "LevelItem.h"

#include "Helpers\StringHelper.h"

# define M_PI 3.14159265358979323846

USING_NS_CC;

LevelItem::LevelItem(std::string kind)
{
    this->tag = kind;
    itemKind = kind;
}

LevelItem* LevelItem::createLevelItem(std::string kind)
{
    LevelItem* item = new LevelItem(kind);
    
    std::string path;

    if (item && item->initWithFile("LevelItems/" + kind + ".png"))
    {
        item->autorelease();

        return item;
    }
    CC_SAFE_DELETE(item);
    return NULL;
}
