#include "math.h"
#include "Shooter.h"
#include "Player.h"

#include "GameScenes\MainGameScene.h"

#include "GameSceneComponents\TagDefinitions.h"

#include  "GameEngine\LevelManager.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

USING_NS_CC;

#define M_PI 3.14159265358979323846
#define PTM_RATIO 32.0

Shooter::Shooter(std::string kind, std::string type, float angle, float speed, float secondsBetweenShots) : LevelItem(kind)
{
    _angle = -angle;
    _shooterSpeed = speed;
    _shooterType = type;
    _secondsBetweenShots = secondsBetweenShots;

    this->setRotation(_angle);

    //this->schedule(schedule_selector(Shooter::shootLinearProjectile), secondsBetweenShots);
}

Shooter* Shooter::createShooter(std::string kind, std::string type, float angle, float speed, float secondsBetweenShots)
{
    Shooter* shooter = new Shooter(kind, type, angle, speed, secondsBetweenShots);
    
    if (shooter && shooter->initWithFile("LevelItems/Shooter_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png"))
    {
        shooter->autorelease();

        return shooter;
    }
    CC_SAFE_DELETE(shooter);
    return NULL;
}

void Shooter::update(Player* player)
{
    if (_shooterType == "following")
    {
        b2Vec2 toTarget =
            b2Vec2(this->getPositionX(), this->getPositionY()) -
            b2Vec2(player->getPositionX(), player->getPositionY());
        _angle = CC_RADIANS_TO_DEGREES(atan2f(toTarget.x, toTarget.y)) + 90;
        this->setRotation(_angle);
    }
}

void Shooter::shootLinearProjectile(float delta)
{
    auto mainGameScene = (MainGameScene*)(this->getParent());
    
    float shooterXPosition = this->getPosition().x;
    float shooterYPosition = this->getPosition().y;

    float angleInRadians = -1 * CC_DEGREES_TO_RADIANS(_angle);
    float impulseScale = 1000;
    auto impulseDirection = b2Rot(angleInRadians).GetXAxis();

    // ===================== Initialize projectile =======================
    auto projectile = LevelItem::createLevelItem("LinearProjectile_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "");
    projectile->tag = "LinearProjectile";
    projectile->setRotation(_angle);
    projectile->setPosition(shooterXPosition, shooterYPosition);

    auto screenBounds = PositioningHelper::screenBounds;
    auto relativeSideDimension = 0.2;

    float originalWidth = this->getContentSize().width;
    float originalHeight = this->getContentSize().height;

    float finalXScale = (screenBounds.width * relativeSideDimension) / originalWidth;
    float finalYScale = (screenBounds.width * relativeSideDimension) / originalHeight;

    projectile->setScale(finalXScale, finalYScale);

    mainGameScene->addChild(projectile, 0);

    auto finalPosition = Vec2(
        impulseDirection.x * impulseScale,
        impulseDirection.y * impulseScale);

    auto movementCompleteCallback = CCCallFuncN::create(this, callfuncN_selector(Shooter::movementComplete));

    projectile->runAction(CCMoveBy::create(_shooterSpeed, finalPosition));
    // ====================================================================

    // ===================== Initialize physics body =======================
    b2Body* gameItemBody;
    b2BodyDef projectileBodyDef;
    
    projectileBodyDef.type = b2_staticBody; // Sets wheter the body will move or stat still in the world
   
    projectileBodyDef.userData = projectile;
    gameItemBody = mainGameScene->World->CreateBody(&projectileBodyDef);
    gameItemBody->SetTransform(
        b2Vec2(shooterXPosition / PTM_RATIO, shooterYPosition / PTM_RATIO),
        -1 * CC_DEGREES_TO_RADIANS(projectile->getRotation()));

    b2PolygonShape projectileShape;
    projectileShape.SetAsBox(
        projectile->getWidth() / PTM_RATIO / 2, 
        projectile->getHeight() / PTM_RATIO / 2);

    b2FixtureDef projectileShapeDef;
    projectileShapeDef.shape = &projectileShape;
    projectileShapeDef.density = 0.0f;
    projectileShapeDef.friction = 0.0f;
    projectileShapeDef.restitution = 0.0f;
    gameItemBody->CreateFixture(&projectileShapeDef);
}

void Shooter::shootGravityProjectile(float delta)
{
    auto mainGameScene = (MainGameScene*)(this->getParent());

    float shooterXPosition = this->getPosition().x;
    float shooterYPosition = this->getPosition().y;

    float angleInRadians = -1 * CC_DEGREES_TO_RADIANS(_angle);
    float impulseScale = 10;
    auto impulseDirection = b2Rot(angleInRadians).GetXAxis();

    auto finalPulse = b2Vec2(
        impulseDirection.x * impulseScale,
        impulseDirection.y * impulseScale);

    // ===================== Initialize projectile =======================
    auto projectile = LevelItem::createLevelItem("GravityProjectile_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "");
    projectile->tag = "GravityProjectile";
    projectile->setRotation(_angle);
    projectile->setPosition(shooterXPosition, shooterYPosition);

    auto screenBounds = PositioningHelper::screenBounds;
    auto relativeSideDimension = 0.2;

    float originalWidth = this->getContentSize().width;
    float originalHeight = this->getContentSize().height;

    float finalXScale = (screenBounds.width * relativeSideDimension) / originalWidth;
    float finalYScale = (screenBounds.width * relativeSideDimension) / originalHeight;

    projectile->setScale(finalXScale, finalYScale);

    mainGameScene->addChild(projectile, 0);

    // ====================================================================

    // ===================== Initialize physics body =======================
    b2Body* gameItemBody;
    b2BodyDef projectileBodyDef;

    projectileBodyDef.type = b2_dynamicBody; // Sets wheter the body will move or stat still in the world

    projectileBodyDef.userData = projectile;
    gameItemBody = mainGameScene->World->CreateBody(&projectileBodyDef);
    gameItemBody->SetTransform(
        b2Vec2(shooterXPosition / PTM_RATIO, shooterYPosition / PTM_RATIO),
        -1 * CC_DEGREES_TO_RADIANS(projectile->getRotation()));

    b2PolygonShape projectileShape;
    projectileShape.SetAsBox(
        projectile->getWidth() / PTM_RATIO / 2,
        projectile->getHeight() / PTM_RATIO / 2);

    b2FixtureDef projectileShapeDef;
    projectileShapeDef.shape = &projectileShape;
    projectileShapeDef.density = 0.0f;
    projectileShapeDef.friction = 0.0f;
    projectileShapeDef.restitution = 0.0f;
    gameItemBody->CreateFixture(&projectileShapeDef);

    gameItemBody->ApplyLinearImpulse(finalPulse, gameItemBody->GetPosition(), true);
    gameItemBody->ApplyAngularImpulse(60.0f, true);
}

void Shooter::movementComplete(Node* sender)
{
    auto mainGameScene = (MainGameScene*)(((Sprite*)this)->getParent());

    mainGameScene->removeChild(sender);
}

void Shooter::startShooting()
{
    if (_shooterType == "gravity")
        this->schedule(schedule_selector(Shooter::shootGravityProjectile), _secondsBetweenShots);
    else
        this->schedule(schedule_selector(Shooter::shootLinearProjectile), _secondsBetweenShots);

}