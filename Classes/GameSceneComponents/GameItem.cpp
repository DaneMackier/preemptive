#include "GameItem.h"

GameItem::GameItem()
{
    _flaggedForDelete = false;
}

float GameItem::getWidth()
{
    return (this->getBoundingBox().getMaxX() - this->getBoundingBox().getMinX());
}

float GameItem::getHeight()
{
    return (this->getBoundingBox().getMaxY() - this->getBoundingBox().getMinY());
}

void GameItem::markForDeletion()
{
    _flaggedForDelete = true;
}

bool GameItem::isMarkForDelete()
{
    return _flaggedForDelete;
}