#ifndef __LEVELSELECTIONITEM_H__
#define __LEVELSELECTIONITEM_H__

#include "MenuObject.h"


#include "ui\CocosGUI.h"

class LevelSelectionItem : public cocos2d::Node
{
private:
  
    /*
    Pointer to the levelselect button
    */
    cocos2d::ui::Button* _levelSelectButton;

    /*
    Pointer to the prediction button
    */
    cocos2d::ui::Button* _predictionButton;

    /*
    Contains pointers to the ratings buttons
    */
    std::vector<cocos2d::ui::Button* > ratingsButtons;

    /*
    Image that contains the lock
    */
    cocos2d::Sprite* _lock;

    int _levelNumber;

    /*
    Stores the values of the starting
    */
    float _startingXScale;
    float _startingYScale;

public:
   
    /*
    Indicates if the levelselection item is 'clickable'
    */
    bool levelItemActive;

    /*
    Constructor
    */
    LevelSelectionItem(int level, int prediction);
    
    /*
    Sets the starting scale of the levelItem
    */
    void setStartingScale(float startingXScale, float startingYScale);

    /*
    Activates current level selection item
    */
    void activateLevel();

    /*
    Deactivates the current level selection item
    */
    void deactivateLevel();

    /*
    Lights up the ratings
    */
    void setRating(int rating);

    /*
    Creates and sets the level item label for this item
    */
    void createItemLabel(float relativeXPosition, float relativeYPosition);

    /*
    supplies a calback function to call when the user touches the item
    */
    void addTouchEventListener(const cocos2d::ui::Widget::ccWidgetTouchCallback& callback);

    /*
    sets the tag of the button part of the selection item
    */
    void setItemTag(int tag);

    /*
    Creates a levelselection item
    */
    static LevelSelectionItem* createLevelSelectionItem(int level, int prediction);
};


#endif