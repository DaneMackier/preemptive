#ifndef __ANIMATIONLAYER_H__
#define __ANIMATIONLAYER_H__

#include "cocos2d.h"

#include "GameSceneComponents\GameScene.h"

class AnimationLayer : public cocos2d::Layer
{
public:

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(AnimationLayer);
};

#endif