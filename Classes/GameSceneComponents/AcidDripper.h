#pragma once

#include "LevelItem.h"

class AcidDripper : public LevelItem
{
private:
    void spawnDrop(cocos2d::Node* sender);
    void movementComplete(cocos2d::Node* sender);

public:
    AcidDripper(std::string kind, std::string type, float secondsBetweenDrops);

    /*
    Schedules the dripper to start dripping drops
    */
    void startDripping(float secondsBetweenDrops);

    /*
    Create's a Dripper item
    */
    static AcidDripper* createDripper(std::string kind, std::string type, float secondsBetweenDrops);
};