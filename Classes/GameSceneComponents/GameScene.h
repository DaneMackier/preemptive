#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__

#include "cocos2d.h"

enum SCENES
{
    MAINMENU = 0,
    WORLDSELECTION = 1,
    LEVELSELECTION = 2,
    MAINGAME = 3,
    PAUSE = 4,
    LEVELEND = 5,
    ABOUT = 6
};

class GameScene : public cocos2d::Layer
{

protected:
    /*
    Contains the value of the current scene on screen
    */
    static SCENES _currentScene;

private:
    /*
    Function called when a system key has been pressed on android and iOS
    */
    virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

    /*
    Set's all the children opacity's to opacity value passed in. Used when you 
    want to see the physics debug draw on screen.
    */
    void setAllChildrenOpacity(float opacity);

public:
 

    /*
    Called when the gamescene will dispose of all it's children on screen
    */
    void dispose();

    /*
    Name of this scene
    */
    std::string gameScene;

    /*
    Handles the back button press for every scene
    */
    virtual void handleBackNavigation();
};


#endif