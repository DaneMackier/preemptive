#include "GameScene.h"


USING_NS_CC;

SCENES GameScene::_currentScene = MAINMENU;

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
    }
}

void GameScene::setAllChildrenOpacity(float opacity)
{
    auto& children = this->getChildren();
    for (auto &child : children) {
    child->setOpacity(opacity);
    }
}

void GameScene::handleBackNavigation()
{
    if (true)
    {

    }
}

void GameScene::dispose()
{
    this->removeAllChildrenWithCleanup(true);
}