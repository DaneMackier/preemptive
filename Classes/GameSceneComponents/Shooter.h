#ifndef __SHOOTER_H__
#define __SHOOTER_H__

#include "Player.h"
#include "LevelItem.h"

class Shooter : public LevelItem
{
private:
    /*
    Angle at which the shooter will be shooting
    */
    int _angle;

    /*
    Speed the shooter will be shooting projectiles
    */
    int _shooterSpeed;

    /*
    The time between shooting projectiles
    */
    float _secondsBetweenShots;

    /*
    Type of the shooter. Either static or following
    */
    std::string _shooterType;

    /*
    Function to shoot projectile every [scheduled time]
    */
    void shootLinearProjectile(float delta);

    /*
    Shoots a projectile affected by gravity
    */
    void shootGravityProjectile(float delta);

    /*
    Function that's called when the movement is complete
    */
    void movementComplete(cocos2d::Node* sender);

public:
    /*
    stores the angle of the shooter if it's a shooter
    */
    float shooterAngle;

    /*
    stores the speed a shooter will be shooting a projectile at
    */
    int shooterSpeed;

    /*
    Seconds between every time a projectile is fired
    */
    float secondsBetweenShooterShots;

    /*
    Constructs a shooter element with given angle and shooterSpeed
    */
    Shooter(std::string kind, std::string type, float angle, float speed, float secondsBetweenShots);

    /*
    Create's a shooter item with given angle and shooterSpeed and shooter sprite
    */
    static Shooter* createShooter(std::string kind, std::string type, float angle, float speed ,float secondsBetweenShots);
    
    /*
    Update's the shooter according to what's happening in the game
    */
    void update(Player* player);

    /*
    Schedules the shooter to start shooting projectiles
    */
    void startShooting();
};

#endif