#ifndef __TAGDEFINITIONS_H__
#define __TAGDEFINITIONS_H__

class TagDefinitions
{
public:
    static int PLAYERTAG;
    static int PAUSEMENUHUD;
    static int GAMESCENELAYER;
    static int MENUSCENELAYER;
    static int MAINGAMEBACKGROUNDLAYER;
    static int MIDDLEGAMESCENELAYER;
    static int MIDDLEMENUSCENELAYER;

    static int LEVELSELECTIONITEM;
    
    
    static int GAMESCENETAG;

    // Cococstudio root nodes
    static int MENU_SCENE_LAYER;
    static int MAIN_MENU_SCENE;
    static int LEVEL_SELECTION_SCENE;
    static int PAUSE_MENU_HUD;

    static int STATISTICS_SCENE;
    static int MAIN_MENU_LAYER;
    static int LEVEL_SELECTION_LAYER;
    static int WORLD_SELECTION_SCENE;
    static int LEVEL_END_SCENE;


    // game scene tags
    static int SHOOTER_TAG;    

    static int GAMESCENE;
    static int TOPSCENE;
};

#endif