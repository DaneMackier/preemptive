#ifndef __LEVELITEM_H__
#define __LEVELITEM_H__

#include "GameItem.h"

class LevelItem : public GameItem
{
public:
    /*
    Kind of item this object represents
    - Saw
    - Projectile
    - Player
    - Shooter
    */
    std::string itemKind;

    /*
    Constructs a level item with appropriate values
    */
    LevelItem(std::string kind);
    
    /*
    Create's a level item to add into the game
    */
    static LevelItem* createLevelItem(std::string kind);

};

#endif