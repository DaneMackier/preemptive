#include "LevelSelectionItem.h"


#include  "GameSceneComponents\TagDefinitions.h"

#include "Helpers\StringHelper.h"
#include "Helpers\PositioningHelper.h"
#include "Helpers\AnimationHelper.h"

#include "GameEngine\LevelManager.h"

#include "GameScenes\LevelSelectionScene.h"


USING_NS_CC;

using namespace ui;

LevelSelectionItem* LevelSelectionItem::createLevelSelectionItem(int level, int prediction)
{
    auto levelSelectionItem = new LevelSelectionItem(level, prediction);
    if (levelSelectionItem)
    {
        levelSelectionItem->autorelease();
        return levelSelectionItem;
    }
    CC_SAFE_DELETE(levelSelectionItem);
    return nullptr;
}

LevelSelectionItem::LevelSelectionItem(int level, int prediction)
{
    _levelNumber = level;
    levelItemActive = false;

    Layout* mainContainer = Layout::create();
    float relativeSideDimension = 0.3;
    auto totalSize = Size(PositioningHelper::getRelativeXPosition(relativeSideDimension), PositioningHelper::getRelativeXPosition(relativeSideDimension));
    
    mainContainer->setContentSize(totalSize);
    mainContainer->setLayoutType(LAYOUT_ABSOLUTE);

    int worldLevel = LevelManager::getInstance()->getCurrentWorld();

    // create the levelselection button
    _levelSelectButton = Button::create(
        "UserInterface/LevelSelection/World_" + StringHelper::ConvertIntToString(worldLevel) + ".png",
        "UserInterface/LevelSelection/World_" + StringHelper::ConvertIntToString(worldLevel) + "_pressed.png");
    float buttonSides = 0.2;
    _levelSelectButton->setTitleText(StringHelper::ConvertIntToString(_levelNumber));
    _levelSelectButton->setTitleFontSize(50);
    _levelSelectButton->setScale(
        PositioningHelper::getXScale(_levelSelectButton, buttonSides),
        PositioningHelper::getXScale(_levelSelectButton, buttonSides));
    _levelSelectButton->setPosition(Vec2(totalSize.width / 2, totalSize.width / 2));

    // add button to the main container
    mainContainer->addChild(_levelSelectButton);

    std::vector<Vec2> relativePositions;
    relativePositions.push_back(Vec2(0.2, 0.15));
    relativePositions.push_back(Vec2(0.5, 0.05));
    relativePositions.push_back(Vec2(0.8, 0.15));
    
    for (int i = 0; i < 3; i++)
    {
        auto ratingsButton = Button::create(
            "UserInterface/LevelSelection/Rate_" + StringHelper::ConvertIntToString(worldLevel) + "_Empty.png",
            "UserInterface/LevelSelection/Rate_" + StringHelper::ConvertIntToString(worldLevel) + "_Empty.png",
            "UserInterface/LevelSelection/Rate_" + StringHelper::ConvertIntToString(worldLevel) + "_Full.png"

            );
        auto relativePosition = relativePositions.at(i);
        PositioningHelper::placeSquareWidget(
            ratingsButton,
            0, 0,
            0.06);

        ratingsButton->setPosition(Vec2(totalSize.width*relativePosition.x, totalSize.height*relativePosition.y));
        mainContainer->addChild(ratingsButton, 1);

        ratingsButtons.push_back(ratingsButton);
    }

    _predictionButton = Button::create(
        "UserInterface/LevelSelection/Prediction_" + StringHelper::ConvertIntToString(worldLevel) + ".png"
        );
    _predictionButton->setPosition(Vec2(totalSize.width*0.15, totalSize.height*0.9));
    _predictionButton->setTitleText(StringHelper::ConvertIntToString(prediction));
    _predictionButton->setTitleFontSize(30);
    mainContainer->addChild(_predictionButton);
    
    // lock sprite
    _lock = Sprite::create("UserInterface/Shared/Lock.png");
    _lock->setScale(
        PositioningHelper::getXScale(_levelSelectButton, 0.15),
        PositioningHelper::getXScale(_levelSelectButton, 0.15));
    _lock->setPosition(Vec2(totalSize.width / 2, totalSize.width / 2));
    _lock->setOpacity(0);

    mainContainer->addChild(_lock, 5);

    this->addChild(mainContainer);
}

void LevelSelectionItem::setStartingScale(float startingXScale, float startingYScale)
{
    _startingXScale = startingXScale;
    _startingYScale = startingYScale;
}

void LevelSelectionItem::activateLevel()
{
    _levelSelectButton->setTitleText(StringHelper::ConvertIntToString(_levelNumber));

    _predictionButton->setOpacity(255);

    for (auto rating = ratingsButtons.begin(); rating != ratingsButtons.end(); rating++)
    {
        (*rating)->setOpacity(255);
    }

    _lock->setOpacity(0);

    _levelSelectButton->setEnabled(true);
}

void LevelSelectionItem::deactivateLevel()
{
    _levelSelectButton->setTitleText("");

    _predictionButton->setOpacity(0);

    for (auto rating = ratingsButtons.begin(); rating != ratingsButtons.end(); rating++)
    {
        (*rating)->setOpacity(0);
    }

    _lock->setOpacity(255);

    _levelSelectButton->setEnabled(false);
}

void LevelSelectionItem::setRating(int rating)
{
    for (int i = 0; i < rating; i++)
    {
        ratingsButtons.at(i)->setPressedActionEnabled(true);
        ratingsButtons.at(i)->setBright(false);
    }
}

void LevelSelectionItem::addTouchEventListener(const Widget::ccWidgetTouchCallback &callback)
{
    _levelSelectButton->addTouchEventListener(callback);
}

void LevelSelectionItem::setItemTag(int tag)
{
    _levelSelectButton->setTag(tag);
}