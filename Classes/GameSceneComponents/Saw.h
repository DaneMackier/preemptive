#pragma once

#include "LevelItem.h"

class Saw : public LevelItem
{
private:
    /* 
    Type of the saw 
    */
    std::string _type;

    /*
    Direction the saw will move first
    */
    std::string _direction;

    /*
    Type of special the saw
    */
    std::string _special;

    /* 
    Angle of the saw's movements
    */
    float _angle;

    /* 
    Distance the saw moves when it's a moving saw 
    */
    float _distance;

    /*
    Speed the saw will move at
    */
    float _speed;

    /* 
    Apply the rotation action to the saw 
    */
    void applyRotation();

    /* 
    Apllys the movement action to the saw 
    */
    void applyMovement();

    /* 
    Applys the fading action to the saw 
    */
    void applyFading();

public:
    Saw(std::string kind, std::string type, float angle, float distance, float speed, std::string special, std::string direction);

    /*
    Create's a Saw item
    */
    static Saw* createSaw(std::string kind, std::string type, float angle, float distance, float speed, std::string special, std::string direction);
};