#include "Saw.h"

#include "Helpers\StringHelper.h"
#include "Helpers\PositioningHelper.h"
#include "GameEngine\LevelManager.h"

USING_NS_CC;

Saw::Saw(std::string kind, std::string type, float angle, float distance, float speed, std::string special, std::string direction) : LevelItem(kind)
{
    _type = type;
    _angle = angle;
    _distance = distance;
    _speed = speed;
    _special = special;
    _direction = direction;

    if (_special == "double")
    {
        auto backSaw = Sprite::create("LevelItems/SawBack.png");
        backSaw->setPosition(Vec2(150, 150));
        backSaw->runAction(RepeatForever::create(CCRotateBy::create(1.5, -360)));
        this->addChild(backSaw, -1);
    }
    else if (_special == "heating")
    {
        auto backSaw = Sprite::create("LevelItems/HeatingFront.png");
        backSaw->setPosition(Vec2(150, 150));
        backSaw->setTag(5);
        backSaw->setOpacity(0);
        this->addChild(backSaw);
    }

    applyRotation();

    if (type == "moving")
    {
        applyMovement();
    }
    else if (type == "fading")
    {
        applyFading();
    }

}

Saw* Saw::createSaw(std::string kind, std::string type, float angle, float distance, float speed, std::string special, std::string startDirection)
{
    Saw* saw = new Saw(kind, type, angle, distance, speed, special, startDirection);

    int sawNumber = rand() % 3 + 1;

    int worldNumber = LevelManager::getInstance()->getCurrentWorld();

    std::string path;
    if (special == "none")
    {
        path = "LevelItems/" + StringHelper::ConvertIntToString(worldNumber) + "_Saw_" + StringHelper::ConvertIntToString(sawNumber) + ".png";
    }
    else if (special == "double")
    {
        path = "LevelItems/SawFront.png";
    }
    else if (special == "heating")
    {
        path = "LevelItems/HeatingBack.png";
    }

    if (saw && saw->initWithFile(path))
    {
        saw->autorelease();

        return saw;
    }
    CC_SAFE_DELETE(saw);
    return NULL;
}

void Saw::applyRotation()
{
     if (_special == "heating")
     {
         auto heatingSequence = Sequence::create(
             RotateBy::create(3, 360),
             RotateBy::create(2, 360),
             RotateBy::create(2, 720),
             RotateBy::create(2, 360),
             NULL);

         this->runAction(RepeatForever::create(heatingSequence));

         auto backSaw = this->getChildByTag(5);
         auto fadingSequence = Sequence::create(
             DelayTime::create(5),
             FadeIn::create(2),
             FadeOut::create(2),
             NULL);
         backSaw->runAction(RepeatForever::create(fadingSequence));
     }
     else
     {
         this->runAction(RepeatForever::create(CCRotateBy::create(3, 360)));
     }
}

void Saw::applyMovement()
{
    float movementDuration = _speed;

    float angleInRadians = CC_DEGREES_TO_RADIANS(_angle);
    float impulseScale = 1000;
    auto initialVector = Vec2(1, 0);
    auto impulseDirection = initialVector.rotateByAngle(Vec2(), angleInRadians);

    float movementDirection;
    if (_direction == "left")
        movementDirection = -1;
    else
        movementDirection = 1;

    auto movementDistance = PositioningHelper::getRelativeXPosition(_distance);

    auto angledMovement = Vec2(movementDirection * impulseDirection.x * movementDistance, movementDirection * impulseDirection.y * movementDistance);

    auto movementSequence = CCSequence::create(
        CCMoveBy::create(movementDuration,
        Vec2(angledMovement.x/2, angledMovement.y/2)),
        CCMoveBy::create(movementDuration,
        Vec2(-angledMovement.x/2, -angledMovement.y/2)),
        CCMoveBy::create(movementDuration,
        Vec2(-angledMovement.x/2, -angledMovement.y/2)),
        CCMoveBy::create(movementDuration,
        Vec2(angledMovement.x/2, angledMovement.y/2)),
        NULL);

    this->runAction(RepeatForever::create(movementSequence));
}

void Saw::applyFading()
{
    float fadeOutTime = 1;

    auto fadingSequence = CCSequence::create(
        CCFadeOut::create(fadeOutTime),
        CCDelayTime::create(fadeOutTime + 0.5),
        CCFadeIn::create(fadeOutTime),
        NULL);

    this->runAction(RepeatForever::create(fadingSequence));
}

