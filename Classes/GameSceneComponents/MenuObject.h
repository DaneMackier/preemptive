#ifndef __MENUOBJECT_H__
#define __MENUOBJECT_H__

#include "cocos2d.h"

class MenuObject : public cocos2d::MenuItemImage
{
public:
    int levelNumber;
    MenuObject();
    MenuObject(int level);

    float getWidth();
    float getHeight();

    static MenuObject* createMenuObject(int level, const std::string& normalImage, const std::string& selectedImage, cocos2d::Ref* target, cocos2d::SEL_MenuHandler selector);

};

#endif