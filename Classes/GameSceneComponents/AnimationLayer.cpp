#include "AnimationLayer.h"
#include "Helpers\PositioningHelper.h"
#include "Helpers\AnimationHelper.h"
#include "Helpers\StringHelper.h"



USING_NS_CC;

// Everything in this layer should be added above z-depth of 0
// just to make sure that it's always above the background

Scene* AnimationLayer::createScene()
{
    auto scene = Scene::create();
    auto layer = AnimationLayer::create();
    scene->addChild(layer);
    return scene;
}

bool AnimationLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
   /* cocos2d::MenuItem* backgroundImage = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/background.jpg",
        "UserInterface/MainMenu/background.jpg",
        this,
        menu_selector(MainMenuScene::playGame));
    backgroundImage = PositioningHelper::placeSquareMenuItem(backgroundImage, 0.5, 0.5, 0.5);*/
   


  /*  cocos2d::Menu* pMenu = cocos2d::Menu::create(backgroundImage, NULL);
    pMenu->setPosition(0, 0);
    pMenu->setTag(99);
    this->addChild(pMenu, 5);
*/
    return true;
}
