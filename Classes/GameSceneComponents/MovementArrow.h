#ifndef __MOVEMENTARROW_H__
#define __MOVEMENTARROW_H__

#include "cocos2d.h"

class MovementArrow : public cocos2d::CCSprite
{
public:
    cocos2d::CCTexture2D* onTexture;
    cocos2d::CCTexture2D* offTexture;

    MovementArrow();
    static MovementArrow* createArrow();

    void Enable();
    void Disable();

    float getWidth();
    float getHeight();
};

#endif