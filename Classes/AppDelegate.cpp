#include "AppDelegate.h"
#include "GameScenes/MainGameScene.h"
#include "GameScenes\MainMenuScene.h"
#include "GameScenes\GameOverScene.h"
#include "GameScenes\IntroSplashScene.h"
#include "GameScenes\IntroScene.h"
#include "GameScenes\LevelSelectionScene.h"
#include "GameScenes\MenuScene.h"
#include "GameScenes\WorldSelectionScene.h"
#include "GameScenes\ItemTestScene.h"
#include "GameScenes\LevelEndHud.h"
#include "GameScenes\TutorialScene.h"
#include "GameScenes\StatisticsScene.h"
#include "GameScenes\IntroAnimationScene.h"
#include "GamesceneComponents\MiddleGameSceneLayer.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    srand(time(NULL));

    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("Preemptive");
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = MenuScene::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
