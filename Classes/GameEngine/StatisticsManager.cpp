#include "StatisticsManager.h"
#include "Helpers\DatabaseService.h"
#include "Helpers\StringHelper.h"

#include "LevelManager.h"

#include "cocos\platform\wp8-xaml\cpp\Direct3DInterop.h"

#include "math.h"

using namespace cocos2d;

StatisticsManager::StatisticsManager()
{
    _totalNumberOfDeaths = DatabaseService::getIntegerForKey("totalDeaths");
    if (_totalNumberOfDeaths < 0)
    {
        _totalNumberOfDeaths = 0;
        DatabaseService::setIntegerForKey("totalDeaths", _totalNumberOfDeaths);
    }

    _mostDeaths = DatabaseService::getIntegerForKey("mostDeaths");
    if (_mostDeaths < 0)
    {
        _mostDeaths = 0;
        DatabaseService::setIntegerForKey("mostDeaths", _mostDeaths);
    }

    _leastDeaths = DatabaseService::getIntegerForKey("leastDeaths");
    if (_leastDeaths < 0)
    {
        _leastDeaths = 0;
        DatabaseService::setIntegerForKey("leastDeaths", _leastDeaths);
    }

    _deathsForCurrentLevel = 0;

    setPredictionValues();

    _ratingsvalues.push_back(0);

    Direct3DInterop::getInstance()->OnSetTileInformation(_totalNumberOfDeaths, _leastDeaths, _mostDeaths);
}

void StatisticsManager::setPredictionValues()
{
    _predictionValues.push_back(0.0f);
    _predictionValues.push_back(1.0f);
    _predictionValues.push_back(1.0f);
    _predictionValues.push_back(2.0f);
    _predictionValues.push_back(2.0f);
    _predictionValues.push_back(5.0f);
    _predictionValues.push_back(5.0f);
    _predictionValues.push_back(3.0f);
    _predictionValues.push_back(5.0f);
    _predictionValues.push_back(5.0f);
    _predictionValues.push_back(8.0f);
    _predictionValues.push_back(8.0f);
    _predictionValues.push_back(6.0f);
    _predictionValues.push_back(9.0f);
    _predictionValues.push_back(13.0f);
    _predictionValues.push_back(12.0f);
    _predictionValues.push_back(14.0f);
    _predictionValues.push_back(15.0f);
    _predictionValues.push_back(13.0f);
    _predictionValues.push_back(17.0f);
}

StatisticsManager* StatisticsManager::getInstance()
{
    if (_singletonInstance == nullptr)
    {
        _singletonInstance = new StatisticsManager();
    }

    return _singletonInstance;
}

StatisticsManager* StatisticsManager::_singletonInstance = nullptr;

void StatisticsManager::incrementCurrentLevelDeaths()
{
    _deathsForCurrentLevel++;
    _totalNumberOfDeaths++;

    DatabaseService::setIntegerForKey("totalDeaths", _totalNumberOfDeaths);
    Direct3DInterop::getInstance()->OnSetTileInformation(_totalNumberOfDeaths, _leastDeaths, _mostDeaths);
}

void StatisticsManager::resetCurrentLevelDeaths()
{
    _deathsForCurrentLevel = 0;

    Direct3DInterop::getInstance()->OnSetTileInformation(_totalNumberOfDeaths, _leastDeaths, _mostDeaths);
}

int StatisticsManager::getCurrentLevelDeaths()
{
    return _deathsForCurrentLevel;
}

void StatisticsManager::saveLeastDeaths()
{
    if (_deathsForCurrentLevel < _leastDeaths)
    {
        _leastDeaths = _deathsForCurrentLevel;
        DatabaseService::setIntegerForKey("leastDeaths", _leastDeaths);
    }

    Direct3DInterop::getInstance()->OnSetTileInformation(_totalNumberOfDeaths, _leastDeaths, _mostDeaths);
}

void StatisticsManager::saveMostDeaths()
{
    if (_deathsForCurrentLevel > _mostDeaths)
    {
        _mostDeaths = _deathsForCurrentLevel;
        DatabaseService::setIntegerForKey("mostDeaths", _mostDeaths);
    }

    Direct3DInterop::getInstance()->OnSetTileInformation(_totalNumberOfDeaths, _leastDeaths, _mostDeaths);
}

void StatisticsManager::incrementTotalDeaths()
{
    _totalNumberOfDeaths++;
    DatabaseService::setIntegerForKey("totalDeaths", _totalNumberOfDeaths);
}

int StatisticsManager::getTotalNumberOfDeaths()
{
    return _totalNumberOfDeaths;
}

int StatisticsManager::getMostDeathsOnALevel()
{
    return _mostDeaths;
}

int StatisticsManager::getLeastDeathsOnALevel()
{
    return _leastDeaths;
}

float StatisticsManager::getAverageDeaths()
{
    return (float)_totalNumberOfDeaths / (float)(LevelManager::getInstance()->getFurthestLevel());
}

void StatisticsManager::setRatingValue(int levelNumber)
{
    if (levelNumber > 19) return;
    // rating formula
    auto tempRating = ((float)_predictionValues.at(levelNumber) / (float)_deathsForCurrentLevel); 

    // make sure the deathsForCurrentLevel doesn't throe off the formula
    if (tempRating >= 1)
    {
        tempRating = 3;
    }
    else
    {
        tempRating = ceil(tempRating * 2.0f);
    }

    auto rating = (int)tempRating;

    // clamp rating to 3
    if (rating > 3) rating = 3;

    auto ratingKey = "rating_" + StringHelper::ConvertIntToString(levelNumber);

    CCLog("SetRatingValue for levelNumber:%d as rating:%d", levelNumber, rating);
    DatabaseService::setIntegerForKey(ratingKey.c_str(), rating);
}

int StatisticsManager::getRatingsValue(int levelNumber)
{
    CCLog("GetRatingsValue for levelNumber: %d", levelNumber);

    auto ratingKey = "rating_" + StringHelper::ConvertIntToString(levelNumber);
    auto rating = DatabaseService::getIntegerForKey(ratingKey.c_str());

    if (rating < 0) return 0;

    return rating;
}

int StatisticsManager::getPredictionValue(int levelNumber)
{
    return _predictionValues.at(levelNumber - 1);
}