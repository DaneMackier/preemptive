#ifndef __MIDDLELAYERFOLLOW_H__
#define __MIDDLELAYERFOLLOW_H__

#include "cocos2d.h"

class MiddleLayerFollow : public cocos2d::Follow
{
public:
    MiddleLayerFollow();
    static MiddleLayerFollow* create(cocos2d::Node *followedNode);
private:
    virtual void step(float dt);
};


#endif