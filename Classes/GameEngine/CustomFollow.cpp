#include "CustomFollow.h"
#include "Helpers\PositioningHelper.h"

USING_NS_CC;

// ---------------------------- PUBLIC -------------------------------
CustomFollow::CustomFollow()
{
}

CustomFollow* CustomFollow::create(Node *followedNode)
{
    CustomFollow* customFollow = new CustomFollow(); 

    if (customFollow && customFollow->initWithTarget(followedNode))
    {
        customFollow->autorelease();
        return customFollow;
    }
    CC_SAFE_DELETE(customFollow);
    return nullptr;
}

// ---------------------------- PRIVATE -------------------------------
void CustomFollow::step(float dt)
{
    /*
    SOFT FOLLOW:
    get the current position of the target and the camera,
    then move the camera by the (distance difference) * CONST, where the CONST is a number between 0 \~ 1, you may also adjust your const and factor in delta time to make it independant from frame rate.

    for example, your CONST is 0.5, the target moves right 10 points, the camera only moves 5 points, the next frame, the target did not move, the camera will move another 2.5 points towards the target and so on
    */

    auto cameraTarget = _target->getPosition();
    auto playerPosition = _followedNode->getPosition();
    auto followRectangle = _worldRect;

    float yDelta = cameraTarget.y - playerPosition.y;

    int yOffset = PositioningHelper::getHalfHeight() - 250 - playerPosition.y;

    // yOffset += 1;
    //_y -= 0.5;

    auto offsetTarget = Vec2(0, yOffset);

    float distanceBetweenPlayerAndCameraTarget = cameraTarget.distanceSquared(playerPosition);


    //    if (distanceBetweenPlayerAndCameraTarget > 90000)
    // {



    auto newCameraTarget = cameraTarget.lerp(offsetTarget, 0.05);

    _target->setPosition(newCameraTarget);
    // }
    // auto deltaDistance = cameraTarget.distance(desiredTarget);

    //

    //auto newPosition = Vec2(0, (lerpPosition.y));

    //auto finalCameraPosition = Vec2(newPosition.x, newPosition.y + 10);

    //int yOffset = 10;

    //auto offsetPosition = Vec2(0, yOffset);

    //float distance = offsetPosition.distance(playerPosition);

    //if (!_worldRect.containsPoint(playerPosition))
    //{

    //auto newYPosition = followRectangle.getMidY() + playerPosition.y;

    /*auto newPosition = playerPosition + offsetPosition;
    auto lerpPosition = cameraTarget.lerp(newPosition, 0.05);

    lerpPosition.x = 0;

    */

    //_target->setPosition(lerpPosition);
    //}


    /*
    auto followedNodePosition = _followedNode->getPosition();
    auto currentTargetPosition = _target->getPosition();
    auto newTargetPosition = Vec2(0, -(followedNodePosition.y -
    PositioningHelper::screenBounds.width/2));
    _target->setPosition(newTargetPosition);*/
}