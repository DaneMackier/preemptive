#ifndef __LEVELMANAGER_H__
#define __LEVELMANAGER_H__

#include "cocos2d.h"

#include "GameSceneComponents\GameScene.h"

class LevelManager : public cocos2d::Layer
{
private:
    /*
    Stores the number of levels defined for each level selection
    */
    int _numberOfLevels;

    /*
    Stores the number of the current level being played
    */
    int _currentLevel;

    /*
    Stores the index of the current selected world
    */
    int _currentWorld;

    /*
    Represents the current set of assets to use within the game
    */
    int _currentGameAssetSet;

    /*
    Keeps track of which level and world has been unlocked
    */
    int _furthestWorld;
    int _furthestLevel;

    /*
    Contains the full path to the level to be loaded
    */
    std::string _levelPath;

    /*
    Singleton instance to the levelManager
    */
    static LevelManager* _levelManagerInstance;

public:
    /*
    Contains all the level selection items for the level
    */
    cocos2d::Vector<cocos2d::MenuItem*> levelSelectionIcons;

    LevelManager();
    
    /*
    Returns the total number of levels
    */
    int getNumberOfLevels();

    /*
    Increments the current level number
    */
    void incrementLevel();

    /*
    Decrements the current level number
    */
    void decrementLevel();

    /*
    Sets the currentLevelNumber
    */
    void setLevel(int levelNumber);

    /*
    Returns the number of the currentLevel
    */
    int getCurrentLevel();

    /*
    Returns the current level asset set to use within the main game
    */
    int getCurrentLevelAssetNumber();

    /*
    Sets the number of the selected world
    */
    void setWorld(int worldNumber);

    /*
    Returns the number of the world last selected/current world
    */
    int getCurrentWorld();

    /*
    Returns the furthest level the player has played. Which is made up of
    worldnumber x levelnumber 
    */
    int getFurthestLevel();

    /*
    Sets the furthest level using the current world and level finished
    */
    void setFurthestLevel(int furthestLevel);

    /*
    Returns the furthest world the player has reached
    */
    int getFurthestWorld();

    /*
    Sets the furthest world in the database and the level manager
    */
    void setFurthestWorld(int furthestWorld);

    /*
    Returns a fully qualified path to the selected level
    */
    std::string getLevelPath();

    /*
    Takes all the set variables and returns a path to the current
    Level to play
    */
    std::string buildLevelPath();

    /*
    Dummy callback used when creating levelselectionItems
    */
    void doNothing(cocos2d::Ref* sender);

    /*
    Returns the singleton instance of the levelmanager
    */
    static LevelManager* getInstance();

};


#endif