#include "LevelManager.h"

#include "ThirdPartyHelpers\pugiconfig.h"
#include "ThirdPartyHelpers\pugixml.h"

#include "Helpers\StringHelper.h"
#include "Helpers\PositioningHelper.h"
#include "Helpers\DatabaseService.h"

#include "GameSceneComponents\GameScene.h"

#include <iostream>
#include <math.h>

USING_NS_CC;

LevelManager::LevelManager()
{
    _currentWorld = 1;
   
    if (_currentLevel < 0)
    {
        _currentLevel = 1;
    }

    _furthestWorld = DatabaseService::getIntegerForKey("furthest_world");
    if (_furthestWorld < 1)
    {
        _furthestWorld = 1;
        DatabaseService::setIntegerForKey("furthest_world", _furthestWorld);
    }

    _furthestLevel = DatabaseService::getIntegerForKey("furthest_level");
    if (_furthestLevel < 1)
    {
        _furthestLevel = 1;
        DatabaseService::setIntegerForKey("furthest_level", _furthestLevel);
    }
}

LevelManager* LevelManager::_levelManagerInstance = nullptr;

LevelManager* LevelManager::getInstance()
{
    if (_levelManagerInstance == nullptr)
    {
        _levelManagerInstance = new LevelManager();
    }
    
    return _levelManagerInstance;
}

int LevelManager::getNumberOfLevels()
{
    pugi::xml_document xmlDocument;
    std::string levelSelectionPath = cocos2d::CCFileUtils::sharedFileUtils()->fullPathForFilename("Levels/LevelSelectionDefinition.xml");
    ssize_t bufferSize = 0;
    unsigned char *fileContent = cocos2d::CCFileUtils::sharedFileUtils()->getFileData(levelSelectionPath.c_str(), "r", &bufferSize);
    cocos2d::CCString *ccStr = cocos2d::CCString::createWithData(fileContent, bufferSize);
    if (bufferSize == 0){
        CCLOG("debug cannot read from file %", levelSelectionPath.c_str());
        return 0;
    }

    ssize_t contentSize = ccStr->_string.size() + 1;

    char * fileContentBuffer = new char[contentSize];
    std::copy(ccStr->_string.begin(), ccStr->_string.end(), fileContentBuffer);
    fileContentBuffer[contentSize - 1] = '\0'; // don't forget the terminating 0

    // The block can be allocated by any method; the block is modified during parsing
    pugi::xml_parse_result result = xmlDocument.load_buffer_inplace(fileContentBuffer, contentSize);

    auto numberOfLevels = std::string(xmlDocument.child("LevelSelection").child_value("NumberOfLevels"));

    _numberOfLevels = std::stoi(numberOfLevels);

    return _numberOfLevels;
}

void LevelManager::setLevel(int levelNumber)
{
    _currentLevel = levelNumber;
}

int LevelManager::getCurrentLevel()
{
    return _currentLevel;
}

int LevelManager::getCurrentLevelAssetNumber()
{
    float tempLevel = (float)_currentLevel;
    float tempAssetLevel = tempLevel / 5.0f;
    float currentAssetLevel = ceil(tempAssetLevel);

    if (currentAssetLevel > 4) currentAssetLevel = 4; // clamp a bitch

    CCLog("LevelManager getCurrentLevelAssetNumber %f for _currentLevel %f", currentAssetLevel, tempLevel);
    return (int)currentAssetLevel;
}

void LevelManager::setWorld(int worldNumber)
{
    _currentWorld = worldNumber;
}

int LevelManager::getCurrentWorld()
{
    return _currentWorld;
}

void LevelManager::incrementLevel()
{
    if (_currentLevel < 20)
    {
        if (_currentLevel < _numberOfLevels)
        {
            int furthestLevel = DatabaseService::getIntegerForKey("furthest_level");

            _currentLevel += 1;

            if (_currentLevel > furthestLevel)
            {
                _furthestLevel = _currentLevel;
                DatabaseService::setIntegerForKey("furthest_level", _furthestLevel);
            }
        };
        _levelPath = buildLevelPath();
    }
}

void LevelManager::decrementLevel()
{
    if (_currentLevel > _numberOfLevels)
    {
        _currentLevel -= 1;
    };
    _levelPath = buildLevelPath();
}

std::string LevelManager::buildLevelPath()
{
    return StringHelper::AddNumberToString("Levels/", _currentWorld) + StringHelper::AddNumberToString("/", _currentLevel) + ".xml";
}

std::string LevelManager::getLevelPath()
{
    return buildLevelPath();
}

void LevelManager::doNothing(cocos2d::Ref* sender)
{

}

int LevelManager::getFurthestLevel()
{
    return _furthestLevel;
}

void LevelManager::setFurthestLevel(int furthestLevel)
{
    _furthestLevel = furthestLevel;
    DatabaseService::setIntegerForKey("furthest_level", _furthestLevel);
}

int LevelManager::getFurthestWorld()
{
    return _furthestWorld;
}

void LevelManager::setFurthestWorld(int furthestWorld)
{
    _furthestWorld = furthestWorld;
    DatabaseService::setIntegerForKey("furthest_world", _furthestWorld);
}
