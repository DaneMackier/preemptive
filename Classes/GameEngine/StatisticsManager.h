#pragma once

class StatisticsManager
{
private:

    /*
    Default constructor
    */
    StatisticsManager();

    /*
    singleton pointer to an instance of this class
    */
    static StatisticsManager* _singletonInstance;

    /*
    contains the prediction values for every level
    */
    std::vector<float> _predictionValues;

    /*
    contains all the ratings for each level
    */
    std::vector<int> _ratingsvalues;

    /*
    Contains the total number of deaths
    */
     int _totalNumberOfDeaths;

    /*
    Contains the most deaths on a level
    */
     int _mostDeaths;

    /*
    Contains lest deaths on a level
    */
     int _leastDeaths;

    /*
    Contains average deaths per level
    */
     int _averageDeathsPerLevel;
    
    /*
    Contains deaths for current level
    */
     int _deathsForCurrentLevel;

     /*
     Sets up all the prediction values
     */
     void setPredictionValues();
          
public:

    /*
    Returns the singletonInstance of this class
    */
    static StatisticsManager* getInstance();

    /*
    Increments the current level deaths
    */
    void incrementCurrentLevelDeaths();

    /*
    Resets the current level deaths
    */
    void resetCurrentLevelDeaths();

    /*
    Returns the current level deaths
    */
    int getCurrentLevelDeaths();

    /*
    Set least deaths
    */
    void saveLeastDeaths();

    /*
    set most deaths
    */
    void saveMostDeaths();

    /*
    Increment total deaths
    */
    void incrementTotalDeaths();

    /*
    Returns the total number of deaths
    */
    int getTotalNumberOfDeaths();

    /*
    get most deaths ever on a level
    */
    int getMostDeathsOnALevel();

    /*
    get least deaths ever on a level
    */
    int getLeastDeathsOnALevel();

    /*
    returns the average deaths
    */
    float getAverageDeaths();

    /*
    Saves the rating value for the level passed in
    */
    void setRatingValue(int levelNumber);

    /*
    Gets the rating value for the level number passed in
    */
    int getRatingsValue(int levelNumber);

    /*
    Returns the prediction deaths used for the ratings
    */
    int getPredictionValue(int levelNumber);

};