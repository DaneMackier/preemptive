#ifndef __CUSTOMFOLLOW_H__
#define __CUSTOMFOLLOW_H__

#include "cocos2d.h"

class CustomFollow : public cocos2d::Follow
{
private:
    virtual void step(float dt);
public:
    CustomFollow();
    static CustomFollow* create(cocos2d::Node *followedNode);
};


#endif