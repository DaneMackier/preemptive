#ifndef __SOUNDENGINE_H__
#define __SOUNDENGINE_H__

#include "cocos2d.h"

class SoundEngine
{
public:
    static bool muted;
    static void soundOn();
    static void soundOff();
    static void playEffect(std::string filename);
    static void playEffect(std::string filename, bool loop);
    static void playBackgroundMusic(std::string filename, bool loop);
    static void setEffectsVolume(float volume);
    static void setBackgroundVolume(float volume);
    static void stopAllSounds();
};





#endif