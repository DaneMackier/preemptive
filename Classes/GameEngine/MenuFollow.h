#ifndef __MENUFOLLOW_H__
#define __MENUFOLLOW_H__

#include "cocos2d.h"

class MenuFollow : public cocos2d::Follow
{
public:
    MenuFollow();
    static MenuFollow* create(cocos2d::Node *followedNode, const cocos2d::Rect& rect = cocos2d::Rect::ZERO);
private:
    float _y;
    virtual void step(float dt);
};


#endif