#include "MenuFollow.h"
#include "Helpers\PositioningHelper.h"

USING_NS_CC;

MenuFollow::MenuFollow()
{
    _y = 0;
}

void MenuFollow::step(float dt)
{
    /*
    SOFT FOLLOW:
    get the current position of the target and the camera,
    then move the camera by the (distance difference) * CONST, where the CONST is a number between 0 \~ 1, you may also adjust your const and factor in delta time to make it independant from frame rate.

    for example, your CONST is 0.5, the target moves right 10 points, the camera only moves 5 points, the next frame, the target did not move, the camera will move another 2.5 points towards the target and so on
    */

    auto followedNodePosition = _followedNode->getPosition();
    auto currentTargetPosition = _target->getPosition();
    auto newTargetPosition = Vec2(
        -(followedNodePosition.x - PositioningHelper::screenBounds.width / 2),
        -(followedNodePosition.y -PositioningHelper::screenBounds.height / 2));

    _target->setPosition(newTargetPosition);
}

MenuFollow* MenuFollow::create(Node *followedNode, const Rect& rect)
{
    MenuFollow* customFollow = new MenuFollow();
    if (customFollow && customFollow->initWithTarget(followedNode, rect))
    {
        customFollow->autorelease();
        return customFollow;
    }
    CC_SAFE_DELETE(customFollow);
    return nullptr;
}