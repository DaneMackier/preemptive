#include "SoundEngine.h"
#include "SimpleAudioEngine.h"

bool SoundEngine::muted = false;

void SoundEngine::soundOn()
{
    muted = false;
}

void SoundEngine::soundOff()
{
    muted = true;
    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

void SoundEngine::playEffect(std::string filename)
{
    playEffect(filename, false);
}

void SoundEngine::playEffect(std::string filename, bool loop)
{
    if (!muted){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(filename.c_str(), loop, 1.0f, 1.0f, 1.0f);
    }
}

void SoundEngine::playBackgroundMusic(std::string filename, bool loop)
{
    if (!muted){
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(filename.c_str(), loop);
    }
}

void SoundEngine::setEffectsVolume(float volume)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
}

void SoundEngine::setBackgroundVolume(float volume)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
}

void SoundEngine::stopAllSounds()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
}



