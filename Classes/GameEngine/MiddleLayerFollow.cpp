#include "MiddleLayerFollow.h"
#include "Helpers\PositioningHelper.h"

USING_NS_CC;

MiddleLayerFollow::MiddleLayerFollow()
{
}

void MiddleLayerFollow::step(float dt)
{

    auto cameraTarget = _target->getPosition();
    auto playerPosition = _followedNode->getPosition();
    auto followRectangle = _worldRect;

    float yDelta = cameraTarget.y - playerPosition.y;

    int yOffset = PositioningHelper::getHalfHeight() - 250 - playerPosition.y;

    auto offsetTarget = Vec2(0, yOffset);

    float distanceBetweenPlayerAndCameraTarget = cameraTarget.distanceSquared(playerPosition);

    auto newCameraTarget = cameraTarget.lerp(offsetTarget, 0.05);

    _target->setPosition(_target->getPositionX(), newCameraTarget.y *0.97);   
}

MiddleLayerFollow* MiddleLayerFollow::create(Node *followedNode)
{
    MiddleLayerFollow* middleLayerFollow = new MiddleLayerFollow();

    if (middleLayerFollow && middleLayerFollow->initWithTarget(followedNode))
    {
        middleLayerFollow->autorelease();
        return middleLayerFollow;
    }
    CC_SAFE_DELETE(middleLayerFollow);
    return nullptr;
}