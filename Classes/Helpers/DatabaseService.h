#ifndef __DATABASESERVICE_H__
#define __DATABASESERVICE_H__

#include "cocos2d.h"

class DatabaseService
{
public:
    static int getIntegerForKey(const char* key);
    static void setIntegerForKey(const char* key, int number);
};

#endif