#include "PhysicsHelper.h"
#include "PositioningHelper.h"
#include "GameSceneComponents\GameItem.h"



void PhysicsHelper::createBoundaryFixture(b2World* world, b2Vec2 bodyDefPosition, b2Vec2 edgeShapePosition, b2Vec2 dimensions)
{
    b2BodyDef boundaryBodyDef;

    auto userData = new GameItem();
    userData->tag = "Boundary";

    boundaryBodyDef.position.Set(bodyDefPosition.x, bodyDefPosition.y);

    b2Body* groundBody = world->CreateBody(&boundaryBodyDef);
    b2EdgeShape groundEdge;
    b2FixtureDef boxShapeDef;
    boxShapeDef.shape = &groundEdge;

    // b2Vec2(PositioningHelper::screenBounds.width / PTM_RATIO, 0)

    groundEdge.Set(edgeShapePosition, dimensions);
    groundBody->CreateFixture(&boxShapeDef);
    groundBody->SetUserData(userData);
}

void PhysicsHelper::createPlayerPhysics(b2World* world, Player* player, b2Body* playerPhysicsBody)
{
   
    playerPhysicsBody->SetLinearDamping(2);

    b2CircleShape playerItemShape;
    playerItemShape.m_radius = (player->getWidth() / 2.25) / PTM_RATIO;

    b2FixtureDef playerShapeDef;
    playerShapeDef.shape = &playerItemShape;
    playerShapeDef.density = 0.2f;
    playerShapeDef.friction = 1.0f;
    playerShapeDef.restitution = 0.0f;
    playerPhysicsBody->CreateFixture(&playerShapeDef);

}

b2BodyDef PhysicsHelper::createPlayerBodyDef(Player* player)
{
    b2BodyDef playerBodyDef;
    playerBodyDef.type = b2_dynamicBody;
    playerBodyDef.position.Set(player->getPosition().x / PTM_RATIO,
        player->getPosition().y / PTM_RATIO);
    playerBodyDef.userData = player;

    return playerBodyDef;
}

void PhysicsHelper::createGameItemFixture(b2World* world, GameItem* gameItem)
{
    b2Body* gameItemBody;

    b2BodyDef gameItemBodyDefinition;
    gameItemBodyDefinition.type = b2_staticBody;
    gameItemBodyDefinition.position.Set(gameItem->getPosition().x / PTM_RATIO,
        gameItem->getPosition().y / PTM_RATIO);
    gameItemBodyDefinition.userData = gameItem;
    gameItemBody = world->CreateBody(&gameItemBodyDefinition);

    b2CircleShape gameItemShape;
    gameItemShape.m_radius = (gameItem->getWidth() / 2.25) / PTM_RATIO;

    b2FixtureDef gameItemFixture;
    gameItemFixture.shape = &gameItemShape;
    gameItemFixture.density = 0.1f;
    gameItemFixture.friction = 0.1f;
    gameItemFixture.restitution = 0.1f;
    gameItemBody->CreateFixture(&gameItemFixture);
}