#include "StringHelper.h"

std::string StringHelper::ConvertIntToString(int number)
{
    // Refactor this into a string helper
    std::ostringstream stringStream;
    stringStream<< number;
    return stringStream.str();
}

std::string StringHelper::AddNumberToString(std::string value, int number)
{
    // Refactor this into a string helper
    std::ostringstream stringStream;
    stringStream << value << number;
    return stringStream.str();
}
