#ifndef __SOUNDENGINE_H__
#define __SOUNDENGINE_H__

#include "cocos2d.h"

class SoundEngine
{
public:
    static bool muted;
    static void soundOn();
    static void soundOff();
    static void playEffect(std::string filename);
};





#endif