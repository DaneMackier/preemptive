#include "SoundEngine.h"

bool SoundEngine::muted = false;

void SoundEngine::soundOn()
{
    muted = false;
}

void SoundEngine::soundOff()
{
    muted = true;
}

void SoundEngine::playEffect(std::string filename)
{
    if (!muted){
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(
            filename.c_str());
    }
}