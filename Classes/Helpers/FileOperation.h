#ifndef __FILE_OPERATION__
#define __FILE_OPERATION__

#include <string>

class FileOperation 
{
public:
	static void saveFile(void);
	static void readFile(void);
    
    static void saveHighScore(int highScore);
    static int getHighScore();

	static std::string getFilePath();
};

#endif
