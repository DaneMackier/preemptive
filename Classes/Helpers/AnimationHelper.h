#ifndef __ANIMATIONHELPER_H__
#define __ANIMATIONHELPER_H__

#include "cocos2d.h"

class AnimationHelper
{
public:
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getFallInAnimation(cocos2d::CCNode* item, float delay = 0, float fallSpeed = 0.1);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getVerticalFallInAnimation(cocos2d::CCNode* item, float delay = 0, float fallSpeed = 0.1);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getShakeAction(float shakeSeverity);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getLeftToRightShakeAnimation(float shakeSeverity);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getUpDownShakeAnimation(float shakeSeverity);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getScaleBurst(float burstSize);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getIntroFlicker();
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getLightPulse(float fadeInTime, float fadeOutTime);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getScalePulse(float scaleInTime, float scaleOutTime);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getSlideInLeft(float slideInTime, cocos2d::Vec2 position);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getSlideInRight(float slideInTime, cocos2d::Vec2 position);
    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getPopUpSequence(float popUpTimen, cocos2d::CCNode* node);

    static cocos2d::Vector<cocos2d::CCFiniteTimeAction*> getParalaxSlideInToZero(float paralaxTime, int finalPosition);


};


#endif