#ifndef __XMLHELPER_H__
#define __XMLHELPER_H__

#include "ThirdPartyHelpers\pugiconfig.h"
#include "ThirdPartyHelpers\pugixml.h"

#include "GameSceneComponents\LevelItem.h"

#include "Helpers\PositioningHelper.h"

#include "Box2D\Box2D.h"

class XMLHelper
{
public:
    static std::vector<LevelItem*> getLevelSprites(std::string filename);
    static std::vector<b2Vec2> getLevelMovements(std::string filename);
};

#endif