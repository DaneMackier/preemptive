#include "PositioningHelper.h"

cocos2d::MenuItem* PositioningHelper::placeMenuItem(
    cocos2d::MenuItem* menuItem,
    float relativeXPostition,
    float relativeYPosition,
    float relativeWidth,
    float relativeHeight)
{
    float originalWidth = (menuItem->getContentSize().width);
    float originalHeight = (menuItem->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeWidth) / originalWidth;
    float finalYScale = (screenBounds.height * relativeHeight) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    menuItem->setScale(finalXScale, finalYScale);
    menuItem->setPosition(screenBounds.width * relativeXPostition, screenBounds.height * relativeYPosition);

    return menuItem;
}

cocos2d::CCSprite* PositioningHelper::placeGameItem(
    cocos2d::CCSprite* sprite,
    float relativeXPostition,
    float relativeYPosition,
    float relativeWidth,
    float relativeHeight)
{
    float originalWidth = (sprite->getContentSize().width);
    float originalHeight = (sprite->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeWidth) / originalWidth;
    float finalYScale = (screenBounds.height * relativeHeight) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    sprite->setScale(finalXScale, finalYScale);
    sprite->setPosition(screenBounds.width * relativeXPostition, screenBounds.height * relativeYPosition);

    return sprite;
}

cocos2d::Node* PositioningHelper::scaleNode(
    cocos2d::Node* node,
    float relativeXPostition,
    float relativeYPosition,
    float relativeWidth,
    float relativeHeight)
{
    float originalWidth = (node->getContentSize().width);
    float originalHeight = (node->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeWidth) / originalWidth;
    float finalYScale = (screenBounds.height * relativeHeight) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    node->setScale(finalXScale, finalYScale);
   // node->setPosition(screenBounds.width * relativeXPostition, screenBounds.height * relativeYPosition);

    return node;
}


cocos2d::CCSprite* PositioningHelper::placeSquareGameItem(
    cocos2d::CCSprite* sprite,
    float relativeXPostition,
    float relativeYPosition,
    float relativeSideDimension)
{
    float originalWidth = (sprite->getContentSize().width);
    float originalHeight = (sprite->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeSideDimension) / originalWidth;
    float finalYScale = (screenBounds.width * relativeSideDimension) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    sprite->setScale(finalXScale, finalYScale);
    sprite->setPosition(screenBounds.width * relativeXPostition, screenBounds.height * relativeYPosition);

    return sprite;
}

cocos2d::CCMenuItem* PositioningHelper::placeSquareMenuItem(
    cocos2d::CCMenuItem* sprite,
    float relativeXPostition,
    float relativeYPosition,
    float relativeSideDimension)
{
    float originalWidth = (sprite->getContentSize().width);
    float originalHeight = (sprite->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeSideDimension) / originalWidth;
    float finalYScale = (screenBounds.width * relativeSideDimension) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    sprite->setScale(finalXScale, finalYScale);
    sprite->setPosition(screenBounds.width * relativeXPostition, screenBounds.height * relativeYPosition);

    return sprite;
}

cocos2d::ui::Widget* PositioningHelper::placeSquareWidget(
    cocos2d::ui::Widget* item,
    float relativeXPosition,
    float relativeYPosition,
    float relativeSideDimension)
{
    float originalWidth = (item->getContentSize().width);
    float originalHeight = (item->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeSideDimension) / originalWidth;
    float finalYScale = (screenBounds.width * relativeSideDimension) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    item->setScale(finalXScale, finalYScale);
    item->setPosition(cocos2d::Vec2(screenBounds.width * relativeXPosition, screenBounds.height * relativeYPosition));

    return item;
}

cocos2d::ui::Widget* PositioningHelper::placeWidget(
    cocos2d::ui::Widget* item,
    float relativeXPosition,
    float relativeYPosition,
    float relativeWidth,
    float reativeHeight)
{
    float originalWidth = (item->getContentSize().width);
    float originalHeight = (item->getContentSize().height);
    float finalXScale = (screenBounds.width * relativeWidth) / originalWidth;
    float finalYScale = (screenBounds.width * reativeHeight) / originalHeight;

    // returns the animation scale twice as big, to make the drop in effect
    // a bit more noticeable
    item->setScale(finalXScale, finalYScale);
    item->setPosition(cocos2d::Vec2(screenBounds.width * relativeXPosition, screenBounds.height * relativeYPosition));

    return item;
}


cocos2d::LabelTTF* PositioningHelper::placeLabel(
    cocos2d::LabelTTF* label,
    float relativeXPosition,
    float relativeYPosition)
{
    label->setPosition(screenBounds.width * relativeXPosition, screenBounds.height * relativeYPosition);
    return label;
}

cocos2d::Vec2 PositioningHelper::getRelativePosition(float relativeXPosition, float relativeYPosition)
{
    auto relativeVector = cocos2d::Vec2(
        screenBounds.width * relativeXPosition,
        screenBounds.height * relativeYPosition);

    return relativeVector;
}


cocos2d::Size getBounds(cocos2d::CCNode* item)
{
    auto bounds = cocos2d::Size(
        item->getBoundingBox().getMaxX() - item->getBoundingBox().getMinX(),
        item->getBoundingBox().getMaxY() - item->getBoundingBox().getMinY());

    return bounds;
}

float PositioningHelper::getVisibleHeight()
{
    return screenBounds.height;
}

float PositioningHelper::getVisibleWidth()
{
    return screenBounds.width;
}

float PositioningHelper::getHalfWidth()
{
    return screenBounds.width/2;
}

float PositioningHelper::getHalfHeight()
{
    return screenBounds.height/2;
}

float PositioningHelper::getRelativeXPosition(float xPosition)
{
    return xPosition * screenBounds.width;
}

float PositioningHelper::getRelativeYPosition(float yPosition)
{
    return yPosition * screenBounds.height;
}

float PositioningHelper::getXScale(cocos2d::ui::Widget* item, float relativeWidth)
{
    float originalWidth = (item->getContentSize().width);
    float finalXScale = (screenBounds.width * relativeWidth) / originalWidth;

    return finalXScale;
}

float PositioningHelper::getYScale(cocos2d::ui::Widget* item, float relativeHeight)
{
    float originalHeight = (item->getContentSize().height);
    float finalYScale = (screenBounds.width * relativeHeight) / originalHeight;

    return finalYScale;
}