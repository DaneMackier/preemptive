#include "XMLHelper.h"

#include "GameSceneComponents\Saw.h"
#include "GameSceneComponents\Shooter.h"
#include "GameSceneComponents\AcidDripper.h"

std::vector<LevelItem*> XMLHelper::getLevelSprites(std::string filename)
{
    std::vector<LevelItem*> levelSprites;

    pugi::xml_document xmlDocument;
    std::string levelSelectionPath = cocos2d::CCFileUtils::sharedFileUtils()->fullPathForFilename(filename);
    ssize_t bufferSize = 0;
    unsigned char *fileContent = cocos2d::CCFileUtils::sharedFileUtils()->getFileData(levelSelectionPath.c_str(), "r", &bufferSize);
    cocos2d::CCString *ccStr = cocos2d::CCString::createWithData(fileContent, bufferSize);
    if (bufferSize == 0){
        CCLOG("debug cannot read from file %", levelSelectionPath.c_str());
        return levelSprites;
    }

    ssize_t contentSize = ccStr->_string.size() + 1;

    char * fileContentBuffer = new char[contentSize];
    std::copy(ccStr->_string.begin(), ccStr->_string.end(), fileContentBuffer);
    fileContentBuffer[contentSize - 1] = '\0'; // don't forget the terminating 0

    // The block can be allocated by any method; the block is modified during parsing
    pugi::xml_parse_result result = xmlDocument.load_buffer_inplace(fileContentBuffer, contentSize);

    auto root = xmlDocument.child("Level").child("LevelItems");

    int sawNumber = rand() % 3 + 1;

    for (pugi::xml_node levelItem = root.first_child(); levelItem; levelItem = levelItem.next_sibling())
    {
        auto kind = std::string(levelItem.name());
        auto xPosition = std::stof(levelItem.attribute("x").value());
        auto yPosition = std::stof(levelItem.attribute("y").value());
        int angle = 0;
        
        LevelItem* tempLevelItem;

        std::string type;
        float sawDistance = 0;
        float sawSpeed = 0;

        if (kind == "Saw")
        {
            type = std::string(levelItem.attribute("type").value());
            angle = std::stoi(levelItem.attribute("angle").value());
            sawDistance = std::stof(levelItem.attribute("distance").value());
            sawSpeed = std::stof(levelItem.attribute("speed").value());
            std::string special = std::string(levelItem.attribute("special").value());
            std::string _startDirection = std::string(levelItem.attribute("startDirection").value());

            tempLevelItem = Saw::createSaw(kind, type, angle, sawDistance, sawSpeed, special, _startDirection);
        }
        else if (kind == "Shooter")
        {
            type = std::string(levelItem.attribute("type").value());
            angle = std::stoi(levelItem.attribute("angle").value());
            int shooterSpeed = std::stoi(levelItem.attribute("speed").value());
            float secondsBetweenShots = std::stof(levelItem.attribute("secondsBetweenShots").value());
            tempLevelItem = Shooter::createShooter(kind, type, angle, shooterSpeed, secondsBetweenShots);
        }
        else if (kind == "AcidDripper")
        {
            float secondsBetweenDrops = std::stof(levelItem.attribute("secondsBetweenDrops").value());
            type = std::string(levelItem.attribute("type").value());
            tempLevelItem = AcidDripper::createDripper(kind, type, secondsBetweenDrops);
        }
        else
        {
            tempLevelItem = LevelItem::createLevelItem(kind);
        }
        
        tempLevelItem = (LevelItem*)PositioningHelper::placeSquareGameItem(tempLevelItem, xPosition, yPosition, 0.2);

        tempLevelItem->tag = kind;

        levelSprites.push_back(tempLevelItem);
    }

    return levelSprites;
}

std::vector<b2Vec2> XMLHelper::getLevelMovements(std::string filename)
{
    std::vector<b2Vec2> levelMovements;

    pugi::xml_document xmlDocument;
    std::string levelSelectionPath = cocos2d::CCFileUtils::sharedFileUtils()->fullPathForFilename(filename);
    ssize_t bufferSize = 0;
    unsigned char *fileContent = cocos2d::CCFileUtils::sharedFileUtils()->getFileData(levelSelectionPath.c_str(), "r", &bufferSize);
    cocos2d::CCString *ccStr = cocos2d::CCString::createWithData(fileContent, bufferSize);
    if (bufferSize == 0){
        CCLOG("debug cannot read from file %", levelSelectionPath.c_str());
        return levelMovements;
    }

    ssize_t contentSize = ccStr->_string.size() + 1;

    char * fileContentBuffer = new char[contentSize];
    std::copy(ccStr->_string.begin(), ccStr->_string.end(), fileContentBuffer);
    fileContentBuffer[contentSize - 1] = '\0'; // don't forget the terminating 0

    // The block can be allocated by any method; the block is modified during parsing
    pugi::xml_parse_result result = xmlDocument.load_buffer_inplace(fileContentBuffer, contentSize);

    auto root = xmlDocument.child("Level").child("Movements");

    for (pugi::xml_node levelItem = root.first_child(); levelItem; levelItem = levelItem.next_sibling())
    {
        auto xVelocity = std::stof(levelItem.attribute("x").value());
        auto yVelocity = std::stof(levelItem.attribute("y").value());

        auto movementVector = b2Vec2(xVelocity, yVelocity);
       
        levelMovements.push_back(movementVector);
    }

    return levelMovements;
}