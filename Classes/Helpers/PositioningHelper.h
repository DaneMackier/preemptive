#ifndef __POSITIONINGHELPER_H__
#define __POSITIONINGHELPER_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"

class PositioningHelper
{
public:
    static cocos2d::Size screenBounds;

    static cocos2d::MenuItem* placeMenuItem(
        cocos2d::MenuItem* item,
        float relativeXPosition,
        float relativeYPosition,
        float relativeWidth,
        float relativeHeight);

    static cocos2d::CCSprite* placeGameItem(
        cocos2d::CCSprite* item,
        float relativeXPosition,
        float relativeYPosition,
        float relativeWidth,
        float relativeHeight);

    static cocos2d::Node* scaleNode(
        cocos2d::Node* node,
        float relativeXPosition,
        float relativeYPosition,
        float relativeWidth,
        float relativeHeight);

    static cocos2d::CCSprite* placeSquareGameItem(
        cocos2d::CCSprite* item,
        float relativeXPosition,
        float relativeYPosition,
        float relativeSideDimension);

    static cocos2d::MenuItem* placeSquareMenuItem(
        cocos2d::MenuItem* item,
        float relativeXPosition,
        float relativeYPosition,
        float relativeSideDimension);

    static cocos2d::ui::Widget* placeSquareWidget(
        cocos2d::ui::Widget* item,
        float relativeXPosition,
        float relativeYPosition,
        float relativeSideDimension);

    static cocos2d::ui::Widget* placeWidget(
        cocos2d::ui::Widget* item,
        float relativeXPosition,
        float relativeYPosition,
        float relativeWidth,
        float realtiveHeight);

    static cocos2d::LabelTTF* placeLabel(
        cocos2d::LabelTTF* item,
        float relativeXPosition,
        float relativeYPosition);

    static cocos2d::Vec2 getRelativePosition(float relativeXPosition, float relativeYPosition);

    static cocos2d::Size getWidth(cocos2d::CCNode* item);

    static float getVisibleWidth();
    static float getVisibleHeight();

    static float getHalfWidth();
    static float getHalfHeight();

    static float getRelativeXPosition(float xPosition);
    static float getRelativeYPosition(float yPosition);

    static float getXScale(cocos2d::ui::Widget* item, float relativeX);
    static float getYScale(cocos2d::ui::Widget* item, float relativeY);
};


#endif