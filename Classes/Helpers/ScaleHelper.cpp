#include "ScaleHelper.h"

cocos2d::CCNode* ScaleHelper::ScaleToFit(cocos2d::CCNode* node, cocos2d::Vec2 targetBounds)
{
    // set scale to what we want it to be using the following formula
    // finalScale = (size_we_want)/(actual_size)
    float finalXScale = targetBounds.x / (node->getContentSize().width);
    float finalYScale = targetBounds.y / (node->getContentSize().height);
    node->setScale(finalXScale, finalYScale);

    return node;
}

