// to enable CCLOG()
#define COCOS2D_DEBUG 1

#include "cocos2d.h"
#include "FileOperation.h"
#include "StringHelper.h"
#include <stdio.h>

using namespace std;

void FileOperation::saveFile()
{
	string path = getFilePath();
	FILE *fp = fopen(path.c_str(), "w");

	if (! fp)
	{
		CCLOG("can not create file %s", path.c_str());
		return;
	}

	fputs("file example", fp);
	fclose(fp);
}

void FileOperation::saveHighScore(int highScore)
{
    string path = getFilePath();
    FILE *fp = fopen(path.c_str(), "w");

    if (!fp)
    {
        CCLOG("can not create file %s", path.c_str());
        return;
    }

    fputs(StringHelper::ConvertIntToString(highScore).c_str(), fp);
    fclose(fp);
}

int FileOperation::getHighScore()
{
    string path = getFilePath();
    FILE *fp = fopen(path.c_str(), "r");
    char buf[50] = { 0 };

    if (!fp)
    {
        CCLOG("can not open file %s", path.c_str());
        return 0;
    }

    fgets(buf, 50, fp);

    int highScore = atoi(buf);
    CCLOG("__HighScoreRetreived: %s", buf);
    fclose(fp);

    return highScore;
}

void FileOperation::readFile()
{
	string path = getFilePath();
	FILE *fp = fopen(path.c_str(), "r");
	char buf[50] = {0};

	if (! fp)
	{
		CCLOG("can not open file %s", path.c_str());
		return;
	}

	fgets(buf, 50, fp);

    int highScore = atoi(buf);
	CCLOG("%s", buf);
	fclose(fp);
}

string FileOperation::getFilePath()
{
	string path("");

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	// In android, every programe has a director under /data/data.
	// The path is /data/data/ + start activity package name.
	// You can save application specific data here.
	path.append("/data/data/org.cocos2dx.application/tmpfile");
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	// You can save file in anywhere if you have the permision.
	path.append("D:/tmpfile");
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	path = "highScore";

#ifdef _TRANZDA_VM_
	// If runs on WoPhone simulator, you should insert "D:/Work7" at the
	// begin. We will fix the bug in no far future.
	path = "D:/Work7" + path;
	path.append("tmpfile");
#endif

#endif

	return path;
}