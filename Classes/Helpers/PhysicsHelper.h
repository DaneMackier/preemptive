#ifndef __PHYSICSHELPER_H__
#define __PHYSICSHELPER_H__

#include "Box2D\Box2D.h"

#include "GameSceneComponents\Player.h"

#define PTM_RATIO 32.0

class PhysicsHelper
{
public:

    /*
    Creates a fixture in the box2d world
    */
    static void createBoundaryFixture(b2World* world, b2Vec2 bodyDefPosition, b2Vec2 edgeShapePosition, b2Vec2 dimensions);

    /*
    Adds all the player physics properties required in the game
    */
    static void createPlayerPhysics(b2World* world, Player* player, b2Body* playerPhysicsBody);

    /*
    Creates a body definition to be used when creating the fixture in the 
    physics world.
    */
    static b2BodyDef createPlayerBodyDef(Player* player);

    /*
    Creates a fixture in the world for the gameItem passed in
    */
    static void createGameItemFixture(b2World* world, GameItem* gameItem);
};


#endif