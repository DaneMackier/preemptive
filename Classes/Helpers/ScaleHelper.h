#ifndef __SCALEHELPER_H__
#define __SCALEHELPER_H__

#include "cocos2d.h"

class ScaleHelper
{
public:

    /// Scales node to fit targetBounds
    static cocos2d::CCNode* ScaleToFit(cocos2d::CCNode* node, cocos2d::Vec2 targetBounds);
};


#endif