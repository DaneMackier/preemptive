#include "AnimationHelper.h"

USING_NS_CC;

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getFallInAnimation(cocos2d::CCNode* item, float delay, float fallSpeed)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;
    
    actions.pushBack(cocos2d::CCDelayTime::create(delay));
   // actions.pushBack(cocos2d::CCScaleTo::create(0, item->getScaleX() * 2, item->getScaleY() * 2));
    //actions.pushBack(cocos2d::CCMoveBy::create(fallSpeed, cocos2d::Vec2(0, 20)));
    actions.pushBack(cocos2d::CCScaleTo::create(fallSpeed, item->getScaleX()/2, item->getScaleY()/2));

    actions.pushBack(getShakeAction(15));

    return actions;
}


cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getVerticalFallInAnimation(cocos2d::CCNode* item, float delay, float fallSpeed)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    float originalY = item->getPositionY();
    item->setPositionY(originalY + 100);
    actions.pushBack(cocos2d::CCDelayTime::create(delay));

    //actions.pushBack(cocos2d::CCScaleTo::create(fallSpeed, item->getScaleX() / 2, item->getScaleY() / 2));
    actions.pushBack(cocos2d::CCMoveTo::create(fallSpeed, cocos2d::Vec2(item->getPositionX(), originalY)));
    actions.pushBack(getShakeAction(15));

    return actions;
}


cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getShakeAction(float shakeSeverity)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(CCMoveBy::create(0.01, Vec2(shakeSeverity, shakeSeverity)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(-shakeSeverity, -shakeSeverity)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(-shakeSeverity, 0)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(shakeSeverity, 0)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(shakeSeverity, shakeSeverity)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(-shakeSeverity, -shakeSeverity)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(0, shakeSeverity)));
    actions.pushBack(CCMoveBy::create(0.01, Vec2(0, -shakeSeverity)));

    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getLeftToRightShakeAnimation(float shakeSeverity)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(shakeSeverity, 0)));
    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(-shakeSeverity, 0)));
    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(-shakeSeverity, 0)));
    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(shakeSeverity, 0)));

    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getUpDownShakeAnimation(float shakeSeverity)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(0,shakeSeverity)));
    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(0,-shakeSeverity)));
    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(0,-shakeSeverity)));
    actions.pushBack(cocos2d::CCMoveBy::create(0.1, cocos2d::Vec2(0,shakeSeverity)));

    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getScaleBurst(float burstSize)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCScaleBy::create(0.01,  burstSize,  burstSize));
    actions.pushBack(cocos2d::CCScaleBy::create(0.01, - burstSize, - burstSize));
    actions.pushBack(cocos2d::CCScaleBy::create(0.01, - burstSize, - burstSize));
    actions.pushBack(cocos2d::CCScaleBy::create(0.01,  burstSize,  burstSize));
    
    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getIntroFlicker()
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCFadeOut::create(0.05));
    actions.pushBack(cocos2d::CCFadeIn::create(0.05));
    actions.pushBack(cocos2d::CCFadeOut::create(0.1));
    actions.pushBack(cocos2d::CCFadeIn::create(0.1));
    actions.pushBack(cocos2d::CCFadeOut::create(0.55));
    actions.pushBack(cocos2d::CCFadeIn::create(0.75));
    
    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getLightPulse(float fadeInTime, float fadeOutTime)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCFadeIn::create(fadeInTime));
    actions.pushBack(cocos2d::CCFadeOut::create(fadeOutTime));

    return actions;
}


cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getScalePulse(float scaleInTime, float scaleOutTime)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCScaleBy::create(scaleInTime, 1.5));
    actions.pushBack(cocos2d::CCScaleBy::create(scaleOutTime, 0.5));

    actions.pushBack(cocos2d::CCScaleBy::create(scaleInTime, 1.5));
    actions.pushBack(cocos2d::CCScaleBy::create(scaleOutTime, 0.5));

    actions.pushBack(cocos2d::CCScaleBy::create(scaleInTime, 1.5));
    actions.pushBack(cocos2d::CCScaleBy::create(scaleOutTime, 0.5));


    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getSlideInLeft(float slideInTime, cocos2d::Vec2 position)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCMoveTo::create(slideInTime, cocos2d::Vec2(position.x - 50, position.y)));
    actions.pushBack(cocos2d::CCMoveTo::create(slideInTime / 2, cocos2d::Vec2(position.x, position.y)));

    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getSlideInRight(float slideInTime, cocos2d::Vec2 position)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    actions.pushBack(cocos2d::CCMoveTo::create(slideInTime, cocos2d::Vec2(position.x + 50, position.y)));
    actions.pushBack(cocos2d::CCMoveTo::create(slideInTime/2, cocos2d::Vec2(position.x, position.y)));

    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getPopUpSequence(float popUpTime, cocos2d::CCNode* node)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;

    float originalScale = node->getScale();
    node->setScale(0.1);

    actions.pushBack(CCScaleTo::create(popUpTime, originalScale + 0.2));
    actions.pushBack(CCScaleTo::create(popUpTime/2, originalScale - 0.1));
    actions.pushBack(CCScaleTo::create(popUpTime, originalScale));

    return actions;
}

cocos2d::Vector<cocos2d::CCFiniteTimeAction*> AnimationHelper::getParalaxSlideInToZero(float paralaxTime, int direction)
{
    cocos2d::Vector<cocos2d::CCFiniteTimeAction* > actions;
     
    actions.pushBack(CCMoveTo::create(paralaxTime / 4, Vec2(450 * direction, 0)));//38.4
    actions.pushBack(CCMoveTo::create(paralaxTime / 4, Vec2(250 * direction, 0)));//23
    actions.pushBack(CCMoveTo::create(paralaxTime / 4, Vec2(50 * direction, 0)));//15
    actions.pushBack(CCMoveTo::create(paralaxTime / 4, Vec2(0, 0)));//7
    //actions.pushBack(callbackAction);

    return actions;
}
