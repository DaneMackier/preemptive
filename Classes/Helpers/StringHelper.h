#ifndef __STRINGHELPER_H__
#define __STRINGHELPER_H__

#include <string>
#include <sstream>

class StringHelper
{
public:
    static std::string ConvertIntToString(int number);
    static std::string AddNumberToString(std::string value, int number);
};

#endif