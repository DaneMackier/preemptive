#include "DatabaseService.h"

int DatabaseService::getIntegerForKey(const char* key)
{
    int value = cocos2d::CCUserDefault::sharedUserDefault()->getIntegerForKey(key);
    return value;
}

void DatabaseService::setIntegerForKey(const char* key, int number)
{
    cocos2d::CCUserDefault::sharedUserDefault()->setIntegerForKey(key, number);
    cocos2d::CCUserDefault::sharedUserDefault()->flush();
}