#include "PauseMenuHud.h"
#include "MainGameScene.h"
#include "MenuScene.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\XMLHelper.h"
#include "Helpers\StringHelper.h"

#include "GameSceneComponents\TagDefinitions.h"

#include "GameEngine\LevelManager.h"
#include "GameEngine\StatisticsManager.h"
#include "GameEngine\SoundEngine.h"

USING_NS_CC;
using namespace ui;

Scene* PauseMenuHud::createScene()
{
    auto scene = Scene::create();
    auto layer = PauseMenuHud::create();
    layer->gameScene = "PauseMenuHud";
    scene->addChild(layer);
    return scene;
}

bool PauseMenuHud::init()
{
    if (!Layer::init())
    {
        return false;
    }

    this->setKeypadEnabled(true);
    this->setKeyboardEnabled(true);

    _playable = true;
    _paused = false;
    _introductionShown = false;

    _numberOfMovements = 16;
    _correctMovementsOnScreen = 0;
    definedLevelMovements = XMLHelper::getLevelMovements(LevelManager::getInstance()->getLevelPath());
    _playoutMovementIndex = 0;
        
    initializeArrowRotationValues();
    
    initializeCorrectMovements();
    initializePlayoutMovements();

    placeHudElements();

    return true;
}

void PauseMenuHud::initializeArrowRotationValues()
{
    // -------------- SEED ARROW ROTATIONS ------------
    _arrowRotations.insert(std::pair<std::string, float>("left", -90));
    _arrowRotations.insert(std::pair<std::string, float>("topLeft", -45));
    _arrowRotations.insert(std::pair<std::string, float>("up", 0));
    _arrowRotations.insert(std::pair<std::string, float>("topRight", 45));
    _arrowRotations.insert(std::pair<std::string, float>("right", 90));
}

void PauseMenuHud::placeHudElements()
{
    auto pauseButton = cocos2d::MenuItemImage::create(
        "UserInterface/PauseMenu/pause.png",
        "UserInterface/PauseMenu/pause.png",
        this,
        menu_selector(PauseMenuHud::onPauseGame));
    pauseButton = (MenuItemImage*)PositioningHelper::placeSquareMenuItem(pauseButton, 0.05, 0.965, 0.08);

    auto hudDock = cocos2d::MenuItemImage::create(
        "UserInterface/PauseMenu/hud.png",
        "UserInterface/PauseMenu/hud.png",
        this,
        menu_selector(PauseMenuHud::onPauseGame));
    hudDock = (MenuItemImage*)PositioningHelper::placeMenuItem(hudDock, 0.5, 0.96, 1.0, 0.08);

    _timesDied = Button::create(
        "UserInterface/PauseMenu/ButtonBackdrop.png",
        "UserInterface/PauseMenu/ButtonBackdrop.png");
    _timesDied = (Button*)PositioningHelper::placeSquareWidget(
        _timesDied,
        0.05, 0.9,
        0.1);
    _timesDied->setTitleText("0");
    _timesDied->setTitleFontSize(45);
    this->addChild(_timesDied, 1);

    cocos2d::Menu* pMenu = cocos2d::Menu::create(hudDock, pauseButton, NULL);
    pMenu->setPosition(0, 0);
    this->addChild(pMenu, 5);
}

void PauseMenuHud::showPauseMenu()
{

    float relativeXPosition = 0.85;
    float relativeButtonSides = 0.3; 

    _alphaBackground = cocos2d::Sprite::create("UserInterface/BlackLayover.png");
    _alphaBackground = PositioningHelper::placeGameItem(_alphaBackground, 0.5, 0.5, 1, 1);
    _alphaBackground->setOpacity(80);
    this->addChild(_alphaBackground);

    int worldNumber = LevelManager::getInstance()->getCurrentWorld();

    // ==================== BUTTON SECTION ===================
    // moveTo y=0.6
    auto replay = cocos2d::MenuItemImage::create(
        "UserInterface/Shared/Replay_" + StringHelper::ConvertIntToString(worldNumber) + ".png",
        "UserInterface/Shared/Replay_" + StringHelper::ConvertIntToString(worldNumber) + ".png",
        this,
        menu_selector(PauseMenuHud::onResetGame));
    replay = (MenuItemImage*)PositioningHelper::placeSquareMenuItem(replay, 0.5, 1.1, 0.4);
    replay->runAction(EaseElasticOut::create(MoveTo::create(1,
        Vec2(PositioningHelper::getRelativePosition(0.5, 0.6))), 2));

    // moveTo x=0.8
    auto levelSelection = cocos2d::MenuItemImage::create(
        "UserInterface/Shared/LevelSelection_" + StringHelper::ConvertIntToString(worldNumber) + ".png",
        "UserInterface/Shared/LevelSelection_" + StringHelper::ConvertIntToString(worldNumber) + ".png",
        this,
        menu_selector(PauseMenuHud::onLevelSelection));
    levelSelection = (MenuItemImage*)PositioningHelper::placeSquareMenuItem(levelSelection, 1.1, 0.3, relativeButtonSides);
    levelSelection->runAction(EaseElasticOut::create(MoveTo::create(1,
        Vec2(PositioningHelper::getRelativePosition(0.7, 0.3))), 2));
    
    // moveTo x = 0.2
    auto soundOn = cocos2d::MenuItemImage::create(
        "UserInterface/Shared/SoundOn_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png",
        "UserInterface/Shared/SoundOn_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png",
        this,
        menu_selector(PauseMenuHud::onSoundSelected));
    soundOn = (MenuItemImage*)PositioningHelper::placeSquareMenuItem(soundOn, -0.1, 0.3, relativeButtonSides);
    soundOn->runAction(EaseElasticOut::create(MoveTo::create(1,
        Vec2(PositioningHelper::getRelativePosition(0.3, 0.3))), 2));
    soundOn->setTag(3);

    auto soundOff = cocos2d::MenuItemImage::create(
        "UserInterface/Shared/SoundOff_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png",
        "UserInterface/Shared/SoundOff_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png",
        this,
        menu_selector(PauseMenuHud::onSoundSelected));
    soundOff = (MenuItemImage*)PositioningHelper::placeSquareMenuItem(soundOff, -0.1, 0.3, relativeButtonSides);
    soundOff->runAction(EaseElasticOut::create(MoveTo::create(1,
        Vec2(PositioningHelper::getRelativePosition(0.3, 0.3))), 2));
    soundOff->setTag(4);

    cocos2d::Menu* pMenu = cocos2d::Menu::create(replay, levelSelection, soundOff, soundOn, NULL);

    // moveTo x=0.25
    auto leftDecoration = Sprite::create("UserInterface/PauseMenu/PauseMenuLeft_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png");
    leftDecoration = (Sprite*)PositioningHelper::placeGameItem(
        leftDecoration, 
        -0.1, 0.5, 
        0.6, 1);
    leftDecoration->runAction(EaseElasticOut::create(MoveTo::create(1,
        Vec2(PositioningHelper::getRelativePosition(0.25, 0.5))), 2));
    pMenu->addChild(leftDecoration, 2);

    // moveTo x=75
    auto rightDecoration = Sprite::create("UserInterface/PauseMenu/PauseMenuRight_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + ".png");
    rightDecoration = (Sprite*)PositioningHelper::placeGameItem(
        rightDecoration, 
        1.1, 0.5,
        0.6, 1);
    rightDecoration->runAction(EaseElasticOut::create(MoveTo::create(1,
        Vec2(PositioningHelper::getRelativePosition(0.75, 0.5))), 2));
    pMenu->addChild(rightDecoration, 2);

    pMenu->setPosition(0, 0);
    pMenu->setTag(99);
    this->addChild(pMenu, 1);
}

void PauseMenuHud::removePauseMenu()
{
    this->removeChildByTag(99);
    this->removeChild(_alphaBackground);
    _paused = false;
}

void PauseMenuHud::onPauseGame(Ref* sender)
{
    handlePauseMenuRequest();
}

void PauseMenuHud::handlePauseMenuRequest()
{
    auto mainGameScene = (MainGameScene*)(this->getParent()->getChildByTag(TagDefinitions::GAMESCENE));

    if (!_paused)
    {
        _paused = true;
        showPauseMenu();
        mainGameScene->pauseGame();
    }
    else
    {
        _paused = false;
        removePauseMenu();
        mainGameScene->resumeGame();
    }
}

void PauseMenuHud::onResetGame(Ref* sender)
{
    auto mainGameScene = (MainGameScene*)(this->getParent()->getChildByTag(TagDefinitions::GAMESCENE));
    mainGameScene->resetGameSceneElements();

    StatisticsManager::getInstance()->incrementCurrentLevelDeaths();
    setNumberOfDeaths();

    removePauseMenu();
    mainGameScene->resumeGame();    
}

void PauseMenuHud::onMainMenu(Ref* sender)
{
    auto newScene = MenuScene::createScene();
    Director::getInstance()->replaceScene((Scene*)newScene);
}

void PauseMenuHud::onLevelSelection(Ref* sender)
{
    SoundEngine::playEffect("Sounds/Button_Press.wav");

    auto newScene = MenuScene::createScene();
    auto menuSceneLayer = newScene->getChildByTag(TagDefinitions::MENU_SCENE_LAYER);

    Director::getInstance()->replaceScene((Scene*)newScene);
}

void PauseMenuHud::setNumberOfDeaths()
{
    auto currentLevelDeaths = StatisticsManager::getInstance()->getCurrentLevelDeaths();

    _timesDied->setTitleText(StringHelper::ConvertIntToString(currentLevelDeaths));
}

void PauseMenuHud::onSoundSelected(Ref* sender)
{
    auto soundButton = (MenuItem*)sender;
    if (!SoundEngine::muted)
    {
        soundButton->setVisible(false);
        auto menu = this->getChildByTag(99);
        auto soundOff = menu->getChildByTag(4);
        soundOff->setVisible(true);

        float startScale = soundOff->getScaleX();

        soundOff->runAction(CCSequence::create(
            CCScaleTo::create(0.1, startScale*0.9),
            CCScaleTo::create(0.2, startScale*1.1),
            CCScaleTo::create(0.2, startScale * 1), NULL));
        soundButton->runAction(CCSequence::create(
            CCScaleTo::create(0.1, startScale*0.9),
            CCScaleTo::create(0.2, startScale*1.1),
            CCScaleTo::create(0.2, startScale * 1), NULL));

        SoundEngine::soundOff();

    }
    else
    {
        soundButton->setVisible(false);
        soundButton->setVisible(false);
        auto menu = this->getChildByTag(99);
        auto soundOn = menu->getChildByTag(3);
        soundOn->setVisible(true);


        float startScale = soundOn->getScaleX();
        soundOn->runAction(CCSequence::create(
            CCScaleTo::create(0.1, startScale*0.9),
            CCScaleTo::create(0.2, startScale*1.1),
            CCScaleTo::create(0.2, startScale * 1), NULL));
        soundButton->runAction(CCSequence::create(
            CCScaleTo::create(0.1, startScale*0.9),
            CCScaleTo::create(0.2, startScale*1.1),
            CCScaleTo::create(0.2, startScale * 1), NULL));

        SoundEngine::soundOn();
    }

}

void PauseMenuHud::onGamePlayable(Node* sender)
{
    _playable = true;

    _playoutMovements->runAction(FadeIn::create(0.3));
}

MovementArrow* PauseMenuHud::applyArrowRotation(std::string rotationId, MovementArrow* movementArrow)
{
    movementArrow->setRotation(_arrowRotations[rotationId]);
    return movementArrow;
}

std::string PauseMenuHud::getMovementIdentifier(b2Vec2 movement)
{
    if (movement.x == -1 && movement.y == 0)
        return "left";
    if (movement.x == -1 && movement.y == 1)
        return "topLeft";
    if (movement.x == 0 && movement.y == 1)
        return "up";
    if (movement.x == 1 && movement.y == 1)
        return "topRight";
    if (movement.x == 1 && movement.y == 0)
        return "right";
}

void PauseMenuHud::handleBackNavigation()
{
    if (true)
    {

    }
}

b2Vec2 PauseMenuHud::getVectorForId(int vectorId)
{
    switch (vectorId)
    {
    case 0: return b2Vec2(-1, 0);
    case 1: return b2Vec2(-1, 1);
    case 2: return b2Vec2(0, 1);
    case 3: return b2Vec2(1, 1);
    case 4: return b2Vec2(1, 0);
    }
}

void PauseMenuHud::runAnimationForCurrentMovements()
{
    CCFiniteTimeAction* onMovementAnimationCompleteCallback = CCCallFuncN::create(this,
        callfuncN_selector(PauseMenuHud::onMovementAnimationComplete));

    auto leftMovement = _playoutMovements->getChildByName(StringHelper::AddNumberToString("L", _playoutMovementIndex));
    
    if (leftMovement == NULL) return;

    float animationTime = 0.2;

    leftMovement->runAction(FadeOut::create(animationTime));
    leftMovement->runAction(Sequence::create(
        ScaleBy::create(animationTime, 2),
        onMovementAnimationCompleteCallback,
        NULL));

    auto rightMovement = _playoutMovements->getChildByName(StringHelper::AddNumberToString("R", _playoutMovementIndex));
    rightMovement->runAction(FadeOut::create(animationTime));
    rightMovement->runAction(Sequence::create(
        ScaleBy::create(animationTime, 2),
        onMovementAnimationCompleteCallback,
        NULL));

    float movementTime = 0.25;
    _playoutMovements->runAction(MoveBy::create(0.25, Vec2(0, PositioningHelper::getHalfHeight() / 3.5)));

    _playoutMovementIndex++;
}

void PauseMenuHud::setIntroductionShown(bool value)
{
    _introductionShown = value;   
}

// ===================== CORRECT ARROWS SECTION ======================

void PauseMenuHud::initializeCorrectMovements()
{  
    _correctMovementsOnScreen = 0;

    definedLevelMovements = XMLHelper::getLevelMovements(LevelManager::getInstance()->getLevelPath());

    // create container the arrows will be in
    _correctMovements = Layer::create();
    float arrowsXPosition = PositioningHelper::screenBounds.width - 5;

    _correctMovements->setPosition(arrowsXPosition, 0);

    for (int i = 0; i < definedLevelMovements.size(); i++)
    {
        auto currentMovement = definedLevelMovements.at(i);
        addCorrectMovementSprite(currentMovement, i);
    }

    this->addChild(_correctMovements, 40);
}

void PauseMenuHud::addCorrectMovementSprite(b2Vec2 movement, int index)
{
    // get the id for the movement
    auto movementId = getMovementIdentifier(movement);

    auto movementArrow = MovementArrow::createArrow();

    movementArrow = applyArrowRotation(movementId, movementArrow);

    float relativeXPosition = ((float)index / (float)(_numberOfMovements)) + 0.04;

    movementArrow = (MovementArrow*)PositioningHelper::placeSquareGameItem(
        movementArrow,
        relativeXPosition, 0.96,
        0.05);

    movementArrow->Enable();

    _correctMovements->addChild(movementArrow,40);
    movementArrows.pushBack(movementArrow);
}

void PauseMenuHud::playOutMovement(int currentMovement)
{
    // Enable the arrow for the current movement. Since the arrows
    // will only appear on screen if it's not, the ones that are on 
    // screen but are not moving will be enabled. which is the desired
    // effect.
    //
    //arrowToEnable = currentMovement - 1 because the movemement is the
    // actual movementNumber and the index is obviously from 0, so thus -1
    int arrowToEnable = currentMovement - 1;
    movementArrows.at(arrowToEnable)->Enable();

    if (currentMovement > _correctMovementsOnScreen)
    {
        //if (movementArrows)
        _correctMovementsOnScreen++;

        float relativeMoveToPosition = (float)1 / (float)_numberOfMovements;

        auto moveToPosition = -Vec2(PositioningHelper::getRelativePosition(relativeMoveToPosition, 0));

        _correctMovements->runAction(CCMoveBy::create(0.2, Vec2(moveToPosition.x, 0)));
    }

}

void PauseMenuHud::clearCorrectArrows()
{
    _correctMovements->removeAllChildrenWithCleanup(true);

    movementArrows.clear();
}

void PauseMenuHud::resetCorrectArrows()
{
    for (int i = 0; i < movementArrows.size(); i++)
    {
        movementArrows.at(i)->Disable();
    }
}

// ======================================================================

// ===================== PLAYOUT MOVEMENTS SECTION ======================

void PauseMenuHud::initializePlayoutMovements()
{
    // initialize playout movements
    _playoutMovements = Menu::create();
    _playoutMovementIndex = 0;

    for (int i = 0; i < definedLevelMovements.size(); i++)
    {
        auto correctMovement = definedLevelMovements.at(i);

        int randomNumber = rand() % 4;
        auto randomMovement = getVectorForId(randomNumber);

        float arrowSize = 0.2;
        float distanceBetweenFactor = 7.0f;

        // Create a random number. If it's bigger than 2.3 place the
        // correct movement on the left
        float sideDecider = rand() % 6;
        if (sideDecider > 2.3)
        {
            addLeftMovement(correctMovement, i, arrowSize, distanceBetweenFactor);
            addRightMovement(randomMovement, i, arrowSize, distanceBetweenFactor);
        }
        // otherwise place the correct movement on the right
        else
        {
            addLeftMovement(randomMovement, i, arrowSize, distanceBetweenFactor);
            addRightMovement(correctMovement, i, arrowSize, distanceBetweenFactor);
        }
    }

    _playoutMovements->setPosition(0, -PositioningHelper::getVisibleHeight());
    this->addChild(_playoutMovements, 1);
}

void PauseMenuHud::addLeftMovement(b2Vec2 movement, int index, float arrowSize, float distanceBetweenFactor)
{
    auto leftMovement = Button::create(
        "UserInterface/PauseMenu/ButtonArrowOn.png",
        "UserInterface/PauseMenu/ButtonArrowOn.png");

    PositioningHelper::placeSquareWidget(leftMovement, 0.25, 1.08 - ((float)index / distanceBetweenFactor), arrowSize);
    leftMovement->addTouchEventListener(CC_CALLBACK_2(PauseMenuHud::onLeftMovementSelected, this));

    auto leftMovementId = getMovementIdentifier(movement);
    leftMovement->setRotation(_arrowRotations[leftMovementId]);
    leftMovement->setName(StringHelper::AddNumberToString("L", index));

    _leftMovements.push_back(movement);
    _playoutMovements->addChild(leftMovement);
}

void PauseMenuHud::addRightMovement(b2Vec2 movement, int index, float arrowSize, float distanceBetweenFactor)
{

    auto rightMovement = Button::create(
        "UserInterface/PauseMenu/ButtonArrowOn.png",
        "UserInterface/PauseMenu/ButtonArrowOn.png");
    PositioningHelper::placeSquareWidget(rightMovement, 0.75, 1.08 - ((float)index / distanceBetweenFactor), arrowSize);
    rightMovement->addTouchEventListener(CC_CALLBACK_2(PauseMenuHud::onRightMovementSelected, this));

    auto rightMovementId = getMovementIdentifier(movement);
    rightMovement->setRotation(_arrowRotations[rightMovementId]);
    rightMovement->setName(StringHelper::AddNumberToString("R", index));

    _rightMovements.push_back(movement);
    _playoutMovements->addChild(rightMovement);
}

void PauseMenuHud::onMovementAnimationComplete(Node* sender)
{
    _playoutMovements->removeChild(sender, true);
}

void PauseMenuHud::movementSelected(SELECTED_SIDE selectedSide)
{
    if (!_introductionShown || !_playable) return;
    if (_leftMovements.size() == 0) return;

    auto mainGameScene = (MainGameScene*)(this->getParent()->getChildByTag(TagDefinitions::GAMESCENE));

    runAnimationForCurrentMovements();
    // Left side
    if (selectedSide == LEFTMOVEMENT)
    {
        auto movement = _leftMovements.at(0);
        mainGameScene->playOutSelectedMovement(movement);
    }
    else
    {
        auto movement = _rightMovements.at(0);
        mainGameScene->playOutSelectedMovement(movement);
    }

    _leftMovements.erase(_leftMovements.begin());
    _rightMovements.erase(_rightMovements.begin());
}

void PauseMenuHud::onLeftMovementSelected(Ref* sender, Widget::TouchEventType eventType)
{
    switch (eventType)
    {
        case Widget::TouchEventType::BEGAN:
            CCLog("Touch began");
            break;
        case Widget::TouchEventType::ENDED:
        {
            auto node = static_cast<Node*>(sender);
            if (node->getOpacity() < 0.6f) return;
            movementSelected(LEFTMOVEMENT);
            break;
        }
        case Widget::TouchEventType::MOVED:
            CCLog("Touch moved");
            break;
        case Widget::TouchEventType::CANCELED:
            CCLog("Touch cancelled");
            break;
    }
  
}

void PauseMenuHud::onRightMovementSelected(Ref* sender, Widget::TouchEventType eventType)
{
    switch (eventType)
    {
        case Widget::TouchEventType::BEGAN:
            CCLog("Touch began");
            break;
        case Widget::TouchEventType::ENDED:
        {
            auto node = static_cast<Node*>(sender);
            if (node->getOpacity() < 0.6f) return;
            movementSelected(RIGHTMOVEMENT);
            break;
        }
        case Widget::TouchEventType::MOVED:
            CCLog("Touch moved");
            break;
        case Widget::TouchEventType::CANCELED:
            CCLog("Touch cancelled");
            break;
    }
   
}

void PauseMenuHud::resetPlayoutMovements()
{
    clearPlayoutMovements();
    initializePlayoutMovements();

    _playable = false;
    
    _playoutMovements->setOpacity(0);

    CCFiniteTimeAction* onResetSequenceComplete = CCCallFuncN::create(this,
        callfuncN_selector(PauseMenuHud::onGamePlayable));

    auto resetSequence = Sequence::create(
        DelayTime::create(0.5),
        onResetSequenceComplete,
        NULL);

    this->runAction(resetSequence);
}

void PauseMenuHud::clearPlayoutMovements()
{
    _playoutMovements->removeAllChildrenWithCleanup(true);
    
    _leftMovements.clear();
    _rightMovements.clear();
}
// ======================================================================
