#ifndef __BACKGROUNDMENUSCENE_H__
#define __BACKGROUNDMENUSCENE_H__

#include "cocos2d.h"

class BackgroundMenuScene : public cocos2d::Layer
{
public:
    void resetScene();
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(BackgroundMenuScene);
};


#endif