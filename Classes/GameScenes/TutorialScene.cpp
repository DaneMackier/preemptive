#include "TutorialScene.h"
#include "MenuScene.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

USING_NS_CC;
using namespace ui;

Scene* TutorialScene::createScene()
{
    auto scene = Scene::create();
    auto layer = TutorialScene::create();
    scene->addChild(layer);
    return scene;
}

bool TutorialScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    // add the back button to the screen
    Button* backButton = Button::create(
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png",
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png");
    backButton = (Button*)PositioningHelper::placeSquareWidget(
        backButton,
        0.2, 0.1,
        0.15);

    backButton->addTouchEventListener(CC_CALLBACK_2(TutorialScene::onBackButtonPressed, this));
    this->addChild(backButton, 2);

    auto pageView = PageView::create();
    pageView->setContentSize(Size(PositioningHelper::getVisibleWidth(), PositioningHelper::getVisibleHeight()));

    for (int i = 0; i < 4; i++)
    {
        Layout* layout = Layout::create();
        layout->setContentSize(Size(PositioningHelper::getVisibleWidth(), PositioningHelper::getVisibleHeight()));

        auto tutImageLocation = "UserInterface/TutorialScene/Tutorial_" + StringHelper::ConvertIntToString(i + 1) + ".jpg";
        auto tutImage = Sprite::create(tutImageLocation);
        PositioningHelper::placeGameItem(
            tutImage,
            0.5, 0.55,
            0.8,0.8);
        layout->addChild(tutImage);
        pageView->insertPage(layout, i);
    }

    this->addChild(pageView, 0);
    
    return true;
}


void TutorialScene::onBackButtonPressed(Ref* sender, Widget::TouchEventType eventType)
{
    this->removeAllChildrenWithCleanup(true);

    auto menuScene = static_cast<MenuScene*>(this->getParent());
    menuScene->navigateToMainMenu();
}
