#ifndef __ITEMTESTSCENE_H__
#define __ITEMTESTSCENE_H__

#include "cocos2d.h"


#include "ui\CocosGUI.h"

class ItemTestScene : public cocos2d::Layer
{
private:
    /*
    Callback function to call when the intro sequence is complete
    */
    void onIntroComplete(cocos2d::Node* sender);

    /*
    Callback test function
    */
    void callbackTest(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

public:
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(ItemTestScene);
};

#endif