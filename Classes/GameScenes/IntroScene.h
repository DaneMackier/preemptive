#ifndef __INTROSCENE_H__
#define __INTROSCENE_H__

#include "cocos2d.h"
#include "GameSceneComponents\GameScene.h"

class IntroScene : public cocos2d::Layer
{
private:
    // ------- MEMBER VARIABLES
    int numberOfShapes;
    int shapesAnimated;
    int shapesSpawned;

    // ------- INITIALIZE FUNCTIONS
    void initializeIntroShapes();

    // ------- CALLBACKS
    void moveDownComplete(cocos2d::CCNode* sender);
    void logoFadeCompleted(cocos2d::CCNode* sender);

    // ------- SHEDULED FUNCTIONS
    void spawnShapes(float delta);


public:
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(IntroScene);
};


#endif