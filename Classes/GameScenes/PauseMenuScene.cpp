#include "PauseMenuScene.h"

USING_NS_CC;

cocos2d::Scene* PauseMenuScene::createScene()
{
    auto scene = Scene::create();
    auto layer = PauseMenuScene::create();
    scene->addChild(layer);
    return scene;
}

bool PauseMenuScene::init()
{
    if (!Layer::init())
    {
        return false;
    }



    return true;
}