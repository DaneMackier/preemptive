#ifndef __LEVELENDHUD_H__
#define __LEVELENDHUD_H__

#include "GameSceneComponents\GameScene.h"


#include "ui\CocosGUI.h"

class LevelEndHud : public GameScene
{
private:

    std::vector<cocos2d::Vec2> _ratingsPositions;

    /*
    Function called when the level selection item has been selected
    */
    void onLevelSelection(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    function called when the sound button is clicked
    */
    void onShare(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Function called when the reset button has been clicked
    */
    void onResetGame(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Function called when the next level button has been clicked
    */
    void onNextLevel(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Called when the fade is completed
    */
    void fadeComplete(cocos2d::CCNode* sender);

public:



    /*
    Makes the level end hud visible
    */
    void showHud();

    /*
    Hides the level end hud
    */
    void hideHud();

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(LevelEndHud);

    virtual void handleBackNavigation();

};

#endif