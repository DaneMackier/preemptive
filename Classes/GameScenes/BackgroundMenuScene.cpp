#include "BackgroundMenuScene.h"

#include "Helpers\PositioningHelper.h"

USING_NS_CC;

Scene* BackgroundMenuScene::createScene()
{
    auto scene = Scene::create();
    auto layer = BackgroundMenuScene::create();
    scene->addChild(layer);
    return scene;
}

bool BackgroundMenuScene::init()
{
    if (!Layer::init())
    {
        return false;
    }


    // background layer: another image
    auto background = Sprite::create("UserInterface/MainMenu/Background.jpg");
    // scale the image (optional)
    background->setScale(1.5f);
    // change the transform anchor point (optional)
    background->setAnchorPoint(Vec2(0, 0));
    background = PositioningHelper::placeGameItem(
        background,
        0.0,
        0.0,
        2,
        1);

    auto ground = Sprite::create("UserInterface/MainMenu/GroundMiddle.png");
    ground = PositioningHelper::placeGameItem(
        ground,
        0.5,
        0.1,
        1.5,
        0.2);

    this->addChild(background);
    this->addChild(ground);

    return true;
}