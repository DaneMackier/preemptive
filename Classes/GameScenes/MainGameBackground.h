#ifndef __MAINGAMEBACKGROUND_H__
#define __MAINGAMEBACKGROUND_H__

#include "cocos2d.h"

class MainGameBackground : public cocos2d::Layer
{
private:

public:
    void resetScene();
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MainGameBackground);
};


#endif