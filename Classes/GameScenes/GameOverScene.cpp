#include "GameOverScene.h"
#include "MainGameScene.h"
#include "MainMenuScene.h"

#include "Helpers/PositioningHelper.h"
#include "Helpers/AnimationHelper.h"
#include "Helpers/StringHelper.h"
#include "Helpers/FileOperation.h"
#include "GameEngine\SoundEngine.h"


#include <exception>

USING_NS_CC;

Scene* GameOverScene::createScene()
{
    auto scene = Scene::create();
    auto layer = GameOverScene::create();
    scene->addChild(layer);
    return scene;
}

Layer* GameOverScene::getGameOverMenuLayer()
{
    auto layer = GameOverScene::create();
    return layer;
}

bool GameOverScene::init()
{
    
    if (!Layer::init())
    {
        return false;
    }
    this->setKeypadEnabled(true);
    this->setKeyboardEnabled(true);

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();
  

    return true;
}
