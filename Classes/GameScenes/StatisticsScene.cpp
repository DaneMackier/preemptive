#include "StatisticsScene.h"
#include "MenuScene.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

#include "GameEngine\StatisticsManager.h"

USING_NS_CC;
using namespace ui;

Scene* StatisticsScene::createScene()
{
    auto scene = Scene::create();
    auto layer = StatisticsScene::create();
    scene->addChild(layer);
    return scene;
}

bool StatisticsScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    auto headerImage = Sprite::create("UserInterface/StatisticsScene/Header.png");
    PositioningHelper::placeGameItem(
        headerImage,
        0.5, 0.95,
        1.0, 0.15
        );
    this->addChild(headerImage, 1);

    auto backdrop = Sprite::create("UserInterface/LevelCleared/Container.png");
    PositioningHelper::placeGameItem(
        backdrop,
        0.5, 0.5,
        0.8, 0.65
        );
    this->addChild(backdrop, 0);

    // add the back button to the screen
    Button* backButton = Button::create(
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png",
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png");
    backButton = (Button*)PositioningHelper::placeSquareWidget(
        backButton,
        0.2, 0.1,
        0.2);

    int totalNumberOfDeaths = StatisticsManager::getInstance()->getTotalNumberOfDeaths();
    placeGameLabel("Total Number of Deaths", totalNumberOfDeaths, 
        PositioningHelper::getRelativePosition(0.5, 0.75),
        PositioningHelper::getRelativePosition(0.5, 0.7));

    int mostDeathsOnOneLevel = StatisticsManager::getInstance()->getMostDeathsOnALevel();
    placeGameLabel("Most Deaths on a level", mostDeathsOnOneLevel,
        PositioningHelper::getRelativePosition(0.5, 0.6),
        PositioningHelper::getRelativePosition(0.5, 0.55));

    int leastDeathsOnLevel = StatisticsManager::getInstance()->getLeastDeathsOnALevel();
    placeGameLabel("Least deaths on a level", leastDeathsOnLevel,
        PositioningHelper::getRelativePosition(0.5, 0.45),
        PositioningHelper::getRelativePosition(0.5, 0.4));

    int averageDeathsPerLevel = StatisticsManager::getInstance()->getAverageDeaths();
    placeGameLabel("Average deaths per level", averageDeathsPerLevel,
        PositioningHelper::getRelativePosition(0.5, 0.3),
        PositioningHelper::getRelativePosition(0.5, 0.25));

 /*   placeGameLabel("YOU ARE ABOVE AVERAGE", 4,
        PositioningHelper::getRelativePosition(0.5, 0.35),
        PositioningHelper::getRelativePosition(0.5, 0.3));*/

    backButton->addTouchEventListener(CC_CALLBACK_2(StatisticsScene::onBackButtonPressed, this));
    this->addChild(backButton, 2);

    return true;
}

void StatisticsScene::placeGameLabel(std::string labelText, int value, Vec2 titlePosition, Vec2 valuePosition)
{
    auto labelPosition = PositioningHelper::getRelativePosition(0.5, 0.5);

    auto titleLabel = ui::Text::create();
    titleLabel->setText(labelText);
    titleLabel->setFontSize(30);
    titleLabel->setPosition(titlePosition);
    this->addChild(titleLabel);

    auto valueLabel = ui::Text::create();
    valueLabel->setText(StringHelper::ConvertIntToString(value));
    valueLabel->setFontSize(30);
    valueLabel->setPosition(valuePosition);
    this->addChild(valueLabel);
}

void StatisticsScene::onBackButtonPressed(Ref* sender, Widget::TouchEventType eventType)
{
    this->removeAllChildrenWithCleanup(true);

    auto menuScene = static_cast<MenuScene*>(this->getParent());
    menuScene->navigateToMainMenu();
}
