#ifndef __INTROSPLASHSCENE_H__
#define __INTROSPLASHSCENE_H__

#include "cocos2d.h"

class IntroSplashScene : public cocos2d::Layer
{
private:
    /*
    Callback function to call when the intro sequence is complete
    */
    void onIntroComplete(cocos2d::Node* sender);

public:
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(IntroSplashScene);
};

#endif