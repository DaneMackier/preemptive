#include "IntroScene.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

#include "GameEngine\SoundEngine.h"

#include "GameEngine\LevelManager.h"

USING_NS_CC;

Scene* IntroScene::createScene()
{
    auto scene = Scene::create();
    auto layer = IntroScene::create();
    scene->addChild(layer);
    return scene;
}

bool IntroScene::init()
{
    if (!Layer::init())
    {
        return false;
    }
    this->setColor(Color3B(255, 255, 255));
   

    this->setKeypadEnabled(true);
    this->setKeyboardEnabled(true);

    // put the sound on
    //SoundEngine::soundOn();
/*
    auto levelManager = new LevelManager();
    levelManager->buildLevelSelectionItems();

    numberOfShapes = 6;
    shapesAnimated = 1;
   
    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)  
    shapesSpawned = -1;
    this->schedule(schedule_selector(IntroScene::spawnShapes), 0.3, 7, 0.1);
#endif

#if (CC_TARGET_PLATFORM != CC_PLATFORM_WP8)
    shapesSpawned = 0;
    this->schedule(schedule_selector(IntroScene::spawnShapes), 0.3, 6, 0.1);
#endif
*/
    return true;

}

void IntroScene::initializeIntroShapes()
{
    
       
}

void IntroScene::spawnShapes(float delta)
{
    //shapesSpawned++;
    //MorphShape cutoutSpawnShape = static_cast<MorphShape>(shapesSpawned);
    //auto cutout = Sprite::create("UserInterface/IntroScene/Letters/" + StringHelper::ConvertIntToString(shapesSpawned) + ".png");
    //cutout = (Sprite*)PositioningHelper::placeSquareGameItem(
    //    cutout,
    //    0.5, 1,
    //    0.35);

    //CCFiniteTimeAction* moveToCentreComplete = CCCallFuncN::create(this, callfuncN_selector(IntroScene::moveDownComplete));
    //// duration should come from a common function!!*********************************************************************
    //cutout->runAction(CCSequence::create(
    //    CCMoveTo::create(1, Vec2(PositioningHelper::screenBounds.width / 2, PositioningHelper::screenBounds.height/2)),
    //    moveToCentreComplete,
    //    NULL));
    //cutout->runAction(CCScaleBy::create(1, 3.5));

    //this->addChild(cutout, 1);
    //
}

void IntroScene::moveDownComplete(cocos2d::CCNode* sender)
{
    /*shapesAnimated++;

    this->removeChild(sender);

    if (shapesAnimated > numberOfShapes)
    {
        auto logo = Sprite::create("UserInterface/MainMenu/logo.png");
        logo = (Sprite*)PositioningHelper::placeGameItem(logo, 0.5, 0.5, 1, 0.25);

        auto logoFadeComplete = CCCallFuncN::create(this, callfuncN_selector(IntroScene::logoFadeCompleted));

        logo->runAction(CCSequence::create(
            CCFadeIn::create(0.2),
            CCDelayTime::create(0.1),
            CCFadeOut::create(0.2), 
            logoFadeComplete, NULL));
        
        this->addChild(logo);
    }*/
}

void IntroScene::logoFadeCompleted(CCNode* sender)
{

}