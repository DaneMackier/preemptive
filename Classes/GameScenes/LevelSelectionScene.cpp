#include "LevelSelectionScene.h"
#include "MainGameScene.h"
#include "MenuScene.h"

#include "GameSceneComponents\LevelSelectionItem.h"

#include "GameEngine\StatisticsManager.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"
#include "Helpers\DatabaseService.h"

#include "GameSceneComponents\TagDefinitions.h"

#include "ui\CocosGUI.h"

USING_NS_CC;
using namespace ui;

Scene* LevelSelectionScene::createScene()
{
    auto scene = Scene::create();
    auto layer = LevelSelectionScene::create();
    layer->gameScene = "LevelSelectionScene";
    scene->addChild(layer);
    return scene;
}

bool LevelSelectionScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    this->setKeypadEnabled(true);
    this->setKeyboardEnabled(true);

    auto headerImage = Sprite::create("UserInterface/LevelSelection/Header.png");
    PositioningHelper::placeGameItem(
        headerImage,
        0.5, 0.95,
        1.0, 0.15
        );
    this->addChild(headerImage, 1);

    int numberOfLevels = LevelManager::getInstance()->getNumberOfLevels();
    
    int worldNumber = LevelManager::getInstance()->getCurrentWorld();
    // add the back button to the screen
    Button* backButton = Button::create(
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(worldNumber) + ".png",
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(worldNumber) + ".png");
    backButton = (Button*)PositioningHelper::placeSquareWidget(
        backButton,
        0.2, 0.1,
        0.2);

    backButton->addTouchEventListener(CC_CALLBACK_2(LevelSelectionScene::onBackButtonPressed, this));
    this->addChild(backButton, 2);

    ScrollView* scrollView = ScrollView::create();
    scrollView->setContentSize(PositioningHelper::screenBounds);
    scrollView->setInnerContainerSize(Size(PositioningHelper::getVisibleWidth() * 5, PositioningHelper::getVisibleHeight()));
    scrollView->setDirection(ScrollView::Direction::HORIZONTAL);

    int levelNumber = 0;
    int positionNumber = 0;

    float xOffset = 0.0f;

    std::vector<Vec2> levelSelectionPositions;
    levelSelectionPositions.push_back(Vec2(0.1, 0.6));
    levelSelectionPositions.push_back(Vec2(0.1, 0.2));
    levelSelectionPositions.push_back(Vec2(0.45, 0.4));
    levelSelectionPositions.push_back(Vec2(0.8, 0.7));
    levelSelectionPositions.push_back(Vec2(0.8, 0.1));

    for (int levelIndex = 0; levelIndex < numberOfLevels; levelIndex++)
    {
        levelNumber++;

        int worldNumber = LevelManager::getInstance()->getCurrentWorld();
        auto levelSelectButton = LevelSelectionItem::createLevelSelectionItem(levelNumber, StatisticsManager::getInstance()->getPredictionValue(levelNumber));

        auto itemPosition = levelSelectionPositions.at(positionNumber);
        positionNumber++;
        levelSelectButton->setPosition(PositioningHelper::getRelativePosition(itemPosition.x + xOffset, itemPosition.y));
        levelSelectButton->addTouchEventListener(CC_CALLBACK_2(LevelSelectionScene::onLevelSelected, this));
        levelSelectButton->setItemTag(levelNumber);
       
        
        if ((levelNumber) > LevelManager::getInstance()->getFurthestLevel())
        {
            //levelSelectButton->deactivateLevel();
        }
        else
        {
            levelSelectButton->setRating(StatisticsManager::getInstance()->getRatingsValue(levelNumber));
        }

        if (levelNumber % 5 == 0)
        {
            xOffset += 1.2;
            positionNumber = 0;
        }

        scrollView->addChild(levelSelectButton);
    }

    this->addChild(scrollView);

    return true;
}

void LevelSelectionScene::dispose(CCNode* sender)
{
    this->removeAllChildrenWithCleanup(true);
}

void LevelSelectionScene::onLevelSelected(Ref* sender, Widget::TouchEventType eventType)
{
    switch (eventType)
    {
    case Widget::TouchEventType::BEGAN:
        break;
    case Widget::TouchEventType::ENDED:
    {

        this->removeAllChildrenWithCleanup(true);
        
        auto selectedItem = static_cast<Node*>(sender);
        int selectedNumber = selectedItem->getTag();

        LevelManager::getInstance()->setLevel(selectedNumber);

        auto newScene = MainGameScene::createScene();
        Director::getInstance()->replaceScene((Scene*)newScene);

        break;
    }
    case Widget::TouchEventType::MOVED:
        break;
    case Widget::TouchEventType::CANCELED:
        break;
    }
}

void LevelSelectionScene::onBackButtonPressed(Ref* sender, Widget::TouchEventType eventType)
{
    this->removeAllChildrenWithCleanup(true);

    auto menuScene = static_cast<MenuScene*>(this->getParent());
    menuScene->navigateToWorldSelection();
}


void LevelSelectionScene::backButtonPressed()
{
    //_backButton->runAction(EaseElasticOut::create(ScaleTo::create(0.5, 1)));
    //this->removeAllChildrenWithCleanup(true);

    //auto menuScene = static_cast<MenuScene*>(this->getParent());
    //menuScene->navigateToWorldSelection();
}