#pragma once

#include "GameSceneComponents\GameScene.h"
#include "ui\CocosGUI.h"

class IntroAnimationScene : public GameScene
{
private:
    /* Callback for when the skip button is pressed */
    void onSkipIntro(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /* Callback for when the animation is complete */
    void animationComplete(cocos2d::Node* sender);

public:
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(IntroAnimationScene);
};