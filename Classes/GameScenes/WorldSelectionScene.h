#ifndef __WORLDSELECTIONSCENE_H__
#define __WORLDSELECTIONSCENE_H__

#include "cocos2d.h"
#include "ui\CocosGUI.h"


class WorldSelectionScene : public cocos2d::Layer
{
private:
    cocos2d::ui::Button* _backButton;

    /*
    Function called when a world has been selected
    */
    void onWorldSelected(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Function called when the back button on screen is clicked
    */
    void onBackButtonPressed(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

public:

    /*
    Function used to handle back button pressed
    */
    void backButtonPressed();

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(WorldSelectionScene);
};

#endif