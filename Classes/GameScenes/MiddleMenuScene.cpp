#include "MiddleMenuScene.h"

#include "Helpers\PositioningHelper.h"
#include "GameSceneComponents\TagDefinitions.h"

USING_NS_CC;

Scene* MiddleMenuScene::createScene()
{
    auto scene = Scene::create();
    auto layer = MiddleMenuScene::create();
    scene->addChild(layer);
    return scene;
}

bool MiddleMenuScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    _middleLayerLevel = 2;

    this->setTag(TagDefinitions::MIDDLEMENUSCENELAYER);

    // ----------------------- LEFT MIDDLE SPRITES ------------------------
    auto leavesLeft = Sprite::create("UserInterface/MainMenu/LeavesLeft.png");
    leavesLeft = PositioningHelper::placeGameItem(
        leavesLeft,
        0.15,
        0.55,
        0.33,
        0.33);

    auto leftRocks = Sprite::create("UserInterface/MainMenu/LeftRocks.png");
    leftRocks = PositioningHelper::placeGameItem(
        leftRocks,
        0.35,// x
        0.22,// y
        0.77,// width
        0.28);// height

    auto topLeftLog = Sprite::create("UserInterface/MainMenu/TopLog.png");
    topLeftLog = PositioningHelper::placeGameItem(
        topLeftLog,
        0.25,
        0.9,
        0.5,
        0.22);

    this->addChild(leavesLeft, _middleLayerLevel);
    this->addChild(leftRocks, _middleLayerLevel);
    this->addChild(topLeftLog, _middleLayerLevel);
   
    // Initiate first drop to fall
    this->runAction(CCCallFuncN::create(this, callfuncN_selector(MiddleMenuScene::addDropToScene)));

    // ----------------------- LEFT MIDDLE SPRITES ------------------------

    // ----------------------- RIGHT MIDDLE SPRITES -----------------------
    auto middleVines = Sprite::create("UserInterface/MainMenu/MiddleVines.png");
    middleVines = PositioningHelper::placeGameItem(
        middleVines,
        1.2,
        0.55,
        0.8,
        0.9);

    auto topThornes = Sprite::create("UserInterface/MainMenu/TopThornes.png");
    topThornes = PositioningHelper::placeGameItem(
        topThornes,
        1.7,
        0.85,
        0.9,
        0.3);

    auto rightRocks = Sprite::create("UserInterface/MainMenu/RightRocks.png");
    rightRocks = PositioningHelper::placeGameItem(
        rightRocks,
        1.8,
        0.2,
        0.6,
        0.3);

    this->addChild(rightRocks, _middleLayerLevel);
    this->addChild(topThornes, _middleLayerLevel);
    this->addChild(middleVines, _middleLayerLevel);
    // ----------------------- RIGHT MIDDLE SPRITES -----------------------

    // ----------------------- LIGHTS SECTION ----------------------------
    initializeLightRays();

    this->setPosition(0, 0);
    
    /*auto& children = this->getChildren();
    for (auto &child : children) {
        child->setOpacity(0);
    }*/

    // ----------------------- LIGHTS SECTION ----------------------------
    return true;
}

void MiddleMenuScene::onLightFadeComplete(Node* sender)
{
    auto lightBeam = sender;
    CCFiniteTimeAction* lightFadeComplete = CCCallFuncN::create(this, callfuncN_selector(MiddleMenuScene::onLightFadeComplete));

    float fadeInTime = rand() % 3 + 1;
    float fadeOutTime = rand() % 3 + 1;

    lightBeam->runAction(CCSequence::create(
        CCFadeOut::create(fadeInTime),
        CCFadeIn::create(fadeOutTime),
        lightFadeComplete,
        NULL));
}

void MiddleMenuScene::onDropComplete(Node* sender)
{
    this->removeChild(sender);

    CCFiniteTimeAction* addDropToSceneCallback = CCCallFuncN::create(this, callfuncN_selector(MiddleMenuScene::addDropToScene));

    float timeToDrop = rand() % 6 + 1;

    this->runAction(CCSequence::create(
        CCDelayTime::create(timeToDrop),
        addDropToSceneCallback,
        NULL));
}

void MiddleMenuScene::addDropToScene(Node* sender)
{
    auto waterDrop = Sprite::create("UserInterface/MainMenu/WaterDrop.png");
    waterDrop = PositioningHelper::placeGameItem(
        waterDrop,
        0.095,
        0.87,
        0.02,
        0.02);

    CCFiniteTimeAction* dropFallComplete = CCCallFuncN::create(this, callfuncN_selector(MiddleMenuScene::onDropComplete));

    auto waterDropSequence = CCSequence::create(
        CCMoveBy::create(1, Vec2(0, -10)),
        CCMoveBy::create(1, Vec2(0, -550)),
        dropFallComplete,
        NULL);

    waterDrop->runAction(waterDropSequence);
    this->addChild(waterDrop, _middleLayerLevel - 2);
}

void MiddleMenuScene::initializeLightRays()
{
    auto lightBeam = Sprite::create("UserInterface/MainMenu/lightbeam.png");
    lightBeam = PositioningHelper::placeGameItem(
        lightBeam,
        0.1,
        0.7,
        0.2,
        1);

    auto lightBeam2 = Sprite::create("UserInterface/MainMenu/lightbeam.png");
    lightBeam2 = PositioningHelper::placeGameItem(
        lightBeam2,
        0.4,
        0.6,
        0.2,
        1);

    auto lightBeam3 = Sprite::create("UserInterface/MainMenu/lightbeam.png");
    lightBeam3 = PositioningHelper::placeGameItem(
        lightBeam3,
        0.45,
        0.4,
        0.2,
        1);

    auto lightBeam4 = Sprite::create("UserInterface/MainMenu/lightbeam.png");
    lightBeam4 = PositioningHelper::placeGameItem(
        lightBeam4,
        1.9,
        0.6,
        0.2,
        1);

    auto lightBeam5 = Sprite::create("UserInterface/MainMenu/lightbeam.png");
    lightBeam5 = PositioningHelper::placeGameItem(
        lightBeam5,
        1.7,
        0.6,
        0.2,
        1);

    auto lightBeam6 = Sprite::create("UserInterface/MainMenu/lightbeam.png");
    lightBeam6 = PositioningHelper::placeGameItem(
        lightBeam6,
        1.45,
        0.4,
        0.2,
        1);

    lightBeam->setRotation(40);
    lightBeam2->setRotation(40);
    lightBeam3->setRotation(25);

    lightBeam4->setRotation(10);
    lightBeam5->setRotation(30);
    lightBeam6->setRotation(25);


    CCFiniteTimeAction* lightFadeComplete = CCCallFuncN::create(this, callfuncN_selector(MiddleMenuScene::onLightFadeComplete));

    lightBeam->runAction(CCSequence::create(
        CCFadeOut::create(1.0f),
        CCFadeIn::create(1.0f),
        lightFadeComplete,
        NULL));

    lightBeam2->runAction(CCSequence::create(
        CCFadeOut::create(1.0f),
        CCFadeIn::create(1.0f),
        lightFadeComplete,
        NULL));

    lightBeam3->runAction(CCSequence::create(
        CCFadeOut::create(1.0f),
        CCFadeIn::create(1.0f),
        lightFadeComplete,
        NULL));

    lightBeam4->runAction(CCSequence::create(
        CCFadeOut::create(1.0f),
        CCFadeIn::create(1.0f),
        lightFadeComplete,
        NULL));

    lightBeam5->runAction(CCSequence::create(
        CCFadeOut::create(1.0f),
        CCFadeIn::create(1.0f),
        lightFadeComplete,
        NULL));

    lightBeam6->runAction(CCSequence::create(
        CCFadeOut::create(1.0f),
        CCFadeIn::create(1.0f),
        lightFadeComplete,
        NULL));


    this->addChild(lightBeam, _middleLayerLevel - 1);
    this->addChild(lightBeam2, _middleLayerLevel - 1);
    this->addChild(lightBeam3, _middleLayerLevel);
    this->addChild(lightBeam4, _middleLayerLevel - 2);
    //this->addChild(lightBeam5, _middleLayerLevel - 1);
    this->addChild(lightBeam6, _middleLayerLevel);

}