
#include "MainGameBackground.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

#include "GameEngine\LevelManager.h"

USING_NS_CC;

Scene* MainGameBackground::createScene()
{
    auto scene = Scene::create();
    auto layer = MainGameBackground::create();
    scene->addChild(layer);
    return scene;
}

bool MainGameBackground::init()
{
    if (!Layer::init())
    {
        return false;
    }
    std::string backgroundUrl = "LevelItems/World_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "/Background_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentLevelAssetNumber()) + ".jpg";
    auto levelBackground = Sprite::create(backgroundUrl);
    levelBackground = PositioningHelper::placeGameItem(
        levelBackground, 0.5, 0.5, 1, 1);
    levelBackground->setTag(1);
    //levelBackground->setOpacity(0);
    this->addChild(levelBackground, 1);
  
   /* auto& children = this->getChildren();
    for (auto &child : children) {
        child->setOpacity(0);
    }*/
    
    return true;
}

void MainGameBackground::resetScene()
{
    this->removeAllChildrenWithCleanup(true);

    std::string backgroundUrl = "LevelItems/World_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "/Background_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentLevelAssetNumber()) + ".jpg";
    auto levelBackground = Sprite::create(backgroundUrl);
    levelBackground = PositioningHelper::placeGameItem(
        levelBackground, 0.5, 0.5, 1, 1);
    levelBackground->setTag(1);
    //levelBackground->setOpacity(0);
    this->addChild(levelBackground, 1);

}

