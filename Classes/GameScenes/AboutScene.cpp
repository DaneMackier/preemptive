#include "AboutScene.h"
#include "MenuScene.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

USING_NS_CC;
using namespace ui;

Scene* AboutScene::createScene()
{
    auto scene = Scene::create();
    auto layer = AboutScene::create();
    scene->addChild(layer);
    return scene;
}

bool AboutScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    // add the back button to the screen
    Button* backButton = Button::create(
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png",
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png");
    backButton = (Button*)PositioningHelper::placeSquareWidget(
        backButton,
        0.2, 0.1,
        0.2);

    backButton->addTouchEventListener(CC_CALLBACK_2(AboutScene::onBackButtonPressed, this));
    this->addChild(backButton, 2);

    auto aboutBox = Sprite::create("UserInterface/AboutScene/about_box.png");
    PositioningHelper::placeGameItem(
        aboutBox,
        0.5,0.5,
        1.0,1.0);
    this->addChild(aboutBox, 0);

    return true;
}

void AboutScene::onBackButtonPressed(Ref* sender, Widget::TouchEventType eventType)
{
    this->removeAllChildrenWithCleanup(true);

    auto menuScene = static_cast<MenuScene*>(this->getParent());
    menuScene->navigateToMainMenu();
}