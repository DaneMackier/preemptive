#include "ItemTestScene.h"

#include "GameSceneComponents\LevelSelectionItem.h"

#include "Helpers\PositioningHelper.h"

#include "GameEngine\LevelManager.h"


USING_NS_CC;

using namespace ui;

Scene* ItemTestScene::createScene()
{
    auto scene = Scene::create();
    auto layer = ItemTestScene::create();
    scene->addChild(layer);
    return scene;
}

bool ItemTestScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    LevelManager::getInstance()->setWorld(1);


    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    auto levelItem = LevelSelectionItem::createLevelSelectionItem(1, 5);
    levelItem->setPosition(PositioningHelper::getRelativePosition(0.5, 0.5));
    levelItem->addTouchEventListener(CC_CALLBACK_2(ItemTestScene::callbackTest, this));
    //levelItem->deactivateLevel();
    levelItem->setRating(2);

    this->addChild(levelItem);
    

    return true;
}

void ItemTestScene::callbackTest(Ref* sender, Widget::TouchEventType eventType)
{
    CCLog("This motherfucker was called");

    switch (eventType)
    {
        case Widget::TouchEventType::BEGAN:
            break;
        case Widget::TouchEventType::ENDED:
        {
            break;
        }
        case Widget::TouchEventType::MOVED:
            break;
        case Widget::TouchEventType::CANCELED:
            break;
    }
}