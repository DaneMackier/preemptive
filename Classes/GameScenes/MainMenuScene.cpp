#include "MainMenuScene.h"
#include "MainGameScene.h"
#include "MenuScene.h"

#include "Helpers/PositioningHelper.h"
#include "Helpers/AnimationHelper.h"
#include "Helpers/StringHelper.h"
#include "Helpers/DatabaseService.h"

#include "GameEngine\SoundEngine.h"
#include "GameEngine\LevelManager.h"

#include "cocostudio\CCSSceneReader.h"
#include "cocostudio\CocoLoader.h"

#include "GameSceneComponents\TagDefinitions.h"

#include "cocostudio\CCSGUIReader.h"
#include "..\..\cocos2d\cocos\editor-support\cocostudio\ActionTimeline\CSLoader.h"

#include "GameEngine\SoundEngine.h"


USING_NS_CC;

using namespace cocostudio;

cocos2d::Size PositioningHelper::screenBounds = cocos2d::Size();

Scene* MainMenuScene::createScene()
{
    auto scene = Scene::create();

    auto layer = MainMenuScene::create();
    layer->gameScene = "MainMenuScene";
    layer->setTag(TagDefinitions::MAIN_MENU_LAYER);
    
    scene->addChild(layer);
    return scene;
}

bool MainMenuScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    _settingsOut = false;

    checkAndSetFurthestLevel();
    placeMenuItems();
  
    return true;
}

void MainMenuScene::placeMenuItems()
{
    float outerButtonWidth = 0.17;
    float buttonHeight = 0.15;

    int currentWorld = LevelManager::getInstance()->getCurrentWorld();

    // 0.5, 0.85
    Sprite* titleImage = Sprite::create("UserInterface/MainMenu/TitleImage.png");
    PositioningHelper::placeGameItem(titleImage,
        0.45,1.3,
        0.7, 0.35);
    titleImage->runAction(EaseElasticOut::create(MoveTo::create(1, PositioningHelper::getRelativePosition(0.5, 0.8))));
    this->addChild(titleImage);

    // 0.5, 0.5,
    cocos2d::MenuItem* play = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/Play_"+ StringHelper::ConvertIntToString(currentWorld) +".png",
        "UserInterface/MainMenu/Play_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::onPlayButton));
    play = PositioningHelper::placeSquareMenuItem(play, 0.5, 0.45, 0.5);

    float animationDelay = 0.5;

    float settingsPositionX = 0.5;
    float settingsPositionY = 0.2;

    // moveTo 0.9, 0.4,
    _exit = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/Exit_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/Exit_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::onExitGame));
    _exit = PositioningHelper::placeSquareMenuItem(_exit, settingsPositionX, settingsPositionY, outerButtonWidth);

    // 0.25, 0.25,
    _about = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/About_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/About_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::onAbout));
    _about = PositioningHelper::placeSquareMenuItem(_about, settingsPositionX, settingsPositionY, outerButtonWidth);

    // 0.75, 0.25,
    _help = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/Help_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/Help_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::onTutorial));
    _help = PositioningHelper::placeSquareMenuItem(_help, settingsPositionX, settingsPositionY, outerButtonWidth);

    // 0.5, 0.1,
    cocos2d::MenuItem* settings = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/Settings_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/Settings_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::onSettings));
    settings = PositioningHelper::placeSquareMenuItem(settings, settingsPositionX, settingsPositionY, 0.3);
    
    // 0.1, 0.4,
    _soundOn = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/SoundOn_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/SoundOn_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::soundOnOff));
    _soundOn = PositioningHelper::placeSquareMenuItem(_soundOn, settingsPositionX, settingsPositionY, outerButtonWidth);
    _soundOn->setTag(3);

    // 0.1, 0.4,
    _soundOff = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/SoundOff_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/SoundOff_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::soundOnOff));
    _soundOff = PositioningHelper::placeSquareMenuItem(_soundOff, settingsPositionX, settingsPositionY, outerButtonWidth);
    _soundOff->setTag(4);

    _statistics = cocos2d::MenuItemImage::create(
        "UserInterface/MainMenu/Stats_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        "UserInterface/MainMenu/Stats_" + StringHelper::ConvertIntToString(currentWorld) + ".png",
        this,
        menu_selector(MainMenuScene::onStatistics));
    _statistics = PositioningHelper::placeSquareMenuItem(_statistics, settingsPositionX, settingsPositionY, outerButtonWidth);
    _statistics->setTag(4);

    if (SoundEngine::muted)
    {
        _soundOff->setVisible(true);
        _soundOn->setVisible(false);
    }
    else
    {
        _soundOff->setVisible(false);
        _soundOn->setVisible(true);
    }

    cocos2d::Menu* pMenu = cocos2d::Menu::create();

    pMenu->addChild(_soundOn, 0);
    pMenu->addChild(_soundOff, 0);
    pMenu->addChild(_exit, 0);
    pMenu->addChild(_about, 0);
    pMenu->addChild(_help, 0);
    pMenu->addChild(_statistics, 0);
    pMenu->addChild(settings, 0);
    pMenu->addChild(play, 1);

    pMenu->setPosition(0, 0);
    pMenu->setTag(99);
    this->addChild(pMenu, 5);
}

void MainMenuScene::checkAndSetFurthestLevel()
{
    int furthestLevel = DatabaseService::getIntegerForKey("furthest_level");
    if (furthestLevel < 1)
    {
        DatabaseService::setIntegerForKey("furthest_level", 1);
    }
}

void MainMenuScene::doNothing(cocos2d::Ref* sender)
{

}

void MainMenuScene::onSettings(Ref* sender)
{
   // SoundEngine::playEffect("Sounds/Button_Press.wav");
    auto settingsButton = (Node*)sender;

    float animationTime = 2;

    float settingsMoveTime = 0.2;

    auto settingsPosition = PositioningHelper::getRelativePosition(0.5, 0.2);

    if (_settingsOut == false)
    {
        auto rotateElastic = EaseElasticOut::create(RotateBy::create(1, 180));
        auto moveToEase = MoveTo::create(settingsMoveTime, PositioningHelper::getRelativePosition(0.5, 0.1));

        settingsButton->runAction(Spawn::createWithTwoActions(rotateElastic, moveToEase));

        _exit->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, PositioningHelper::getRelativePosition(0.9, 0.4))),
            NULL
            ));
        _about->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, PositioningHelper::getRelativePosition(0.25, 0.3))),
            NULL
            ));
        _help->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, PositioningHelper::getRelativePosition(0.75, 0.3))),
            NULL
            ));

        _soundOn->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, PositioningHelper::getRelativePosition(0.1, 0.4))),
            NULL
            ));
        _soundOff->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, PositioningHelper::getRelativePosition(0.1, 0.4))),
            NULL
            ));

        _statistics->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, PositioningHelper::getRelativePosition(0.5, 0.25))),
            NULL
            ));
    }
    else
    {
        auto rotateElastic = EaseElasticOut::create(RotateBy::create(1, 180));
        auto moveToEase = MoveTo::create(settingsMoveTime, PositioningHelper::getRelativePosition(0.5, 0.2));

        settingsButton->runAction(Spawn::createWithTwoActions(rotateElastic, moveToEase));

        _exit->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, settingsPosition)),
            NULL
            ));
        _about->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, settingsPosition)),
            NULL
            ));
        _help->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, settingsPosition)),
            NULL
            ));

        _soundOn->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, settingsPosition)),
            NULL
            ));
        _soundOff->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, settingsPosition)),
            NULL
            ));

        _statistics->runAction(Sequence::create(
            EaseElasticOut::create(MoveTo::create(animationTime, settingsPosition)),
            NULL
            ));
    }
    _settingsOut = !_settingsOut;
}

void MainMenuScene::onPlayButton(cocos2d::Ref * sender)
{
    SoundEngine::playEffect("Sounds/Button_Press.wav");
    this->removeAllChildren();

    auto menuScene = (MenuScene*)(this->getParent());
    menuScene->navigateToWorldSelection();
}

void MainMenuScene::onStatistics(cocos2d::Ref * sender)
{
    this->removeAllChildren();

    auto menuScene = (MenuScene*)(this->getParent());
    menuScene->navigateToStatisticScene();
}

void MainMenuScene::onAbout(cocos2d::Ref * sender)
{
    this->removeAllChildren();
    auto menuScene = (MenuScene*)(this->getParent());
    menuScene->navigateToAboutScene();
}

void MainMenuScene::onTutorial(cocos2d::Ref * sender)
{
    this->removeAllChildren();

    auto menuScene = (MenuScene*)(this->getParent());
    menuScene->navigateToTutorialScene();
}

void MainMenuScene::navigateToGame(CCNode* sender)
{

}

void MainMenuScene::dispose(CCNode* sender)
{
    this->removeAllChildrenWithCleanup(true);
}

void MainMenuScene::fadeComplete(CCNode* sender)
{
    float randomFadeIn = (rand() % 21)/10;
    float randomFadeOut = (rand() % 21)/10;

    CCDelayTime* timeDelay;

    int delayTimeDecider = rand() % 10;
    if (delayTimeDecider < 5)
    {
        float randomTimeDelay = (rand() % 21) / 10;
        timeDelay = CCDelayTime::create(randomTimeDelay);
    }
    else
    {
        timeDelay = CCDelayTime::create(0);
    }

    CCFiniteTimeAction* fadeOutComplete = CCCallFuncN::create(this, callfuncN_selector(MainMenuScene::fadeComplete));
    sender->runAction(CCSequence::create(timeDelay, CCFadeIn::create(randomFadeIn), CCFadeOut::create(randomFadeOut), fadeOutComplete, NULL));
}

void MainMenuScene::soundOnOff(cocos2d::CCObject* sender)
{
     auto soundButton = (MenuItem*)sender;
     if (!SoundEngine::muted)
     {
         soundButton->setVisible(false);
         auto menu = this->getChildByTag(99);
         auto soundOff = menu->getChildByTag(4);
         soundOff->setVisible(true);

         float startScale = soundOff->getScaleX();

         soundOff->runAction(CCSequence::create(
             CCScaleTo::create(0.1, startScale*0.9),
             CCScaleTo::create(0.2, startScale*1.1),
             CCScaleTo::create(0.2, startScale * 1), NULL));
         soundButton->runAction(CCSequence::create(
             CCScaleTo::create(0.1, startScale*0.9),
             CCScaleTo::create(0.2, startScale*1.1),
             CCScaleTo::create(0.2, startScale * 1), NULL));

         SoundEngine::soundOff();

     }
     else
     {
         soundButton->setVisible(false);
         auto menu = this->getChildByTag(99);
         auto soundOn = menu->getChildByTag(3);
         soundOn->setVisible(true);


         float startScale = soundOn->getScaleX();
         soundOn->runAction(CCSequence::create(
             CCScaleTo::create(0.1, startScale*0.9),
             CCScaleTo::create(0.2, startScale*1.1),
             CCScaleTo::create(0.2, startScale * 1), NULL));
         soundButton->runAction(CCSequence::create(
             CCScaleTo::create(0.1, startScale*0.9),
             CCScaleTo::create(0.2, startScale*1.1),
             CCScaleTo::create(0.2, startScale * 1), NULL));

         SoundEngine::soundOn();
         SoundEngine::playBackgroundMusic("Sounds/Menu_Music.wav", true);
     }

}

void MainMenuScene::onExitGame(cocos2d::Ref * sender)
{
    Director::getInstance()->end();
}

void MainMenuScene::handleBackNavigation()
{
    // when the back button is clicked on the main menu scene we should exit
    // this motherfucker!
  
}

