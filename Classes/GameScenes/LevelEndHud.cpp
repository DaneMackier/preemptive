#include "LevelEndHud.h"
#include "MainGameScene.h"

#include "Helpers\StringHelper.h"
#include "Helpers\PositioningHelper.h"
#include "GameEngine\StatisticsManager.h"

#include "GameEngine\LevelManager.h"
#include "GameScenes\MenuScene.h"

#include "GameSceneComponents\TagDefinitions.h"

#include "..\cocos\platform\wp8-xaml\cpp\Direct3DInterop.h"

USING_NS_CC;
using namespace ui;

Scene* LevelEndHud::createScene()
{
    auto scene = Scene::create();
    auto layer = LevelEndHud::create();
    layer->gameScene = "LevelEndHud";
    scene->addChild(layer);
    return scene;
}

bool LevelEndHud::init()
{
    if (!Layer::init())
    {
        return false;
    }

    _ratingsPositions.push_back(Vec2(0.3, 0.7));
    _ratingsPositions.push_back(Vec2(0.5, 0.75));
    _ratingsPositions.push_back(Vec2(0.7, 0.7));

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    int worldNumber = LevelManager::getInstance()->getCurrentWorld();
    float buttonSize = 0.25;

    // add the back button to the screen
    Button* levelSelection = Button::create(
        "UserInterface/Shared/LevelSelection_1.png",
        "UserInterface/Shared/LevelSelection_1.png");
    levelSelection->addTouchEventListener(CC_CALLBACK_2(LevelEndHud::onLevelSelection, this));
    levelSelection = (Button*)PositioningHelper::placeSquareWidget(
        levelSelection,
        0.25, 0.5,
        buttonSize);

    Button* nextLevel = Button::create(
        "UserInterface/Shared/Arrow_1.png",
        "UserInterface/Shared/Arrow_1.png");
    nextLevel->addTouchEventListener(CC_CALLBACK_2(LevelEndHud::onNextLevel, this));
    nextLevel = (Button*)PositioningHelper::placeSquareWidget(
        nextLevel,
        0.5, 0.3,
        buttonSize);

    Button* shareScore = Button::create(
        "UserInterface/LevelCleared/Share_1.png",
        "UserInterface/LevelCleared/Share_1.png");
    shareScore->addTouchEventListener(CC_CALLBACK_2(LevelEndHud::onShare, this));
    shareScore = (Button*)PositioningHelper::placeSquareWidget(
        shareScore,
        0.75, 0.5,
        buttonSize);
    int zIndex = 20;

    
    this->addChild(levelSelection, zIndex);
    //this->addChild(restartLevel, zIndex);
    this->addChild(nextLevel, zIndex);
    this->addChild(shareScore, zIndex);

    Button* timesDied = Button::create(
        "UserInterface/LevelSelection/Prediction_1.png",
        "UserInterface/LevelSelection/Prediction_1.png"); 
    timesDied->setTitleText(StringHelper::ConvertIntToString(StatisticsManager::getInstance()->getCurrentLevelDeaths()));
    timesDied->setTitleFontSize(30);
    timesDied = (Button*)PositioningHelper::placeSquareWidget(
        timesDied,
        0.5, 0.6,
        0.2);
    this->addChild(timesDied, zIndex);

    for (int i = 0; i < 3; i++)
    {
        auto ratingsButton = Button::create(
            "UserInterface/LevelSelection/Rate_1_Empty.png",
            "UserInterface/LevelSelection/Rate_1_Empty.png",
            "UserInterface/LevelSelection/Rate_1_Full.png"
            );
        auto ratingPosition = _ratingsPositions.at(i);
        
        PositioningHelper::placeSquareWidget(
            ratingsButton,
            ratingPosition.x, ratingPosition.y,
            0.2);

        if (i <= StatisticsManager::getInstance()->getRatingsValue(LevelManager::getInstance()->getCurrentLevel()))
        {
            ratingsButton->setPressedActionEnabled(true);
            ratingsButton->setBright(false);
        }

        this->addChild(ratingsButton, zIndex);
    }
    


    auto backdrop = Sprite::create("UserInterface/LevelCleared/Container.png");
    PositioningHelper::placeGameItem(
        backdrop,
        0.5,0.5,
        0.8,0.65
        );
    this->addChild(backdrop, zIndex - 1);

    return true;
}

void LevelEndHud::handleBackNavigation()
{
    if (true)
    {

    }
}

void LevelEndHud::onLevelSelection(Ref* sender, Widget::TouchEventType eventType)
{
    if (((Node*)sender)->getOpacity() == 0) return;

    this->removeAllChildrenWithCleanup(true);

    auto newScene = MenuScene::createScene();
    Director::getInstance()->replaceScene((Scene*)newScene);
}

void LevelEndHud::onShare(Ref* sender, Widget::TouchEventType eventType)
{
    if (((Node*)sender)->getOpacity() == 0) return;

    Direct3DInterop::getInstance()->OnShareRating(
        StatisticsManager::getInstance()->getRatingsValue(LevelManager::getInstance()->getCurrentLevel()-1),
        LevelManager::getInstance()->getCurrentLevel()-1);
}

void LevelEndHud::onResetGame(Ref* sender, Widget::TouchEventType eventType)
{
    if (((Node*)sender)->getOpacity() == 0) return;

    // Making sure that the reset does not load the next level when 
    LevelManager::getInstance()->decrementLevel();

    hideHud();

    auto mainGameScene = static_cast<MainGameScene*>(this->getParent()->getChildByTag(TagDefinitions::GAMESCENE));
    mainGameScene->resetGameSceneElements();
    mainGameScene->resumeGame();
}

void LevelEndHud::onNextLevel(Ref* sender, Widget::TouchEventType eventType)
{
    if (((Node*)sender)->getOpacity() == 0) return;

    if (LevelManager::getInstance()->getCurrentLevel() > 19)
    {
        auto newScene = MenuScene::createScene();
        Director::getInstance()->replaceScene(TransitionFade::create(1, newScene));
    }
    else if (LevelManager::getInstance()->getCurrentLevel() > 19 && LevelManager::getInstance()->getCurrentWorld() == 3)
    {
        // Take the player back to World selection when next is pressed and we are
        // at the end of the level.
        auto toBeContinued = Sprite::create("UserInterface/TheEnd.jpg");
        PositioningHelper::placeGameItem(toBeContinued,
            0.5, 0.5,
            1.0, 1.0);
        this->addChild(toBeContinued, 10000);
        CCFiniteTimeAction* fadeOutComplete = CCCallFuncN::create(this, callfuncN_selector(LevelEndHud::fadeComplete));
        toBeContinued->setOpacity(0);

        toBeContinued->runAction(Sequence::create(
            FadeIn::create(1.0),
            DelayTime::create(0.5),
            fadeOutComplete,
            NULL));
        return;
    }

    hideHud();

    // Go to the next level motherfuckers
    auto mainGameScene = static_cast<MainGameScene*>(this->getParent()->getChildByTag(TagDefinitions::GAMESCENE));
    mainGameScene->goToNextLevel();
    mainGameScene->resumeGame();
}

void LevelEndHud::fadeComplete(CCNode* sender)
{
    auto newScene = MenuScene::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(1, newScene));
}

void LevelEndHud::showHud()
{
    auto hudElements = this->getChildren();

    for (auto hudElement = hudElements.begin(); hudElement != hudElements.end(); hudElement++)
    {
        (*hudElement)->runAction(FadeIn::create(0.5));
    }
}

void LevelEndHud::hideHud()
{
    auto hudElements = this->getChildren();

    for (auto hudElement = hudElements.begin(); hudElement != hudElements.end(); hudElement++)
    {
        (*hudElement)->setOpacity(0);
    }
}