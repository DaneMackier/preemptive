#ifndef __MAINMENUSCENE_H__
#define __MAINMENUSCENE_H__

#include "cocos2d.h"
#include "GameSceneComponents\GameScene.h"

#include "ui\UIButton.h"
#include "ui\UIHelper.h"


class MainMenuScene : public GameScene
{
private:
    cocos2d::MenuItem* _exit;
    cocos2d::MenuItem* _about;
    cocos2d::MenuItem* _help;
    cocos2d::MenuItem* _soundOn;
    cocos2d::MenuItem* _soundOff;
    cocos2d::MenuItem* _statistics;

    bool _settingsOut;

public:
    bool muted;

    void addBackgroundContent();
    void placeMenuItems();
    void checkAndSetFurthestLevel();
    
    // button actions
    void doNothing(cocos2d::Ref* sender);
    
    /*
    Callback function when the start buton is pressed
    */
    void onPlayButton(cocos2d::Ref * sender);


    /*
    Callback function when the start buton is pressed
    */
    void onStatistics(cocos2d::Ref * sender);


    /*
    Callback function when the start buton is pressed
    */
    void onTutorial(cocos2d::Ref * sender);

    /*
    Callback function for the sound
    */
    void soundOnOff(cocos2d::Ref* sender);

    /*
    Exits the game
    */
    void onAbout(cocos2d::Ref * sender);

    /*
    Exits the game
    */
    void onExitGame(cocos2d::Ref * sender);

    /*
    Action called when the settings button is clicked
    */
    void onSettings(cocos2d::Ref* sender);

    /*
    Removes all the children from this element
    */
    void dispose(cocos2d::CCNode* sender);

    // callback functions
    void navigateToGame(cocos2d::CCNode* sender);
    void fadeComplete(cocos2d::CCNode* sender);

    static cocos2d::Layer* getGameOverMenuLayer();
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MainMenuScene);

    virtual void handleBackNavigation();
};

#endif