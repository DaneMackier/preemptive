#include "MenuScene.h"
#include "BackgroundMenuScene.h"
#include "MiddleMenuScene.h"
#include "WorldSelectionScene.h"
#include "LevelSelectionScene.h"
#include "MainMenuScene.h"
#include "TutorialScene.h"
#include "StatisticsScene.h"
#include "AboutScene.h"

#include "GameSceneComponents\TagDefinitions.h"

#include "Helpers\AnimationHelper.h"
#include "Helpers\DatabaseService.h"
#include "Helpers\StringHelper.h"

#include "GameEngine\LevelManager.h"
#include "GameEngine\SoundEngine.h"

USING_NS_CC;

ParallaxNode* MenuScene::_paralaxNode = nullptr;
float MenuScene::_paralaxTime = 0.5f;
float MenuScene::_paralaxRate = 0.4f;

cocos2d::Scene* MenuScene::createScene()
{
    auto scene = Scene::create();

    auto layer = MenuScene::create();
    layer->gameScene = "MainMenu";
    layer->setTag(TagDefinitions::GAMESCENE);

    scene->addChild(layer);
    return scene;
}

bool MenuScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    auto alphaBackground = cocos2d::Sprite::create("UserInterface/BlackLayover.png");
    alphaBackground = PositioningHelper::placeGameItem(alphaBackground, 0.5, 0.5, 1, 1);
    alphaBackground->setOpacity(80);
    this->addChild(alphaBackground, 1);

    initializeParalaxMenu();

    SoundEngine::stopAllSounds();
    SoundEngine::setEffectsVolume(0.3f);
    SoundEngine::setBackgroundVolume(0.8f);
    SoundEngine::playBackgroundMusic("Sounds/Menu_Music.wav", true);

    return true;
}

void MenuScene::initializeParalaxMenu()
{
    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    // initialize paralax node
    _paralaxNode = ParallaxNode::create();

    // TOP LAYER
    auto mainMenuScene = MainMenuScene::createScene();
    mainMenuScene->setAnchorPoint(Vec2(0, 0));
    mainMenuScene->setTag(TagDefinitions::TOPSCENE);
    this->addChild(mainMenuScene, 1);

    GameScene::_currentScene = MAINMENU;

    int selectedWorld = LevelManager::getInstance()->getCurrentWorld();

    auto backgroundLayer = Sprite::create("UserInterface/MainMenu/World_" + StringHelper::ConvertIntToString(selectedWorld) + "_Background.jpg");
    PositioningHelper::placeGameItem(backgroundLayer, 0, 0, 3.5, 1);

    auto middleBackLayer = Sprite::create("UserInterface/MainMenu/World_" + StringHelper::ConvertIntToString(selectedWorld) + "_MidBack.png");
    PositioningHelper::placeGameItem(middleBackLayer, 0, 0, 3.5, 1);

    auto middleFronLayer = Sprite::create("UserInterface/MainMenu/World_" + StringHelper::ConvertIntToString(selectedWorld) + "_MidFront.png");
    PositioningHelper::placeGameItem(middleFronLayer, 0, 0, 3.5, 1);

    auto frontLayer = Sprite::create("UserInterface/MainMenu/World_" + StringHelper::ConvertIntToString(selectedWorld) + "_Front.png");
    PositioningHelper::placeGameItem(frontLayer, 0, 0, 3.5, 1);

    backgroundLayer->setAnchorPoint(Vec2(0, 0));
    middleBackLayer->setAnchorPoint(Vec2(0, 0));
    middleFronLayer->setAnchorPoint(Vec2(0, 0));
    frontLayer->setAnchorPoint(Vec2(0, 0));

    _paralaxNode->addChild(backgroundLayer, 0, Vec2(0.4f, 0.5f), Vec2(0, 0));
    _paralaxNode->addChild(middleBackLayer, 0, Vec2(1.2f, 0.9f), Vec2(0, 0));
    _paralaxNode->addChild(middleFronLayer, 0, Vec2(1.8f, 1.5f), Vec2(0, 0));
    _paralaxNode->addChild(frontLayer, 0, Vec2(2.5f, 0.5f), Vec2(0, 0));

    float movementSpeed = 40;

    auto paralaxSequence = RepeatForever::create(
        Sequence::create(
        MoveBy::create(movementSpeed, -PositioningHelper::getRelativePosition(1, 0)),
        MoveBy::create(movementSpeed, PositioningHelper::getRelativePosition(1, 0)),
        NULL));

    _paralaxNode->runAction(paralaxSequence);

    this->addChild(_paralaxNode, 0);
}

void MenuScene::navigateToLevelSelection()
{
    GameScene::_currentScene = LEVELSELECTION;
    auto levelSelection = LevelSelectionScene::create();
    levelSelection->setTag(TagDefinitions::TOPSCENE);
    this->addChild(levelSelection);
}

void MenuScene::navigateToWorldSelection()
{
    GameScene::_currentScene = WORLDSELECTION;
    auto worldSelections = WorldSelectionScene::create();
    worldSelections->setTag(TagDefinitions::TOPSCENE);
    this->addChild(worldSelections);
}

void MenuScene::navigateToMainMenu()
{
    GameScene::_currentScene = MAINMENU;
    auto mainMenuScene = MainMenuScene::create();
    mainMenuScene->setTag(TagDefinitions::TOPSCENE);
    this->addChild(mainMenuScene);
}

void MenuScene::navigateToStatisticScene()
{
    GameScene::_currentScene = MAINMENU;
    auto statisticScene = StatisticsScene::create();
    statisticScene->setTag(TagDefinitions::TOPSCENE);
    this->addChild(statisticScene);
}

void MenuScene::navigateToTutorialScene()
{
    GameScene::_currentScene = MAINMENU;
    auto tutorialScene = TutorialScene::create();
    tutorialScene->setTag(TagDefinitions::TOPSCENE);
    this->addChild(tutorialScene);
}

void MenuScene::navigateToAboutScene()
{
    GameScene::_currentScene = ABOUT;
    auto tutorialScene = AboutScene::create();
    tutorialScene->setTag(TagDefinitions::TOPSCENE);
    this->addChild(tutorialScene);
}

void MenuScene::handleBackNavigation()
{
    CCLog("Back key pressed in the menu scene");

    switch (GameScene::_currentScene)
    {
        case MAINMENU:
        {
            // Exit if we are on the main menu. Or should we pop up a thing?
            Director::getInstance()->end();
            break;
        }
        case WORLDSELECTION:
        {
            auto topScene = static_cast<WorldSelectionScene*>(this->getChildByTag(TagDefinitions::TOPSCENE));
            topScene->backButtonPressed();
            break;
        }
        case LEVELSELECTION:
        {
            auto topScene = static_cast<LevelSelectionScene*>(this->getChildByTag(TagDefinitions::TOPSCENE));
            topScene->backButtonPressed();
            break;
        }
    }

}
