#ifndef __PAUSEMENUHUD_H__
#define __PAUSEMENUHUD_H__


#include "cocos2d.h"

#include "GameSceneComponents\GameScene.h"
#include "GameSceneComponents\MovementArrow.h"

#include "Box2D\Box2D.h"
#include "ui\CocosGUI.h"

enum SELECTED_SIDE
{
    LEFTMOVEMENT = 0,
    RIGHTMOVEMENT = 1,
};

class PauseMenuHud : public GameScene
{
private:

    cocos2d::ui::Button* _timesDied;

    /*
    Contains all the elements of the level end hud
    */
    cocos2d::Layer* _levelEndHud;

    /*
    True when intro animation is stil being played out
    */
    bool _introductionShown;


    /*
    Indicates if the user can press the buttons yet
    */
    bool _playable;

    /*
    Indicates if the game has been pause
    */
    bool _paused;

    /*
    Contains a mapping of movementId to rotational value
    e.g movementId:right = rotationValue:180
    */
    std::map<std::string, float> _arrowRotations;

    /*
    Layer that contains the correct movements to finish the level
    */
    cocos2d::Layer* _correctMovements;

    /*
    Menu that contains all the playout movements that will be presented to the player
    */
    cocos2d::Menu* _playoutMovements;

    /*
    Alpha background used when displaying the menu options
    */
    cocos2d::Sprite* _alphaBackground;

    /*
    Number of movements that will fit on the screen
    currently set t 16
    */
    int _numberOfMovements;

    /*
    Keeps count of the number of correct movements on screen currently
    being displayed to the user on screen
    */
    int _correctMovementsOnScreen;

    /*
    Keeps track of the index of the current movement is shown on screen
    */
    int _playoutMovementIndex;

    /*
    A list of pointers to the movementArrows on screen
    */
    cocos2d::Vector<MovementArrow*> movementArrows;

    /*
    Keeps pointers to the movements that will be presented to the player
    */
    std::vector<b2Vec2> _leftMovements;
    std::vector<b2Vec2> _rightMovements;

    void initializeArrowRotationValues();
    void placeHudElements();


    /*
    Removes pause menu buttons from the screen
    */
    void removePauseMenu();

    
    void onPauseGame(cocos2d::Ref* sender);
    void onLevelSelection(cocos2d::Ref* sender);
    void onSoundSelected(cocos2d::Ref* sender);
    void onResetGame(cocos2d::Ref* sender);
    void onMainMenu(cocos2d::Ref* sender);

    /*
    Callback function called when the left button is clicked
    */
    void onLeftMovementSelected(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Callback function called when right button is clicked
    */
    void onRightMovementSelected(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Selector to call when the movement animations are complete
    */
    void onMovementAnimationComplete(cocos2d::Node* sender);

    /*
    Set the level hud playable
    */
    void onGamePlayable(cocos2d::Node* sender);

    /*
    Handles the logic for when a movement is clicked
    */
    void movementSelected(SELECTED_SIDE selectedSide);

    /*
    Creates and adds the left/right movement to its list
    */
    void addLeftMovement(b2Vec2 movement, int index, float arrowSize, float distanceBetweenFactor);
    void addRightMovement(b2Vec2 movement, int index, float arrowSize, float distanceBetweenFactor);

    /*
    Adds an arrow into the list og other correct movements to be shown in the HUD
    */
    void addCorrectMovementSprite(b2Vec2 movement, int index);

    /*
    applies rotation to the movement arrow
    */
    MovementArrow* applyArrowRotation(std::string rotationId, MovementArrow* movementArrow);

    /*
    Returns the movement id that maps to the rotation angle of the arrow
    */
    std::string getMovementIdentifier(b2Vec2 movement);

    /*
    
    */
    void addMovementToPlayerMovements(b2Vec2 movement, float scaler);

    /*
    Gets a b2Vec2 depending on the number passed in
    */
    b2Vec2 getVectorForId(int vectorId);

    /*
    Takes the current movments on screen and runs the appropriate actions.
    */
    void runAnimationForCurrentMovements();

public:

    /*
    Contains all the movements defined in the xml for this level
    */
    std::vector<b2Vec2> definedLevelMovements;

    /*
    Should be called when the animation for the player intro sequence is complete/started
    */
    void setIntroductionShown(bool value);

    /*
    Loads the arrow options that will be displayed to the user to play out
    movements with
    */
    void initializePlayoutMovements();

    /*
    Loads the correct movements for this game that will be shown in the
    HUD
    */
    void initializeCorrectMovements();

    /*
    Reset the playout movements
    */
    void resetPlayoutMovements();

    /*
    Reads the total number of deaths and sets the number of moves
    */
    void setNumberOfDeaths();

    /*
    Plays out the movement that has just been used by the player
    Changes the HUD/lights up correct movements
    */
    void playOutMovement(int currentMovement);

    /*
    Clears all the current arrows from the HUD menu
    */
    void clearCorrectArrows();

    /*
    Disables all the arrows on the screen
    */
    void resetCorrectArrows();

    /*
    Clears the playout movements from the scene
    */
    void clearPlayoutMovements();

    void showLevelEndHud();

    void hideLevelEndHud();

    void resetGame();

    void goToNextLevel();

    // Add all the buttons that will be used in the pause menu onto the layer
    void showPauseMenu();

    void handlePauseMenuRequest();

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(PauseMenuHud);

    virtual void handleBackNavigation();
};




#endif