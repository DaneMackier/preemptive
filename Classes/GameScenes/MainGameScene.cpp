#include "MainGameScene.h"
#include "MainMenuScene.h"
#include "GameOverScene.h"
#include "PauseMenuHud.h"
#include "MainGameBackground.h"
#include "MenuScene.h"
#include "LevelEndHud.h"

#include "Helpers\ScaleHelper.h"
#include "Helpers\AnimationHelper.h"
#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"
#include "Helpers\XMLHelper.h"
#include "Helpers\PhysicsHelper.h"

#include "GameEngine\SoundEngine.h"
#include "GameEngine\CustomFollow.h"
#include "GameEngine\LevelManager.h"
#include "GameEngine\StatisticsManager.h"

#include "GameSceneComponents\TagDefinitions.h"
#include "GameSceneComponents\ContactListener.h"
#include "GameSceneComponents\MiddleGameSceneLayer.h"
#include "GameSceneComponents\Shooter.h"

#include "ThirdPartyHelpers\GLESRender.h"
#include "ThirdPartyHelpers\pugiconfig.h"
#include "ThirdPartyHelpers\pugixml.h"


#define PTM_RATIO 32.0

USING_NS_CC;

// We need classes from this namespace
//using namespace cocos2d::extension;

Scene* MainGameScene::_parentScene = nullptr;

Scene* MainGameScene::createScene()
{
    _parentScene = Scene::create();

    // add the game layer. Where all the gameplay will happen on
    auto gameLayer = MainGameScene::create();
    auto gameBackground = MainGameBackground::create();
    auto middleGameScene = MiddleGameSceneLayer::create();
    auto hud = PauseMenuHud::create();

    gameBackground->setTag(TagDefinitions::MAINGAMEBACKGROUNDLAYER);
    middleGameScene->setTag(TagDefinitions::MIDDLEGAMESCENELAYER);

    _parentScene->addChild(gameBackground, 0);
    _parentScene->addChild(middleGameScene, 1);
    _parentScene->addChild(gameLayer, 2);
    _parentScene->addChild(hud, 3);

    gameLayer->setTag(TagDefinitions::GAMESCENE);
    gameLayer->gameScene = "MainGameScene";

    gameLayer->hudLayer = hud;
    gameLayer->gameSceneLayer = gameLayer;
    gameLayer->backgroundLayer = gameBackground;
    gameLayer->middleGameSceneLayer = middleGameScene;
    
    return _parentScene;
}

bool MainGameScene::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    SoundEngine::stopAllSounds();
    SoundEngine::setEffectsVolume(0.3f);
    SoundEngine::playBackgroundMusic("Sounds/World_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "_Background.wav", true);

    _currentScene = MAINGAME;

    _introductionShown = false;

    _levelCompleteAnimation = Sequence::create(
        RotateBy::create(0.5, 360),
        NULL);

    initializeClassVariables();                    // initialize variables that's being used throughout the class                         
    initializeLevel();

    return true;
}

void MainGameScene::initializeLevel()
{
    initializePhysicsWorld();                      // initialize box2D physics world
    initializeContactListener();                   // initialize contact listener for layer

    loadLevel();                                   // load the level items for this level

    //enableDebugDraw();                             // use for debug purposed only

    if (!_introductionShown)
    {
        runPlayerIntroSequence();
    }

    this->runAction(CustomFollow::create(_player));

    // Scheduling like below is not good. It's better to set it to 60 ticks every second.
    this->schedule(schedule_selector(MainGameScene::update));
}

void MainGameScene::runPlayerIntroSequence()
{
    auto playerStartPosition = _player->getPosition();

    CCFiniteTimeAction* introductionCompleteCallback = CCCallFuncN::create(this, callfuncN_selector(MainGameScene::onIntroductionComplete));

    auto playerIntroductionSequence = CCSequence::create(
        CCDelayTime::create(2),
        CCMoveTo::create(3, Vec2(-100, playerStartPosition.y)),
        CCMoveTo::create(0.5, playerStartPosition),
        introductionCompleteCallback,
        NULL);

    _player->setPosition(Vec2(-100, _portalPosition.y - 150));

    _player->runAction(playerIntroductionSequence);
}

void MainGameScene::initializeClassVariables()
{
    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    _movementMagnitude = 1.85;

    _currentMovement = 0;
    _playerAddedToMiddleLayer = false;
}

void MainGameScene::addPlayerToMiddleGameLayer()
{
    static_cast<MiddleGameSceneLayer*>(middleGameSceneLayer)->addPlayerToMiddleLayerFollow(_player);
}

void MainGameScene::initializePhysicsWorld()
{
    b2Vec2 gravity = b2Vec2(0.0f, -9.0f);
    World = new b2World(gravity);

    initializeWorldGround();
}

void MainGameScene::initializeWorldGround()
{
    PhysicsHelper::createBoundaryFixture(World, b2Vec2(0, 0), b2Vec2(0, 0), b2Vec2(PositioningHelper::getVisibleWidth() / PTM_RATIO, 0));
    PhysicsHelper::createBoundaryFixture(World, b2Vec2(0, 0), b2Vec2(0, 0), b2Vec2(0, PositioningHelper::getVisibleHeight() * 3 / PTM_RATIO));
    PhysicsHelper::createBoundaryFixture(World, b2Vec2(PositioningHelper::getVisibleWidth() / PTM_RATIO, 0), b2Vec2(0, 0), b2Vec2(0, PositioningHelper::getVisibleHeight() * 3 / PTM_RATIO));

    auto groundAsset = "LevelItems/World_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentWorld()) + "/Ground_" + StringHelper::ConvertIntToString(LevelManager::getInstance()->getCurrentLevelAssetNumber()) + ".png";
    auto ground = Sprite::create(groundAsset);
    ground = PositioningHelper::placeGameItem(
        ground, 0.5, 0.00, 1, 0.35);
    this->addChild(ground);
}

void MainGameScene::initializeContactListener()
{
    World->SetContactListener(this);
}

void MainGameScene::initializePlayerFromLevelItem(GameItem* gameItem)
{
    _player = Player::createPlayerFromGameItem(gameItem);

    // physics related setup
    auto playerBodyDef = PhysicsHelper::createPlayerBodyDef(_player);
    _playerPhysicsBody = World->CreateBody(&playerBodyDef);
    PhysicsHelper::createPlayerPhysics(World, _player, _playerPhysicsBody);

    _player->setTag(TagDefinitions::PLAYERTAG);

    this->addChild(_player);
}

void MainGameScene::initializeShooterFromLevelItem(GameItem* gameItem)
{
    // create a shooter item
    auto shooter = static_cast<Shooter*>(gameItem);

    // set shooter item tag
    shooter->setTag(TagDefinitions::SHOOTER_TAG);

    // add shooter to the current scene
    this->addChild(shooter, 1);

    // add shooter into the shooters list
    _shooters.push_back(shooter);
}

// -------------- Level related functionality ----------
void MainGameScene::loadLevel()
{
    auto levelItems = XMLHelper::getLevelSprites(LevelManager::getInstance()->getLevelPath());
    
    for (int i = 0; i < levelItems.size(); i++)
    {
        auto levelItem = levelItems.at(i);

        if (levelItem->itemKind == "Player")
        {
            initializePlayerFromLevelItem(levelItem);
        }
        else if (levelItem->itemKind == "Shooter")
        {
            initializeShooterFromLevelItem(levelItem);
        }
        else
        {
            placeLevelItem(levelItem);
        }
    }
}

void MainGameScene::dispose()
{
    this->removeAllChildrenWithCleanup(true);
}

// ------------------- HELPER FUNCTIONS ---------------------

void MainGameScene::placeLevelItem(LevelItem* levelItem)
{
    if (levelItem->itemKind == "Portal")
    {
        _portalPosition = levelItem->getPosition();
        
        auto rotateAction = RotateBy::create(1, 360);
        levelItem->runAction(RepeatForever::create(rotateAction));
        levelItem->runAction(ScaleBy::create(0.2, 1.4));
    }

    PhysicsHelper::createGameItemFixture(World, levelItem);

    levelItem->setTag(TagDefinitions::GAMESCENELAYER);
    this->addChild(levelItem);
}

void MainGameScene::enableDebugDraw()
{
    auto debug = new GLESDebugDraw(PTM_RATIO);
    //Debug flags
    uint32 flags = 0;
    flags += b2Draw::e_aabbBit;
    flags += b2Draw::e_centerOfMassBit;
    flags += b2Draw::e_jointBit;
    flags += b2Draw::e_pairBit;
    flags += b2Draw::e_shapeBit;
    debug->SetFlags(flags);
    World->SetDebugDraw(debug);
}

void MainGameScene::update(float delta)
{
    if (!_playerAddedToMiddleLayer)
    {
        static_cast<MiddleGameSceneLayer*>(middleGameSceneLayer)->addPlayerToMiddleLayerFollow(_player);
        _playerAddedToMiddleLayer = true;
    }

    updateShooterAngle();

    World->Step(delta, 10, 10);

    for (b2Body* physicsBody = World->GetBodyList(); physicsBody; physicsBody = physicsBody->GetNext())
    {
        auto userData = physicsBody->GetUserData();
        if (userData != NULL){
            auto levelItemData = static_cast<LevelItem*>(userData);
            auto physicsBodyPosition = physicsBody->GetPosition();
            
            if (levelItemData == NULL) return;

            // Remove the physics body from the world that has been marked for deletion
            if (levelItemData->isMarkForDelete())
            {
                World->DestroyBody(physicsBody);
                physicsBody->SetUserData(nullptr);
                physicsBody = nullptr;
                return;
            }

            if (levelItemData->tag == "Player" || levelItemData->tag == "GravityProjectile")
            {
                if (_introductionShown)
                {
                    levelItemData->setPosition(physicsBodyPosition.x * PTM_RATIO,
                        physicsBodyPosition.y * PTM_RATIO);
                }
               
                auto tempMovementsSize = static_cast<PauseMenuHud*>(hudLayer)->definedLevelMovements.size();
                // Make sure that when the player has no more movements left the level is
                // restarted.
                if (_playerPhysicsBody->GetLinearVelocity().y < -4.0f
                    &&  tempMovementsSize == _currentMovement)
                {
                    this->unschedule(schedule_selector(MainGameScene::update));

                    StatisticsManager::getInstance()->incrementCurrentLevelDeaths();
                    static_cast<PauseMenuHud*>(hudLayer)->setNumberOfDeaths();

                    resetGameSceneElements();
                    return;
                }
            }

            // logic for saw or Projectile
            if (levelItemData->tag == "Saw" || levelItemData->tag == "LinearProjectile")
            {
                physicsBody->SetTransform(b2Vec2(
                    levelItemData->getPositionX() / PTM_RATIO,
                    levelItemData->getPositionY() / PTM_RATIO),
                    -1 * CC_DEGREES_TO_RADIANS(levelItemData->getRotation()));

                physicsBody->SetActive(true);

                if (levelItemData->getOpacity() == 0)
                {
                    physicsBody->SetActive(false);
                }
            }
        }
    }
}

void MainGameScene::updateShooterAngle()
{
    for (int i = 0; i < _shooters.size(); i++)
    {
        _shooters.at(i)->update(_player);
    }
}

void MainGameScene::BeginContact(b2Contact* contact)
{
    auto objectOne = contact->GetFixtureA()->GetBody();
    auto objectTwo = contact->GetFixtureB()->GetBody();

    auto objectOneUserData = (objectOne->GetUserData());
    auto objectTwoUserData = (objectTwo->GetUserData());

    if (objectOneUserData == NULL || objectTwoUserData == NULL) return;

    auto objectOneGameItem = static_cast<GameItem*>(objectOneUserData);
    auto objectTwoGameItem = static_cast<GameItem*>(objectTwoUserData);

    if (objectOneGameItem->tag.empty() || objectTwoGameItem->tag.empty()) return;

    std::string objectOneTag(objectOneGameItem->tag);
    std::string objectTwoTag(objectTwoGameItem->tag);

    if (objectOneTag.empty() || objectTwoTag.empty()) return;

    if (playerPortalContact(objectOneTag, objectTwoTag))
    {
        StatisticsManager::getInstance()->setRatingValue(LevelManager::getInstance()->getCurrentLevel());
        pauseGame();

        _player->runAction(ScaleTo::create(0.25, 0));

        // place the game end layer on the screen before we reset anything
        auto gameEndLayer = LevelEndHud::create();
        gameEndLayer->hideHud();
        _parentScene->addChild(gameEndLayer, 4);
        this->gameOverLayer = gameEndLayer;
        static_cast<LevelEndHud*>(gameOverLayer)->showHud();      

        // save the stats we need thoughout the game
        StatisticsManager::getInstance()->saveLeastDeaths();
        StatisticsManager::getInstance()->saveMostDeaths();
        StatisticsManager::getInstance()->resetCurrentLevelDeaths();

        // increment the level as soon you hit the portal
        LevelManager::getInstance()->incrementLevel();
        LevelManager::getInstance()->setFurthestLevel(LevelManager::getInstance()->getCurrentLevel());

        static_cast<MiddleGameSceneLayer*>(middleGameSceneLayer)->resetMiddleScene();
        static_cast<MainGameBackground*>(backgroundLayer)->resetScene();

        _currentScene = LEVELEND;

        static_cast<PauseMenuHud*>(hudLayer)->setNumberOfDeaths();
        return;
    }
    else if (dangerousContact(objectOneTag, objectTwoTag))
    {
        float objOneOpacity = objectOneGameItem->getOpacity();
        float objTwoOpacity = objectTwoGameItem->getOpacity();
        if (objOneOpacity == 0 || objTwoOpacity == 0)
        {

        }
        else
        {
            SoundEngine::playEffect("Sounds/Player_Death.wav");

            StatisticsManager::getInstance()->incrementCurrentLevelDeaths();
            static_cast<PauseMenuHud*>(hudLayer)->setNumberOfDeaths();

            resetGameSceneElements();
        }
        return;
    }
    else if (projectileBoundaryContact(objectOneTag, objectTwoTag) || projectileSawContact(objectOneTag, objectTwoTag))
    {
        if (objectOneTag == "GravityProjectile")
        {
            objectOneGameItem->runAction(FadeOut::create(0.25));
            objectOneGameItem->markForDeletion();
        }
        else
        {
            objectTwoGameItem->runAction(FadeOut::create(0.25));
            objectTwoGameItem->markForDeletion();
        }
    }
    else if (projectileProjectileContact(objectOneTag, objectTwoTag))
    {
        // Do nothing
    }

  
}

void MainGameScene::fadeComplete(CCNode* sender)
{
    auto newScene = MenuScene::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(1, newScene));
}

bool MainGameScene::playerPortalContact(std::string objectOneTag, std::string objectTwoTag)
{
    return  (objectOneTag == "Player" && objectTwoTag == "Portal") ||
        (objectTwoTag == "Player" && objectOneTag == "Portal");
}

bool MainGameScene::playerSawContact(std::string objectOneTag, std::string objectTwoTag)
{
    return  (objectOneTag == "Player" && objectTwoTag == "Saw") ||
        (objectOneTag == "Saw" && objectTwoTag == "Player"); 
}

bool MainGameScene::playerProjectileContact(std::string objectOneTag, std::string objectTwoTag)
{
    return  playerLinearProjectile(objectOneTag, objectTwoTag) ||
        playerGravityProjectile(objectOneTag, objectTwoTag);
}

bool MainGameScene::playerLinearProjectile(std::string objectOneTag, std::string objectTwoTag)
{
    return (objectOneTag == "Player" && objectTwoTag == "LinearProjectile") ||
        (objectOneTag == "LinearProjectile" && objectTwoTag == "Player");
}

bool MainGameScene::playerGravityProjectile(std::string objectOneTag, std::string objectTwoTag)
{
    return (objectOneTag == "Player" && objectTwoTag == "GravityProjectile") ||
        (objectOneTag == "GravityProjectile" && objectTwoTag == "Player");
}

bool MainGameScene::dangerousContact(std::string objectOneTag, std::string objectTwoTag)
{
    return (playerSawContact(objectOneTag, objectTwoTag)) ||
        (playerProjectileContact(objectOneTag, objectTwoTag));
}

bool MainGameScene::projectileBoundaryContact(std::string objectOneTag, std::string objectTwoTag)
{
    return  (objectOneTag == "Boundary" && objectTwoTag == "GravityProjectile") ||
        (objectOneTag == "GravityProjectile" && objectTwoTag == "Boundary");
}

bool MainGameScene::projectileProjectileContact(std::string objectOneTag, std::string objectTwoTag)
{
    return  (objectOneTag == "GravityProjectile" && objectTwoTag == "GravityProjectile") ||
        (objectOneTag == "GravityProjectile" && objectTwoTag == "GravityProjectile");
}

bool MainGameScene::projectileSawContact(std::string objectOneTag, std::string objectTwoTag)
{
    return  (objectOneTag == "GravityProjectile" && objectTwoTag == "Saw") ||
        (objectOneTag == "Saw" && objectTwoTag == "GravityProjectile");
}

void MainGameScene::resetGameSceneElements()
{
    _currentScene = MAINGAME;

    _currentMovement = 0;
    
    _shooters.clear();
    _movements.clear();

    static_cast<PauseMenuHud*>(hudLayer)->resetCorrectArrows();
    static_cast<PauseMenuHud*>(hudLayer)->resetPlayoutMovements();

    this->removeChildByTag(TagDefinitions::GAMESCENELAYER, true);
    this->removeAllChildrenWithCleanup(true);

    _playerAddedToMiddleLayer = false;

    _parentScene->removeChild(gameOverLayer, true);

    this->unschedule(schedule_selector(MainGameScene::update));
    initializeLevel();

    for (auto shooter : _shooters)
    {
        shooter->startShooting();
    }
}

void MainGameScene::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    CCNode::draw(renderer, transform, true);
    World->DrawDebugData();
}

void MainGameScene::onIntroductionComplete(Node* sender)
{
    _introductionShown = true;
    static_cast<PauseMenuHud*>(hudLayer)->setIntroductionShown(_introductionShown);

    for (auto shooter : _shooters)
    {
        shooter->startShooting();
    }
}

void MainGameScene::pauseGame()
{
    _currentScene = PAUSE;
    this->unschedule(schedule_selector(MainGameScene::update));
}

void MainGameScene::resumeGame()
{
    _currentScene = MAINGAME;
    this->schedule(schedule_selector(MainGameScene::update));
}

void MainGameScene::playOutSelectedMovement(b2Vec2 movement)
{
    float playSoundDecider = rand() % 6 + 1;

    if (playSoundDecider < 3.0f)
    {
        SoundEngine::playEffect("Sounds/Player_Sound.wav");
    }
    
    if (movement.x == 1) // make the player face right
    {
        _player->setFlipX(false);
    }
    else if (movement.x == -1) // make the player face left
    {
        _player->setFlipX(true);
    }

    float scaler = 10 * _movementMagnitude;
    auto movementToApply = b2Vec2(movement.x * scaler, movement.y * scaler);
    _playerPhysicsBody->SetLinearVelocity(b2Vec2(0, 0));
    _playerPhysicsBody->ApplyLinearImpulse(movementToApply, _playerPhysicsBody->GetPosition(), true);

    // play out the movement in the game HUD
    _currentMovement++;
    static_cast<PauseMenuHud*>(hudLayer)->playOutMovement(_currentMovement);

    _player->runTappedAnimation();       
}

void MainGameScene::goToNextLevel()
{
    World = nullptr;
    _player = nullptr;

    _introductionShown = false;
    static_cast<PauseMenuHud*>(hudLayer)->setIntroductionShown(_introductionShown);

    // Indicate that the player has collided with the portal
    _playerCollidedWithPortal = true;

    static_cast<PauseMenuHud*>(hudLayer)->clearCorrectArrows();
    static_cast<PauseMenuHud*>(hudLayer)->initializeCorrectMovements();

    resetGameSceneElements();
}

void MainGameScene::handleBackNavigation()
{
    CCLog("Back button pressed in the Main Game Scene");
    switch (_currentScene)
    {
        case MAINGAME:
        {
            showPauseMenu();
            break;
        }
        case PAUSE:
        {
            showPauseMenu();
            break;
        }
        case LEVELEND:
        {
            break;
        }
    }
    
}

void MainGameScene::showPauseMenu()
{
    //pause();
    static_cast<PauseMenuHud*>(hudLayer)->handlePauseMenuRequest();
}

