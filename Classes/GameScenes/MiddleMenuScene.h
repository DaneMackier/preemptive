#ifndef __MIDDLEMENUSCENE_H__
#define __MIDDLEMENUSCENE_H__

#include "cocos2d.h"

class MiddleMenuScene : public cocos2d::Layer
{
private:
    /*
    Indicates the z value of the elements on this layer
    */
    int _middleLayerLevel;

    /*
    Callback function when lightFade is complete
    */
    void onLightFadeComplete(cocos2d::Node* sender);

    /*
    Callback function to execute when the drop is out of the screen
    */
    void onDropComplete(cocos2d::Node* sender);

    /*
    Adds a drop to the waterlog and runs the action
    */
    void addDropToScene(cocos2d::Node* sender);
    
    /*
    Adds all 6 light rays to the middle menu scene
    */
    void initializeLightRays();

public:
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MiddleMenuScene);
};


#endif