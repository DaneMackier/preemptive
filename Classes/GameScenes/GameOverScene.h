#ifndef __GAMEOVERSCENE_H__
#define __GAMEOVERSCENE_H__

#include "cocos2d.h"
#include "GameSceneComponents\GameScene.h"

class GameOverScene : public cocos2d::Layer
{
public:
    void doNothing(cocos2d::Ref* sender);
    void retry(cocos2d::Ref* sender);
    void quit(cocos2d::Ref* sender);

    bool buttonsActive;

    static cocos2d::Layer* getGameOverMenuLayer();
    void fadeComplete(cocos2d::CCNode* sender);
    void activateButtons(cocos2d::CCNode* sender);


    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(GameOverScene);
};

#endif