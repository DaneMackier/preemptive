#pragma once

#include "cocos2d.h"

#include "ui\CocosGUI.h"

class StatisticsScene : public cocos2d::Layer
{
private:

    /*
    Places a ui label on screen with the values passed in
    */
    void placeGameLabel(
        std::string labelText,
        int value,
        cocos2d::Vec2 titlePosition,
        cocos2d::Vec2 valuePosition);

    /*
    function called when the on screen back button is pressed
    */
    void onBackButtonPressed(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
public:
    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(StatisticsScene);
};
