#ifndef __LEVELSELECTION_H__
#define __LEVELSELECTION_H__

#include "GameSceneComponents\GameScene.h"
#include "GameEngine\LevelManager.h"
#include "GameSceneComponents\LevelSelectionItem.h"

#include "cocos2d.h"

#include "ui\CocosGUI.h"

class LevelSelectionScene : public GameScene
{
public:

    cocos2d::ui::Button* _backButton; 
    
    /*
    Callback function assigned to each LevelSelectionItem that will
    start a game with the selected level number.
    */
    void onLevelSelected(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    function called when the on screen back button is pressed
    */
    void onBackButtonPressed(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

    /*
    Removes all the children from within the current parent
    */
    void dispose(cocos2d::CCNode* sender);

    /*
    Function used to handle back button pressed
    */
    void backButtonPressed();

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(LevelSelectionScene);
};


#endif