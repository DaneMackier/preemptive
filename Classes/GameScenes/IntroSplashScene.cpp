#include "IntroSplashScene.h"
#include "MenuScene.h"
#include "IntroAnimationScene.h"

#include "Helpers/PositioningHelper.h"

#include "cocos2d.h"

USING_NS_CC;

Scene* IntroSplashScene::createScene()
{
    auto scene = Scene::create();
    auto layer = IntroSplashScene::create();
    scene->addChild(layer);
    return scene;
}

bool IntroSplashScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    auto casualisticIntroScene = Sprite::create("CasualisticGamerLogo.png");
    PositioningHelper::placeGameItem(
        casualisticIntroScene,
        0.5, 0.5,
        1, 1);

    casualisticIntroScene->setOpacity(0);

    auto introSequenceComplete = CallFuncN::create(this, callfuncN_selector(IntroSplashScene::onIntroComplete));

    auto introSequence = Sequence::create(
        FadeIn::create(1),
        DelayTime::create(0.5),
        FadeOut::create(0.5),
        introSequenceComplete,
        NULL);

    casualisticIntroScene->runAction(introSequence);

    this->addChild(casualisticIntroScene);

    return true;

}

void IntroSplashScene::onIntroComplete(Node* sender)
{
    auto introAnimationScene = MenuScene::createScene();
    Director::getInstance()->runWithScene(TransitionFade::create(1, introAnimationScene));
}