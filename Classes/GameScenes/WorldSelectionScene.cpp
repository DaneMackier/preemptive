#include "WorldSelectionScene.h"
#include "MenuScene.h"

#include "GameSceneComponents\TagDefinitions.h"
#include "GameEngine\LevelManager.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"
#include "Helpers\DatabaseService.h"


USING_NS_CC;

using namespace ui;

Scene* WorldSelectionScene::createScene()
{
    auto scene = Scene::create();
    auto layer = WorldSelectionScene::create();
    scene->addChild(layer);
    return scene;
}

bool WorldSelectionScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    auto headerImage = Sprite::create("UserInterface/WorldSelection/Header.png");
    PositioningHelper::placeGameItem(
        headerImage,
        0.5, 0.95,
        1.0, 0.15
        );
    this->addChild(headerImage, 1);

    int worldNumber = LevelManager::getInstance()->getCurrentWorld();
    // add the back button to the screen
    _backButton = Button::create(
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(worldNumber) + ".png",
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(worldNumber) + ".png");
    _backButton = (Button*)PositioningHelper::placeSquareWidget(
        _backButton,
        0.2,0.1,
        0.2);

    _backButton->addTouchEventListener(CC_CALLBACK_2(WorldSelectionScene::onBackButtonPressed, this));
    this->addChild(_backButton, 1);

    ScrollView* scrollView = ScrollView::create();
    scrollView->setContentSize(Size(PositioningHelper::getVisibleWidth(), PositioningHelper::getVisibleHeight() * 0.5));
    scrollView->setInnerContainerSize(Size(PositioningHelper::getVisibleWidth()*3, PositioningHelper::getHalfHeight()));
    scrollView->setDirection(ScrollView::Direction::HORIZONTAL);
    scrollView->setPosition(PositioningHelper::getRelativePosition(0.0, 0.25));


    Layout* layout = Layout::create();

    for (int i = 0; i < 4; i++)
    {
        
        layout->setContentSize(Size(PositioningHelper::getVisibleWidth(), PositioningHelper::getVisibleHeight() * 0.5));
        layout->setPosition(PositioningHelper::getRelativePosition(0.0, -0.25));
        int worldImageIndex = i + 1;

        Button* world = Button::create(
            "UserInterface/WorldSelection/World_" + StringHelper::ConvertIntToString(worldImageIndex) + ".png", 
            "UserInterface/WorldSelection/World_" + StringHelper::ConvertIntToString(worldImageIndex) + ".png",
            "UserInterface/WorldSelection/World_" + StringHelper::ConvertIntToString(worldImageIndex) + "_locked.png");
       
        if (worldImageIndex > LevelManager::getInstance()->getFurthestWorld())
        {
            // How to disable fucking buttons
           /* world->setEnabled(false);
            world->setPressedActionEnabled(true);
            world->setBright(false);*/
        }
       
        world->addTouchEventListener(CC_CALLBACK_2(WorldSelectionScene::onWorldSelected, this));
        world = (Button*)PositioningHelper::placeWidget(
                world,
                0.5 + (float)i,
                0.5 ,
                1,
                0.8
            );
        world->setTag(i);
        
        layout->addChild(world);
    }
    scrollView->addChild(layout);

    this->addChild(scrollView, 0);
    return true;
}

void WorldSelectionScene::onWorldSelected(Ref* sender, Widget::TouchEventType eventType)
{
    switch (eventType)
    {
    case Widget::TouchEventType::BEGAN:
        CCLog("Touch began");
        break;
    case Widget::TouchEventType::ENDED:
    {
        this->removeAllChildren();

        int selectedWorld = ((Node*)sender)->getTag() + 1;
        LevelManager::getInstance()->setWorld(selectedWorld);

        DatabaseService::setIntegerForKey("selected_world", selectedWorld);

        auto menuScene = (MenuScene*)(this->getParent());
        menuScene->navigateToLevelSelection();

        break;
    }
    case Widget::TouchEventType::MOVED:
        CCLog("Touch moved");
        break;
    case Widget::TouchEventType::CANCELED:
        CCLog("Touch cancelled");
        break;
    }
}

void WorldSelectionScene::onBackButtonPressed(Ref* sender, Widget::TouchEventType eventType)
{
    this->removeAllChildrenWithCleanup(true);

    auto menuScene = static_cast<MenuScene*>(this->getParent());
    menuScene->navigateToMainMenu();
}

void WorldSelectionScene::backButtonPressed()
{
    //_backButton->runAction(EaseElasticOut::create(ScaleTo::create(0.5, 1)));
    /*this->removeAllChildrenWithCleanup(true);

    auto menuScene = static_cast<MenuScene*>(this->getParent());
    menuScene->navigateToMainMenu();*/
}