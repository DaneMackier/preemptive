#ifndef __MENUSCENE_H__
#define __MENUSCENE_H__

#include "cocos2d.h"

#include "GameSceneComponents\AnimationLayer.h"
#include "GameSceneComponents\GameScene.h"

#include "Helpers\PositioningHelper.h"

#include "GameEngine\MenuFollow.h"


class MenuScene : public GameScene
{
private:
    
    /*
    * This is the time it will take for a paralx transition to complete
    */
    static float _paralaxTime;
    
    /*
    * The peak at which the easing starts to slow down. e.g 0.1 = 10% of above time
    */
    static float _paralaxRate;

    /*
    Initializes the paralax layers on the menu scene
    */
    void initializeParalaxMenu();
    
public:
    /*
    * Paralax node that all the menu layers are added to  to give the 3D world feel
    */
    static cocos2d::ParallaxNode* _paralaxNode;

    /*
    Replaces the world selection scene with the level selection scene
    */
    void navigateToLevelSelection();

    /*
    Replaces the main menu with the world selection scene
    */
    void navigateToWorldSelection();

    /*
    Places the current scene onto the main scene
    */
    void navigateToMainMenu();

    /*
    Places the current scene onto the main scene
    */
    void navigateToTutorialScene();

    /*
    Places the current scene onto the main scene
    */
    void navigateToStatisticScene();

    /*
    Places the current scene onto the main scene
    */
    void navigateToAboutScene();

    // ===================================================================================
    // The functions defined below is needed for all cocos2d scenes
    // Layer overrides and default cocos functionality ===================================
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MenuScene);

    virtual void handleBackNavigation();
};


#endif