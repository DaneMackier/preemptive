#include "IntroAnimationScene.h"
#include "MenuScene.h"

#include "Helpers\PositioningHelper.h"
#include "Helpers\StringHelper.h"

USING_NS_CC;
using namespace ui;

Scene* IntroAnimationScene::createScene()
{
    auto scene = Scene::create();
    auto layer = IntroAnimationScene::create();
    scene->addChild(layer);
    return scene;
}


bool IntroAnimationScene::init()
{
    if (!Layer::init())
    {
        return false;
    }
    PositioningHelper::screenBounds = Director::getInstance()->getVisibleSize();

    // add the back button to the screen
    Button* backButton = Button::create(
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png",
        "UserInterface/MainMenu/Back_" + StringHelper::ConvertIntToString(1) + ".png");
    backButton = (Button*)PositioningHelper::placeSquareWidget(
        backButton,
        0.2, 0.1,
        0.2);
    backButton->setRotation(270);

    backButton->addTouchEventListener(CC_CALLBACK_2(IntroAnimationScene::onSkipIntro, this));
    this->addChild(backButton, 1);

   
    auto introScene = Sprite::create("UserInterface/IntroScene/intro.jpg");
    PositioningHelper::placeGameItem(
        introScene,
        -0.5, 0.2,
        3.1, 1.7);
    this->addChild(introScene);

    auto startPosition = PositioningHelper::getRelativePosition(-0.5, 0.2);
    auto topRightPosition = PositioningHelper::getRelativePosition(-0.5, 0.8);
    auto middleRightPosition = PositioningHelper::getRelativePosition(0.5, 0.8);
    auto middleLeftPosition = PositioningHelper::getRelativePosition(0.5, 0.2);
    auto bottomLeftPosition = PositioningHelper::getRelativePosition(1.5, 0.2);
    auto bottomRightPosition = PositioningHelper::getRelativePosition(1.5, 0.8);

    CCFiniteTimeAction* fadeOutComplete = CCCallFuncN::create(this, callfuncN_selector(IntroAnimationScene::animationComplete));

    auto introSequence = Sequence::create(
        DelayTime::create(1.8),
        EaseBackOut::create(MoveTo::create(1, topRightPosition)),
        DelayTime::create(1),
        EaseBackOut::create(MoveTo::create(1, middleRightPosition)),
        DelayTime::create(1),
        EaseBackOut::create(MoveTo::create(1, middleLeftPosition)),
        DelayTime::create(1),
        EaseBackOut::create(MoveTo::create(1, bottomLeftPosition)),
        DelayTime::create(1),
        EaseBackOut::create(MoveTo::create(1, bottomRightPosition)),
        NULL);

    auto fadeSequence = Sequence::create(
        DelayTime::create(10.0f),
        fadeOutComplete,
        NULL);

    introScene->runAction(introSequence);
    introScene->runAction(fadeSequence);

    return true;
}

void IntroAnimationScene::onSkipIntro(Ref* sender,  Widget::TouchEventType eventType)
{
    auto menuScene = MenuScene::createScene();
    Director::getInstance()->runWithScene(TransitionFade::create(1, menuScene));
}

void IntroAnimationScene::animationComplete(Node* sender)
{
    auto menuScene = MenuScene::createScene();
    Director::getInstance()->runWithScene(TransitionFade::create(1, menuScene));
}